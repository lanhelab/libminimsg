/*
    Copyright (c) 2012-2013 Martin Sustrik  All rights reserved.
    Copyright 2016 Garrett D'Amore <garrett@damore.org>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "xpair.h"

#include "../../minimsg.h"
#include "../../pair.h"

#include "../utils/excl.h"

#include "../../utils/err.h"
#include "../../utils/cont.h"
#include "../../utils/alloc.h"
#include "../../utils/attr.h"

struct mini_xpair
{
    struct mini_sockbase sockbase;
    struct mini_excl excl;
};

/*  Private functions. */
static void mini_xpair_init(struct mini_xpair *self,
                            const struct mini_sockbase_vfptr *vfptr, void *hint);
static void mini_xpair_term(struct mini_xpair *self);

/*  Implementation of mini_sockbase's virtual functions. */
static void mini_xpair_destroy(struct mini_sockbase *self);
static int mini_xpair_add(struct mini_sockbase *self, struct mini_pipe *pipe);
static void mini_xpair_rm(struct mini_sockbase *self, struct mini_pipe *pipe);
static void mini_xpair_in(struct mini_sockbase *self, struct mini_pipe *pipe);
static void mini_xpair_out(struct mini_sockbase *self, struct mini_pipe *pipe);
static int mini_xpair_events(struct mini_sockbase *self);
static int mini_xpair_send(struct mini_sockbase *self, struct mini_msg *msg);
static int mini_xpair_recv(struct mini_sockbase *self, struct mini_msg *msg);
static const struct mini_sockbase_vfptr mini_xpair_sockbase_vfptr = {
    NULL,
    mini_xpair_destroy,
    mini_xpair_add,
    mini_xpair_rm,
    mini_xpair_in,
    mini_xpair_out,
    mini_xpair_events,
    mini_xpair_send,
    mini_xpair_recv,
    NULL,
    NULL};

static void mini_xpair_init(struct mini_xpair *self,
                            const struct mini_sockbase_vfptr *vfptr, void *hint)
{
    mini_sockbase_init(&self->sockbase, vfptr, hint);
    mini_excl_init(&self->excl);
}

static void mini_xpair_term(struct mini_xpair *self)
{
    mini_excl_term(&self->excl);
    mini_sockbase_term(&self->sockbase);
}

void mini_xpair_destroy(struct mini_sockbase *self)
{
    struct mini_xpair *xpair;

    xpair = mini_cont(self, struct mini_xpair, sockbase);

    mini_xpair_term(xpair);
    mini_free(xpair);
}

static int mini_xpair_add(struct mini_sockbase *self, struct mini_pipe *pipe)
{
    return mini_excl_add(&mini_cont(self, struct mini_xpair, sockbase)->excl,
                         pipe);
}

static void mini_xpair_rm(struct mini_sockbase *self, struct mini_pipe *pipe)
{
    mini_excl_rm(&mini_cont(self, struct mini_xpair, sockbase)->excl, pipe);
}

static void mini_xpair_in(struct mini_sockbase *self, struct mini_pipe *pipe)
{
    mini_excl_in(&mini_cont(self, struct mini_xpair, sockbase)->excl, pipe);
}

static void mini_xpair_out(struct mini_sockbase *self, struct mini_pipe *pipe)
{
    mini_excl_out(&mini_cont(self, struct mini_xpair, sockbase)->excl, pipe);
}

static int mini_xpair_events(struct mini_sockbase *self)
{
    struct mini_xpair *xpair;
    int events;

    xpair = mini_cont(self, struct mini_xpair, sockbase);

    events = 0;
    if (mini_excl_can_recv(&xpair->excl))
        events |= MINI_SOCKBASE_EVENT_IN;
    if (mini_excl_can_send(&xpair->excl))
        events |= MINI_SOCKBASE_EVENT_OUT;
    return events;
}

static int mini_xpair_send(struct mini_sockbase *self, struct mini_msg *msg)
{
    return mini_excl_send(&mini_cont(self, struct mini_xpair, sockbase)->excl,
                          msg);
}

static int mini_xpair_recv(struct mini_sockbase *self, struct mini_msg *msg)
{
    int rc;

    rc = mini_excl_recv(&mini_cont(self, struct mini_xpair, sockbase)->excl, msg);

    /*  Discard MINI_PIPEBASE_PARSED flag. */
    return rc < 0 ? rc : 0;
}

int mini_xpair_create(void *hint, struct mini_sockbase **sockbase)
{
    struct mini_xpair *self;

    self = mini_alloc(sizeof(struct mini_xpair), "socket (pair)");
    alloc_assert(self);
    mini_xpair_init(self, &mini_xpair_sockbase_vfptr, hint);
    *sockbase = &self->sockbase;

    return 0;
}

int mini_xpair_ispeer(int socktype)
{
    return socktype == MINI_PAIR ? 1 : 0;
}

struct mini_socktype mini_xpair_socktype = {
    AF_SP_RAW,
    MINI_PAIR,
    0,
    mini_xpair_create,
    mini_xpair_ispeer,
};
