/*
    Copyright (c) 2012-2013 Martin Sustrik  All rights reserved.
    Copyright 2016 Garrett D'Amore <garrett@damore.org>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#ifndef MINI_REQ_INCLUDED
#define MINI_REQ_INCLUDED

#include "xreq.h"
#include "task.h"

#include "../../protocol.h"
#include "../../io/fsm.h"

struct mini_req
{

    /*  The base class. Raw REQ socket. */
    struct mini_xreq xreq;

    /*  The state machine. */
    struct mini_fsm fsm;
    int state;

    /*  Last request ID assigned. */
    uint32_t lastid;

    /*  Protocol-specific socket options. */
    int resend_ivl;

    /*  The request being processed. */
    struct mini_task task;
};

/*  Some users may want to extend the REQ protocol similar to how REQ extends XREQ.
    Expose these methods to improve extensibility. */
void mini_req_init(struct mini_req *self,
                   const struct mini_sockbase_vfptr *vfptr, void *hint);
void mini_req_term(struct mini_req *self);
int mini_req_inprogress(struct mini_req *self);
void mini_req_handler(struct mini_fsm *self, int src, int type,
                      void *srcptr);
void mini_req_shutdown(struct mini_fsm *self, int src, int type,
                       void *srcptr);
void mini_req_action_send(struct mini_req *self, int allow_delay);

/*  Implementation of mini_sockbase's virtual functions. */
void mini_req_stop(struct mini_sockbase *self);
void mini_req_destroy(struct mini_sockbase *self);
void mini_req_in(struct mini_sockbase *self, struct mini_pipe *pipe);
void mini_req_out(struct mini_sockbase *self, struct mini_pipe *pipe);
int mini_req_events(struct mini_sockbase *self);
int mini_req_csend(struct mini_sockbase *self, struct mini_msg *msg);
void mini_req_rm(struct mini_sockbase *self, struct mini_pipe *pipe);
int mini_req_crecv(struct mini_sockbase *self, struct mini_msg *msg);
int mini_req_setopt(struct mini_sockbase *self, int level, int option,
                    const void *optval, size_t optvallen);
int mini_req_getopt(struct mini_sockbase *self, int level, int option,
                    void *optval, size_t *optvallen);
int mini_req_csend(struct mini_sockbase *self, struct mini_msg *msg);
int mini_req_crecv(struct mini_sockbase *self, struct mini_msg *msg);

#endif
