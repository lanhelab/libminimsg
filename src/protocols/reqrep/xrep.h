/*
    Copyright (c) 2012-2013 Martin Sustrik  All rights reserved.

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#ifndef MINI_XREP_INCLUDED
#define MINI_XREP_INCLUDED

#include "../../protocol.h"

#include "../../utils/hash.h"

#include "../utils/fq.h"

#include <stddef.h>

#define MINI_XREP_OUT 1

struct mini_xrep_data
{
    struct mini_pipe *pipe;
    struct mini_hash_item outitem;
    struct mini_fq_data initem;
    uint32_t flags;
};

struct mini_xrep
{

    struct mini_sockbase sockbase;

    /*  Key to be assigned to the next added pipe. */
    uint32_t next_key;

    /*  Map of all registered pipes indexed by the peer ID. */
    struct mini_hash outpipes;

    /*  Fair-queuer to get messages from. */
    struct mini_fq inpipes;
};

void mini_xrep_init(struct mini_xrep *self, const struct mini_sockbase_vfptr *vfptr,
                    void *hint);
void mini_xrep_term(struct mini_xrep *self);

int mini_xrep_add(struct mini_sockbase *self, struct mini_pipe *pipe);
void mini_xrep_rm(struct mini_sockbase *self, struct mini_pipe *pipe);
void mini_xrep_in(struct mini_sockbase *self, struct mini_pipe *pipe);
void mini_xrep_out(struct mini_sockbase *self, struct mini_pipe *pipe);
int mini_xrep_events(struct mini_sockbase *self);
int mini_xrep_send(struct mini_sockbase *self, struct mini_msg *msg);
int mini_xrep_recv(struct mini_sockbase *self, struct mini_msg *msg);

int mini_xrep_ispeer(int socktype);

#endif
