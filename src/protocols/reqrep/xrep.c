/*
    Copyright (c) 2012-2014 Martin Sustrik  All rights reserved.
    Copyright 2016 Garrett D'Amore <garrett@damore.org>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "xrep.h"

#include "../../minimsg.h"
#include "../../reqrep.h"

#include "../../utils/err.h"
#include "../../utils/cont.h"
#include "../../utils/fast.h"
#include "../../utils/alloc.h"
#include "../../utils/random.h"
#include "../../utils/wire.h"
#include "../../utils/attr.h"

#include <string.h>

/*  Private functions. */
static void mini_xrep_destroy(struct mini_sockbase *self);

static const struct mini_sockbase_vfptr mini_xrep_sockbase_vfptr = {
    NULL,
    mini_xrep_destroy,
    mini_xrep_add,
    mini_xrep_rm,
    mini_xrep_in,
    mini_xrep_out,
    mini_xrep_events,
    mini_xrep_send,
    mini_xrep_recv,
    NULL,
    NULL};

void mini_xrep_init(struct mini_xrep *self, const struct mini_sockbase_vfptr *vfptr,
                    void *hint)
{
    mini_sockbase_init(&self->sockbase, vfptr, hint);

    /*  Start assigning keys beginning with a random number. This way there
        are no key clashes even if the executable is re-started. */
    mini_random_generate(&self->next_key, sizeof(self->next_key));

    mini_hash_init(&self->outpipes);
    mini_fq_init(&self->inpipes);
}

void mini_xrep_term(struct mini_xrep *self)
{
    mini_fq_term(&self->inpipes);
    mini_hash_term(&self->outpipes);
    mini_sockbase_term(&self->sockbase);
}

static void mini_xrep_destroy(struct mini_sockbase *self)
{
    struct mini_xrep *xrep;

    xrep = mini_cont(self, struct mini_xrep, sockbase);

    mini_xrep_term(xrep);
    mini_free(xrep);
}

int mini_xrep_add(struct mini_sockbase *self, struct mini_pipe *pipe)
{
    struct mini_xrep *xrep;
    struct mini_xrep_data *data;
    int rcvprio;
    size_t sz;

    xrep = mini_cont(self, struct mini_xrep, sockbase);

    sz = sizeof(rcvprio);
    mini_pipe_getopt(pipe, MINI_SOL_SOCKET, MINI_RCVPRIO, &rcvprio, &sz);
    mini_assert(sz == sizeof(rcvprio));
    mini_assert(rcvprio >= 1 && rcvprio <= 16);

    data = mini_alloc(sizeof(struct mini_xrep_data), "pipe data (xrep)");
    alloc_assert(data);
    data->pipe = pipe;
    mini_hash_item_init(&data->outitem);
    data->flags = 0;
    mini_hash_insert(&xrep->outpipes, xrep->next_key & 0x7fffffff,
                     &data->outitem);
    ++xrep->next_key;
    mini_fq_add(&xrep->inpipes, &data->initem, pipe, rcvprio);
    mini_pipe_setdata(pipe, data);

    return 0;
}

void mini_xrep_rm(struct mini_sockbase *self, struct mini_pipe *pipe)
{
    struct mini_xrep *xrep;
    struct mini_xrep_data *data;

    xrep = mini_cont(self, struct mini_xrep, sockbase);
    data = mini_pipe_getdata(pipe);

    mini_fq_rm(&xrep->inpipes, &data->initem);
    mini_hash_erase(&xrep->outpipes, &data->outitem);
    mini_hash_item_term(&data->outitem);

    mini_free(data);
}

void mini_xrep_in(struct mini_sockbase *self, struct mini_pipe *pipe)
{
    struct mini_xrep *xrep;
    struct mini_xrep_data *data;

    xrep = mini_cont(self, struct mini_xrep, sockbase);
    data = mini_pipe_getdata(pipe);

    mini_fq_in(&xrep->inpipes, &data->initem);
}

void mini_xrep_out(MINI_UNUSED struct mini_sockbase *self, struct mini_pipe *pipe)
{
    struct mini_xrep_data *data;

    data = mini_pipe_getdata(pipe);
    data->flags |= MINI_XREP_OUT;
}

int mini_xrep_events(struct mini_sockbase *self)
{
    return (mini_fq_can_recv(&mini_cont(self, struct mini_xrep,
                                        sockbase)
                                  ->inpipes)
                ? MINI_SOCKBASE_EVENT_IN
                : 0) |
           MINI_SOCKBASE_EVENT_OUT;
}

int mini_xrep_send(struct mini_sockbase *self, struct mini_msg *msg)
{
    int rc;
    uint32_t key;
    struct mini_xrep *xrep;
    struct mini_xrep_data *data;

    xrep = mini_cont(self, struct mini_xrep, sockbase);

    /*  We treat invalid peer ID as if the peer was non-existent. */
    if (mini_slow(mini_chunkref_size(&msg->sphdr) < sizeof(uint32_t)))
    {
        mini_msg_term(msg);
        return 0;
    }

    /*  Retrieve the destination peer ID. Trim it from the header. */
    key = mini_getl(mini_chunkref_data(&msg->sphdr));
    mini_chunkref_trim(&msg->sphdr, 4);

    /*  Find the appropriate pipe to send the message to. If there's none,
        or if it's not ready for sending, silently drop the message. */
    data = mini_cont(mini_hash_get(&xrep->outpipes, key), struct mini_xrep_data,
                     outitem);
    if (!data || !(data->flags & MINI_XREP_OUT))
    {
        mini_msg_term(msg);
        return 0;
    }

    /*  Send the message. */
    rc = mini_pipe_send(data->pipe, msg);
    errnum_assert(rc >= 0, -rc);
    if (rc & MINI_PIPE_RELEASE)
        data->flags &= ~MINI_XREP_OUT;

    return 0;
}

int mini_xrep_recv(struct mini_sockbase *self, struct mini_msg *msg)
{
    int rc;
    struct mini_xrep *xrep;
    struct mini_pipe *pipe;
    int i;
    int maxttl;
    void *data;
    size_t sz;
    struct mini_chunkref ref;
    struct mini_xrep_data *pipedata;

    xrep = mini_cont(self, struct mini_xrep, sockbase);

    rc = mini_fq_recv(&xrep->inpipes, msg, &pipe);
    if (mini_slow(rc < 0))
        return rc;

    if (!(rc & MINI_PIPE_PARSED))
    {

        sz = sizeof(maxttl);
        rc = mini_sockbase_getopt(self, MINI_MAXTTL, &maxttl, &sz);
        errnum_assert(rc == 0, -rc);

        /*  Determine the size of the message header. */
        data = mini_chunkref_data(&msg->body);
        sz = mini_chunkref_size(&msg->body);
        i = 0;
        while (1)
        {

            /*  Ignore the malformed requests without the bottom of the stack. */
            if (mini_slow((i + 1) * sizeof(uint32_t) > sz))
            {
                mini_msg_term(msg);
                return -EAGAIN;
            }

            /*  If the bottom of the backtrace stack is reached, proceed. */
            if (mini_getl((uint8_t *)(((uint32_t *)data) + i)) & 0x80000000)
                break;

            ++i;
        }
        ++i;

        /* If we encountered too many hops, just toss the message */
        if (i > maxttl)
        {
            mini_msg_term(msg);
            return -EAGAIN;
        }

        /*  Split the header and the body. */
        mini_assert(mini_chunkref_size(&msg->sphdr) == 0);
        mini_chunkref_term(&msg->sphdr);
        mini_chunkref_init(&msg->sphdr, i * sizeof(uint32_t));
        memcpy(mini_chunkref_data(&msg->sphdr), data, i * sizeof(uint32_t));
        mini_chunkref_trim(&msg->body, i * sizeof(uint32_t));
    }

    /*  Prepend the header by the pipe key. */
    pipedata = mini_pipe_getdata(pipe);
    mini_chunkref_init(&ref,
                       mini_chunkref_size(&msg->sphdr) + sizeof(uint32_t));
    mini_putl(mini_chunkref_data(&ref), pipedata->outitem.key);
    memcpy(((uint8_t *)mini_chunkref_data(&ref)) + sizeof(uint32_t),
           mini_chunkref_data(&msg->sphdr), mini_chunkref_size(&msg->sphdr));
    mini_chunkref_term(&msg->sphdr);
    mini_chunkref_mv(&msg->sphdr, &ref);

    return 0;
}

static int mini_xrep_create(void *hint, struct mini_sockbase **sockbase)
{
    struct mini_xrep *self;

    self = mini_alloc(sizeof(struct mini_xrep), "socket (xrep)");
    alloc_assert(self);
    mini_xrep_init(self, &mini_xrep_sockbase_vfptr, hint);
    *sockbase = &self->sockbase;

    return 0;
}

int mini_xrep_ispeer(int socktype)
{
    return socktype == MINI_REQ ? 1 : 0;
}

struct mini_socktype mini_xrep_socktype = {
    AF_SP_RAW,
    MINI_REP,
    0,
    mini_xrep_create,
    mini_xrep_ispeer,
};
