/*
    Copyright (c) 2012-2013 Martin Sustrik  All rights reserved.
    Copyright 2016 Garrett D'Amore <garrett@damore.org>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#ifndef MINI_XREQ_INCLUDED
#define MINI_XREQ_INCLUDED

#include "../../protocol.h"

#include "../utils/lb.h"
#include "../utils/fq.h"

struct mini_xreq
{
    struct mini_sockbase sockbase;
    struct mini_lb lb;
    struct mini_fq fq;
};

void mini_xreq_init(struct mini_xreq *self, const struct mini_sockbase_vfptr *vfptr,
                    void *hint);
void mini_xreq_term(struct mini_xreq *self);

int mini_xreq_add(struct mini_sockbase *self, struct mini_pipe *pipe);
void mini_xreq_rm(struct mini_sockbase *self, struct mini_pipe *pipe);
void mini_xreq_in(struct mini_sockbase *self, struct mini_pipe *pipe);
void mini_xreq_out(struct mini_sockbase *self, struct mini_pipe *pipe);
int mini_xreq_events(struct mini_sockbase *self);
int mini_xreq_send(struct mini_sockbase *self, struct mini_msg *msg);
int mini_xreq_send_to(struct mini_sockbase *self, struct mini_msg *msg,
                      struct mini_pipe **to);
int mini_xreq_recv(struct mini_sockbase *self, struct mini_msg *msg);

int mini_xreq_ispeer(int socktype);

#endif
