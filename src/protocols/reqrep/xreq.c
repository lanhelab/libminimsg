/*
    Copyright (c) 2012-2014 Martin Sustrik  All rights reserved.
    Copyright 2016 Garrett D'Amore <garrett@damore.org>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "xreq.h"

#include "../../minimsg.h"
#include "../../reqrep.h"

#include "../../utils/err.h"
#include "../../utils/cont.h"
#include "../../utils/fast.h"
#include "../../utils/alloc.h"
#include "../../utils/attr.h"

struct mini_xreq_data
{
    struct mini_lb_data lb;
    struct mini_fq_data fq;
};

/*  Private functions. */
static void mini_xreq_destroy(struct mini_sockbase *self);

static const struct mini_sockbase_vfptr mini_xreq_sockbase_vfptr = {
    NULL,
    mini_xreq_destroy,
    mini_xreq_add,
    mini_xreq_rm,
    mini_xreq_in,
    mini_xreq_out,
    mini_xreq_events,
    mini_xreq_send,
    mini_xreq_recv,
    NULL,
    NULL};

void mini_xreq_init(struct mini_xreq *self, const struct mini_sockbase_vfptr *vfptr,
                    void *hint)
{
    mini_sockbase_init(&self->sockbase, vfptr, hint);
    mini_lb_init(&self->lb);
    mini_fq_init(&self->fq);
}

void mini_xreq_term(struct mini_xreq *self)
{
    mini_fq_term(&self->fq);
    mini_lb_term(&self->lb);
    mini_sockbase_term(&self->sockbase);
}

static void mini_xreq_destroy(struct mini_sockbase *self)
{
    struct mini_xreq *xreq;

    xreq = mini_cont(self, struct mini_xreq, sockbase);

    mini_xreq_term(xreq);
    mini_free(xreq);
}

int mini_xreq_add(struct mini_sockbase *self, struct mini_pipe *pipe)
{
    struct mini_xreq *xreq;
    struct mini_xreq_data *data;
    int sndprio;
    int rcvprio;
    size_t sz;

    xreq = mini_cont(self, struct mini_xreq, sockbase);

    sz = sizeof(sndprio);
    mini_pipe_getopt(pipe, MINI_SOL_SOCKET, MINI_SNDPRIO, &sndprio, &sz);
    mini_assert(sz == sizeof(sndprio));
    mini_assert(sndprio >= 1 && sndprio <= 16);

    sz = sizeof(rcvprio);
    mini_pipe_getopt(pipe, MINI_SOL_SOCKET, MINI_RCVPRIO, &rcvprio, &sz);
    mini_assert(sz == sizeof(rcvprio));
    mini_assert(rcvprio >= 1 && rcvprio <= 16);

    data = mini_alloc(sizeof(struct mini_xreq_data), "pipe data (req)");
    alloc_assert(data);
    mini_pipe_setdata(pipe, data);
    mini_lb_add(&xreq->lb, &data->lb, pipe, sndprio);
    mini_fq_add(&xreq->fq, &data->fq, pipe, rcvprio);

    return 0;
}

void mini_xreq_rm(struct mini_sockbase *self, struct mini_pipe *pipe)
{
    struct mini_xreq *xreq;
    struct mini_xreq_data *data;

    xreq = mini_cont(self, struct mini_xreq, sockbase);
    data = mini_pipe_getdata(pipe);
    mini_lb_rm(&xreq->lb, &data->lb);
    mini_fq_rm(&xreq->fq, &data->fq);
    mini_free(data);

    mini_sockbase_stat_increment(self, MINI_STAT_CURRENT_SND_PRIORITY,
                                 mini_lb_get_priority(&xreq->lb));
}

void mini_xreq_in(struct mini_sockbase *self, struct mini_pipe *pipe)
{
    struct mini_xreq *xreq;
    struct mini_xreq_data *data;

    xreq = mini_cont(self, struct mini_xreq, sockbase);
    data = mini_pipe_getdata(pipe);
    mini_fq_in(&xreq->fq, &data->fq);
}

void mini_xreq_out(struct mini_sockbase *self, struct mini_pipe *pipe)
{
    struct mini_xreq *xreq;
    struct mini_xreq_data *data;

    xreq = mini_cont(self, struct mini_xreq, sockbase);
    data = mini_pipe_getdata(pipe);
    mini_lb_out(&xreq->lb, &data->lb);

    mini_sockbase_stat_increment(self, MINI_STAT_CURRENT_SND_PRIORITY,
                                 mini_lb_get_priority(&xreq->lb));
}

int mini_xreq_events(struct mini_sockbase *self)
{
    struct mini_xreq *xreq;

    xreq = mini_cont(self, struct mini_xreq, sockbase);

    return (mini_fq_can_recv(&xreq->fq) ? MINI_SOCKBASE_EVENT_IN : 0) |
           (mini_lb_can_send(&xreq->lb) ? MINI_SOCKBASE_EVENT_OUT : 0);
}

int mini_xreq_send(struct mini_sockbase *self, struct mini_msg *msg)
{
    return mini_xreq_send_to(self, msg, NULL);
}

int mini_xreq_send_to(struct mini_sockbase *self, struct mini_msg *msg,
                      struct mini_pipe **to)
{
    int rc;

    /*  If request cannot be sent due to the pushback, drop it silenly. */
    rc = mini_lb_send(&mini_cont(self, struct mini_xreq, sockbase)->lb, msg, to);
    if (mini_slow(rc == -EAGAIN))
        return -EAGAIN;
    errnum_assert(rc >= 0, -rc);

    return 0;
}

int mini_xreq_recv(struct mini_sockbase *self, struct mini_msg *msg)
{
    int rc;

    rc = mini_fq_recv(&mini_cont(self, struct mini_xreq, sockbase)->fq, msg, NULL);
    if (rc == -EAGAIN)
        return -EAGAIN;
    errnum_assert(rc >= 0, -rc);

    if (!(rc & MINI_PIPE_PARSED))
    {

        /*  Ignore malformed replies. */
        if (mini_slow(mini_chunkref_size(&msg->body) < sizeof(uint32_t)))
        {
            mini_msg_term(msg);
            return -EAGAIN;
        }

        /*  Split the message into the header and the body. */
        mini_assert(mini_chunkref_size(&msg->sphdr) == 0);
        mini_chunkref_term(&msg->sphdr);
        mini_chunkref_init(&msg->sphdr, sizeof(uint32_t));
        memcpy(mini_chunkref_data(&msg->sphdr), mini_chunkref_data(&msg->body),
               sizeof(uint32_t));
        mini_chunkref_trim(&msg->body, sizeof(uint32_t));
    }

    return 0;
}

static int mini_xreq_create(void *hint, struct mini_sockbase **sockbase)
{
    struct mini_xreq *self;

    self = mini_alloc(sizeof(struct mini_xreq), "socket (xreq)");
    alloc_assert(self);
    mini_xreq_init(self, &mini_xreq_sockbase_vfptr, hint);
    *sockbase = &self->sockbase;

    return 0;
}

int mini_xreq_ispeer(int socktype)
{
    return socktype == MINI_REP ? 1 : 0;
}

struct mini_socktype mini_xreq_socktype = {
    AF_SP_RAW,
    MINI_REQ,
    0,
    mini_xreq_create,
    mini_xreq_ispeer,
};
