/*
    Copyright (c) 2012-2013 Martin Sustrik  All rights reserved.
    Copyright 2016 Garrett D'Amore

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "req.h"
#include "xreq.h"

#include "../../minimsg.h"
#include "../../reqrep.h"

#include "../../io/fsm.h"
#include "../../io/timer.h"

#include "../../utils/err.h"
#include "../../utils/cont.h"
#include "../../utils/alloc.h"
#include "../../utils/random.h"
#include "../../utils/wire.h"
#include "../../utils/attr.h"

#include <stddef.h>
#include <string.h>

/*  Default re-send interval is 1 minute. */
#define MINI_REQ_DEFAULT_RESEND_IVL 60000

#define MINI_REQ_STATE_IDLE 1
#define MINI_REQ_STATE_PASSIVE 2
#define MINI_REQ_STATE_DELAYED 3
#define MINI_REQ_STATE_ACTIVE 4
#define MINI_REQ_STATE_TIMED_OUT 5
#define MINI_REQ_STATE_CANCELLING 6
#define MINI_REQ_STATE_STOPPING_TIMER 7
#define MINI_REQ_STATE_DONE 8
#define MINI_REQ_STATE_STOPPING 9

#define MINI_REQ_ACTION_START 1
#define MINI_REQ_ACTION_IN 2
#define MINI_REQ_ACTION_OUT 3
#define MINI_REQ_ACTION_SENT 4
#define MINI_REQ_ACTION_RECEIVED 5
#define MINI_REQ_ACTION_PIPE_RM 6

#define MINI_REQ_SRC_RESEND_TIMER 1

static const struct mini_sockbase_vfptr mini_req_sockbase_vfptr = {
    mini_req_stop,
    mini_req_destroy,
    mini_xreq_add,
    mini_req_rm,
    mini_req_in,
    mini_req_out,
    mini_req_events,
    mini_req_csend,
    mini_req_crecv,
    mini_req_setopt,
    mini_req_getopt};

void mini_req_init(struct mini_req *self,
                   const struct mini_sockbase_vfptr *vfptr, void *hint)
{
    mini_xreq_init(&self->xreq, vfptr, hint);
    mini_fsm_init_root(&self->fsm, mini_req_handler, mini_req_shutdown,
                       mini_sockbase_getctx(&self->xreq.sockbase));
    self->state = MINI_REQ_STATE_IDLE;

    /*  Start assigning request IDs beginning with a random number. This way
        there should be no key clashes even if the executable is re-started. */
    mini_random_generate(&self->lastid, sizeof(self->lastid));

    self->task.sent_to = NULL;

    mini_msg_init(&self->task.request, 0);
    mini_msg_init(&self->task.reply, 0);
    mini_timer_init(&self->task.timer, MINI_REQ_SRC_RESEND_TIMER, &self->fsm);
    self->resend_ivl = MINI_REQ_DEFAULT_RESEND_IVL;

    mini_task_init(&self->task, self->lastid);

    /*  Start the state machine. */
    mini_fsm_start(&self->fsm);
}

void mini_req_term(struct mini_req *self)
{
    mini_timer_term(&self->task.timer);
    mini_task_term(&self->task);
    mini_msg_term(&self->task.reply);
    mini_msg_term(&self->task.request);
    mini_fsm_term(&self->fsm);
    mini_xreq_term(&self->xreq);
}

void mini_req_stop(struct mini_sockbase *self)
{
    struct mini_req *req;

    req = mini_cont(self, struct mini_req, xreq.sockbase);

    mini_fsm_stop(&req->fsm);
}

void mini_req_destroy(struct mini_sockbase *self)
{
    struct mini_req *req;

    req = mini_cont(self, struct mini_req, xreq.sockbase);

    mini_req_term(req);
    mini_free(req);
}

int mini_req_inprogress(struct mini_req *self)
{
    /*  Return 1 if there's a request submitted. 0 otherwise. */
    return self->state == MINI_REQ_STATE_IDLE ||
                   self->state == MINI_REQ_STATE_PASSIVE ||
                   self->state == MINI_REQ_STATE_STOPPING
               ? 0
               : 1;
}

void mini_req_in(struct mini_sockbase *self, struct mini_pipe *pipe)
{
    int rc;
    struct mini_req *req;
    uint32_t reqid;

    req = mini_cont(self, struct mini_req, xreq.sockbase);

    /*  Pass the pipe to the raw REQ socket. */
    mini_xreq_in(&req->xreq.sockbase, pipe);

    while (1)
    {

        /*  Get new reply. */
        rc = mini_xreq_recv(&req->xreq.sockbase, &req->task.reply);
        if (mini_slow(rc == -EAGAIN))
            return;
        errnum_assert(rc == 0, -rc);

        /*  No request was sent. Getting a reply doesn't make sense. */
        if (mini_slow(!mini_req_inprogress(req)))
        {
            mini_msg_term(&req->task.reply);
            continue;
        }

        /*  Ignore malformed replies. */
        if (mini_slow(mini_chunkref_size(&req->task.reply.sphdr) !=
                      sizeof(uint32_t)))
        {
            mini_msg_term(&req->task.reply);
            continue;
        }

        /*  Ignore replies with incorrect request IDs. */
        reqid = mini_getl(mini_chunkref_data(&req->task.reply.sphdr));
        if (mini_slow(!(reqid & 0x80000000)))
        {
            mini_msg_term(&req->task.reply);
            continue;
        }
        if (mini_slow(reqid != (req->task.id | 0x80000000)))
        {
            mini_msg_term(&req->task.reply);
            continue;
        }

        /*  Trim the request ID. */
        mini_chunkref_term(&req->task.reply.sphdr);
        mini_chunkref_init(&req->task.reply.sphdr, 0);

        /*  TODO: Deallocate the request here? */

        /*  Notify the state machine. */
        if (req->state == MINI_REQ_STATE_ACTIVE)
            mini_fsm_action(&req->fsm, MINI_REQ_ACTION_IN);

        return;
    }
}

void mini_req_out(struct mini_sockbase *self, struct mini_pipe *pipe)
{
    struct mini_req *req;

    req = mini_cont(self, struct mini_req, xreq.sockbase);

    /*  Add the pipe to the underlying raw socket. */
    mini_xreq_out(&req->xreq.sockbase, pipe);

    /*  Notify the state machine. */
    if (req->state == MINI_REQ_STATE_DELAYED)
        mini_fsm_action(&req->fsm, MINI_REQ_ACTION_OUT);
}

int mini_req_events(struct mini_sockbase *self)
{
    int rc;
    struct mini_req *req;

    req = mini_cont(self, struct mini_req, xreq.sockbase);

    /*  OUT is signalled all the time because sending a request while
        another one is being processed cancels the old one. */
    rc = MINI_SOCKBASE_EVENT_OUT;

    /*  In DONE state the reply is stored in 'reply' field. */
    if (req->state == MINI_REQ_STATE_DONE)
        rc |= MINI_SOCKBASE_EVENT_IN;

    return rc;
}

int mini_req_csend(struct mini_sockbase *self, struct mini_msg *msg)
{
    struct mini_req *req;

    req = mini_cont(self, struct mini_req, xreq.sockbase);

    /*  Generate new request ID for the new request and put it into message
        header. The most important bit is set to 1 to indicate that this is
        the bottom of the backtrace stack. */
    ++req->task.id;
    mini_assert(mini_chunkref_size(&msg->sphdr) == 0);
    mini_chunkref_term(&msg->sphdr);
    mini_chunkref_init(&msg->sphdr, 4);
    mini_putl(mini_chunkref_data(&msg->sphdr), req->task.id | 0x80000000);

    /*  Store the message so that it can be re-sent if there's no reply. */
    mini_msg_term(&req->task.request);
    mini_msg_mv(&req->task.request, msg);

    /*  Notify the state machine. */
    mini_fsm_action(&req->fsm, MINI_REQ_ACTION_SENT);

    return 0;
}

int mini_req_crecv(struct mini_sockbase *self, struct mini_msg *msg)
{
    struct mini_req *req;

    req = mini_cont(self, struct mini_req, xreq.sockbase);

    /*  No request was sent. Waiting for a reply doesn't make sense. */
    if (mini_slow(!mini_req_inprogress(req)))
        return -EFSM;

    /*  If reply was not yet recieved, wait further. */
    if (mini_slow(req->state != MINI_REQ_STATE_DONE))
        return -EAGAIN;

    /*  If the reply was already received, just pass it to the caller. */
    mini_msg_mv(msg, &req->task.reply);
    mini_msg_init(&req->task.reply, 0);

    /*  Notify the state machine. */
    mini_fsm_action(&req->fsm, MINI_REQ_ACTION_RECEIVED);

    return 0;
}

int mini_req_setopt(struct mini_sockbase *self, int level, int option,
                    const void *optval, size_t optvallen)
{
    struct mini_req *req;

    req = mini_cont(self, struct mini_req, xreq.sockbase);

    if (level != MINI_REQ)
        return -ENOPROTOOPT;

    if (option == MINI_REQ_RESEND_IVL)
    {
        if (mini_slow(optvallen != sizeof(int)))
            return -EINVAL;
        req->resend_ivl = *(int *)optval;
        return 0;
    }

    return -ENOPROTOOPT;
}

int mini_req_getopt(struct mini_sockbase *self, int level, int option,
                    void *optval, size_t *optvallen)
{
    struct mini_req *req;

    req = mini_cont(self, struct mini_req, xreq.sockbase);

    if (level != MINI_REQ)
        return -ENOPROTOOPT;

    if (option == MINI_REQ_RESEND_IVL)
    {
        if (mini_slow(*optvallen < sizeof(int)))
            return -EINVAL;
        *(int *)optval = req->resend_ivl;
        *optvallen = sizeof(int);
        return 0;
    }

    return -ENOPROTOOPT;
}

void mini_req_shutdown(struct mini_fsm *self, int src, int type,
                       MINI_UNUSED void *srcptr)
{
    struct mini_req *req;

    req = mini_cont(self, struct mini_req, fsm);

    if (mini_slow(src == MINI_FSM_ACTION && type == MINI_FSM_STOP))
    {
        mini_timer_stop(&req->task.timer);
        req->state = MINI_REQ_STATE_STOPPING;
    }
    if (mini_slow(req->state == MINI_REQ_STATE_STOPPING))
    {
        if (!mini_timer_isidle(&req->task.timer))
            return;
        req->state = MINI_REQ_STATE_IDLE;
        mini_fsm_stopped_noevent(&req->fsm);
        mini_sockbase_stopped(&req->xreq.sockbase);
        return;
    }

    mini_fsm_bad_state(req->state, src, type);
}

void mini_req_handler(struct mini_fsm *self, int src, int type,
                      MINI_UNUSED void *srcptr)
{
    struct mini_req *req;

    req = mini_cont(self, struct mini_req, fsm);

    switch (req->state)
    {

        /******************************************************************************/
        /*  IDLE state.                                                               */
        /*  The socket was created recently. Intermediate state.                      */
        /*  Pass straight to the PASSIVE state.                                       */
        /******************************************************************************/
    case MINI_REQ_STATE_IDLE:
        switch (src)
        {

        case MINI_FSM_ACTION:
            switch (type)
            {
            case MINI_FSM_START:
                req->state = MINI_REQ_STATE_PASSIVE;
                return;
            default:
                mini_fsm_bad_action(req->state, src, type);
            }

        default:
            mini_fsm_bad_source(req->state, src, type);
        }

        /******************************************************************************/
        /*  PASSIVE state.                                                            */
        /*  No request is submitted.                                                  */
        /******************************************************************************/
    case MINI_REQ_STATE_PASSIVE:
        switch (src)
        {

        case MINI_FSM_ACTION:
            switch (type)
            {
            case MINI_REQ_ACTION_SENT:
                mini_req_action_send(req, 1);
                return;
            default:
                mini_fsm_bad_action(req->state, src, type);
            }

        default:
            mini_fsm_bad_source(req->state, src, type);
        }

        /******************************************************************************/
        /*  DELAYED state.                                                            */
        /*  Request was submitted but it could not be sent to the network because     */
        /*  there was no peer available at the moment. Now we are waiting for the     */
        /*  peer to arrive to send the request to it.                                 */
        /******************************************************************************/
    case MINI_REQ_STATE_DELAYED:
        switch (src)
        {

        case MINI_FSM_ACTION:
            switch (type)
            {
            case MINI_REQ_ACTION_OUT:
                mini_req_action_send(req, 0);
                return;
            case MINI_REQ_ACTION_SENT:
                return;
            default:
                mini_fsm_bad_action(req->state, src, type);
            }

        default:
            mini_fsm_bad_source(req->state, src, type);
        }

        /******************************************************************************/
        /*  ACTIVE state.                                                             */
        /*  Request was submitted. Waiting for reply.                                 */
        /******************************************************************************/
    case MINI_REQ_STATE_ACTIVE:
        switch (src)
        {

        case MINI_FSM_ACTION:
            switch (type)
            {
            case MINI_REQ_ACTION_IN:

                /*  Reply arrived. */
                mini_timer_stop(&req->task.timer);
                req->task.sent_to = NULL;
                req->state = MINI_REQ_STATE_STOPPING_TIMER;
                return;

            case MINI_REQ_ACTION_SENT:

                /*  New request was sent while the old one was still being
                    processed. Cancel the old request first. */
                mini_timer_stop(&req->task.timer);
                req->task.sent_to = NULL;
                req->state = MINI_REQ_STATE_CANCELLING;
                return;

            case MINI_REQ_ACTION_PIPE_RM:
                /*  Pipe that we sent request to is removed  */
                mini_timer_stop(&req->task.timer);
                req->task.sent_to = NULL;
                /*  Pretend we timed out so request resent immediately  */
                req->state = MINI_REQ_STATE_TIMED_OUT;
                return;

            default:
                mini_fsm_bad_action(req->state, src, type);
            }

        case MINI_REQ_SRC_RESEND_TIMER:
            switch (type)
            {
            case MINI_TIMER_TIMEOUT:
                mini_timer_stop(&req->task.timer);
                req->task.sent_to = NULL;
                req->state = MINI_REQ_STATE_TIMED_OUT;
                return;
            default:
                mini_fsm_bad_action(req->state, src, type);
            }

        default:
            mini_fsm_bad_source(req->state, src, type);
        }

        /******************************************************************************/
        /*  TIMED_OUT state.                                                          */
        /*  Waiting for reply has timed out. Stopping the timer. Afterwards, we'll    */
        /*  re-send the request.                                                      */
        /******************************************************************************/
    case MINI_REQ_STATE_TIMED_OUT:
        switch (src)
        {

        case MINI_REQ_SRC_RESEND_TIMER:
            switch (type)
            {
            case MINI_TIMER_STOPPED:
                mini_req_action_send(req, 1);
                return;
            default:
                mini_fsm_bad_action(req->state, src, type);
            }

        case MINI_FSM_ACTION:
            switch (type)
            {
            case MINI_REQ_ACTION_SENT:
                req->state = MINI_REQ_STATE_CANCELLING;
                return;
            default:
                mini_fsm_bad_action(req->state, src, type);
            }

        default:
            mini_fsm_bad_source(req->state, src, type);
        }

        /******************************************************************************/
        /*  CANCELLING state.                                                         */
        /*  Request was canceled. Waiting till the timer is stopped. Note that        */
        /*  cancelling is done by sending a new request. Thus there's already         */
        /*  a request waiting to be sent in this state.                               */
        /******************************************************************************/
    case MINI_REQ_STATE_CANCELLING:
        switch (src)
        {

        case MINI_REQ_SRC_RESEND_TIMER:
            switch (type)
            {
            case MINI_TIMER_STOPPED:

                /*  Timer is stopped. Now we can send the delayed request. */
                mini_req_action_send(req, 1);
                return;

            default:
                mini_fsm_bad_action(req->state, src, type);
            }

        case MINI_FSM_ACTION:
            switch (type)
            {
            case MINI_REQ_ACTION_SENT:

                /*  No need to do anything here. Old delayed request is just
                    replaced by the new one that will be sent once the timer
                    is closed. */
                return;

            default:
                mini_fsm_bad_action(req->state, src, type);
            }

        default:
            mini_fsm_bad_source(req->state, src, type);
        }

        /******************************************************************************/
        /*  STOPPING_TIMER state.                                                     */
        /*  Reply was delivered. Waiting till the timer is stopped.                   */
        /******************************************************************************/
    case MINI_REQ_STATE_STOPPING_TIMER:
        switch (src)
        {

        case MINI_REQ_SRC_RESEND_TIMER:

            switch (type)
            {
            case MINI_TIMER_STOPPED:
                req->state = MINI_REQ_STATE_DONE;
                return;
            default:
                mini_fsm_bad_action(req->state, src, type);
            }

        case MINI_FSM_ACTION:
            switch (type)
            {
            case MINI_REQ_ACTION_SENT:
                req->state = MINI_REQ_STATE_CANCELLING;
                return;
            default:
                mini_fsm_bad_action(req->state, src, type);
            }

        default:
            mini_fsm_bad_source(req->state, src, type);
        }

        /******************************************************************************/
        /*  DONE state.                                                               */
        /*  Reply was received but not yet retrieved by the user.                     */
        /******************************************************************************/
    case MINI_REQ_STATE_DONE:
        switch (src)
        {

        case MINI_FSM_ACTION:
            switch (type)
            {
            case MINI_REQ_ACTION_RECEIVED:
                req->state = MINI_REQ_STATE_PASSIVE;
                return;
            case MINI_REQ_ACTION_SENT:
                mini_req_action_send(req, 1);
                return;
            default:
                mini_fsm_bad_action(req->state, src, type);
            }

        default:
            mini_fsm_bad_source(req->state, src, type);
        }

        /******************************************************************************/
        /*  Invalid state.                                                            */
        /******************************************************************************/
    default:
        mini_fsm_bad_state(req->state, src, type);
    }
}

/******************************************************************************/
/*  State machine actions.                                                    */
/******************************************************************************/

void mini_req_action_send(struct mini_req *self, int allow_delay)
{
    int rc;
    struct mini_msg msg;
    struct mini_pipe *to;

    /*  Send the request. */
    mini_msg_cp(&msg, &self->task.request);
    rc = mini_xreq_send_to(&self->xreq.sockbase, &msg, &to);

    /*  If the request cannot be sent at the moment wait till
        new outbound pipe arrives. */
    if (mini_slow(rc == -EAGAIN))
    {
        mini_assert(allow_delay == 1);
        mini_msg_term(&msg);
        self->state = MINI_REQ_STATE_DELAYED;
        return;
    }

    /*  Request was successfully sent. Set up the re-send timer
        in case the request gets lost somewhere further out
        in the topology. */
    if (mini_fast(rc == 0))
    {
        mini_timer_start(&self->task.timer, self->resend_ivl);
        mini_assert(to);
        self->task.sent_to = to;
        self->state = MINI_REQ_STATE_ACTIVE;
        return;
    }

    /*  Unexpected error. */
    errnum_assert(0, -rc);
}

static int mini_req_create(void *hint, struct mini_sockbase **sockbase)
{
    struct mini_req *self;

    self = mini_alloc(sizeof(struct mini_req), "socket (req)");
    alloc_assert(self);
    mini_req_init(self, &mini_req_sockbase_vfptr, hint);
    *sockbase = &self->xreq.sockbase;

    return 0;
}

void mini_req_rm(struct mini_sockbase *self, struct mini_pipe *pipe)
{
    struct mini_req *req;

    req = mini_cont(self, struct mini_req, xreq.sockbase);

    mini_xreq_rm(self, pipe);
    if (mini_slow(pipe == req->task.sent_to))
    {
        mini_fsm_action(&req->fsm, MINI_REQ_ACTION_PIPE_RM);
    }
}

struct mini_socktype mini_req_socktype = {
    AF_SP,
    MINI_REQ,
    0,
    mini_req_create,
    mini_xreq_ispeer,
};
