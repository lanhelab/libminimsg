/*
    Copyright (c) 2012-2013 Martin Sustrik  All rights reserved.

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#ifndef MINI_REP_INCLUDED
#define MINI_REP_INCLUDED

#include "../../protocol.h"
#include "xrep.h"

struct mini_rep
{
    struct mini_xrep xrep;
    uint32_t flags;
    struct mini_chunkref backtrace;
};

/*  Some users may want to extend the REP protocol similar to how REP extends XREP.
    Expose these methods to improve extensibility. */
void mini_rep_init(struct mini_rep *self,
                   const struct mini_sockbase_vfptr *vfptr, void *hint);
void mini_rep_term(struct mini_rep *self);

/*  Implementation of mini_sockbase's virtual functions. */
void mini_rep_destroy(struct mini_sockbase *self);
int mini_rep_events(struct mini_sockbase *self);
int mini_rep_send(struct mini_sockbase *self, struct mini_msg *msg);
int mini_rep_recv(struct mini_sockbase *self, struct mini_msg *msg);

#endif
