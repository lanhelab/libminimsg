/*
    Copyright (c) 2012-2013 Martin Sustrik  All rights reserved.

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "rep.h"
#include "xrep.h"

#include "../../minimsg.h"
#include "../../reqrep.h"

#include "../../utils/err.h"
#include "../../utils/cont.h"
#include "../../utils/alloc.h"
#include "../../utils/chunkref.h"
#include "../../utils/wire.h"

#include <stddef.h>
#include <string.h>

#define MINI_REP_INPROGRESS 1

static const struct mini_sockbase_vfptr mini_rep_sockbase_vfptr = {
    NULL,
    mini_rep_destroy,
    mini_xrep_add,
    mini_xrep_rm,
    mini_xrep_in,
    mini_xrep_out,
    mini_rep_events,
    mini_rep_send,
    mini_rep_recv,
    NULL,
    NULL};

void mini_rep_init(struct mini_rep *self,
                   const struct mini_sockbase_vfptr *vfptr, void *hint)
{
    mini_xrep_init(&self->xrep, vfptr, hint);
    self->flags = 0;
}

void mini_rep_term(struct mini_rep *self)
{
    if (self->flags & MINI_REP_INPROGRESS)
        mini_chunkref_term(&self->backtrace);
    mini_xrep_term(&self->xrep);
}

void mini_rep_destroy(struct mini_sockbase *self)
{
    struct mini_rep *rep;

    rep = mini_cont(self, struct mini_rep, xrep.sockbase);

    mini_rep_term(rep);
    mini_free(rep);
}

int mini_rep_events(struct mini_sockbase *self)
{
    struct mini_rep *rep;
    int events;

    rep = mini_cont(self, struct mini_rep, xrep.sockbase);
    events = mini_xrep_events(&rep->xrep.sockbase);
    if (!(rep->flags & MINI_REP_INPROGRESS))
        events &= ~MINI_SOCKBASE_EVENT_OUT;
    return events;
}

int mini_rep_send(struct mini_sockbase *self, struct mini_msg *msg)
{
    int rc;
    struct mini_rep *rep;

    rep = mini_cont(self, struct mini_rep, xrep.sockbase);

    /*  If no request was received, there's nowhere to send the reply to. */
    if (mini_slow(!(rep->flags & MINI_REP_INPROGRESS)))
        return -EFSM;

    /*  Move the stored backtrace into the message header. */
    mini_assert(mini_chunkref_size(&msg->sphdr) == 0);
    mini_chunkref_term(&msg->sphdr);
    mini_chunkref_mv(&msg->sphdr, &rep->backtrace);
    rep->flags &= ~MINI_REP_INPROGRESS;

    /*  Send the reply. If it cannot be sent because of pushback,
        drop it silently. */
    rc = mini_xrep_send(&rep->xrep.sockbase, msg);
    errnum_assert(rc == 0 || rc == -EAGAIN, -rc);

    return 0;
}

int mini_rep_recv(struct mini_sockbase *self, struct mini_msg *msg)
{
    int rc;
    struct mini_rep *rep;

    rep = mini_cont(self, struct mini_rep, xrep.sockbase);

    /*  If a request is already being processed, cancel it. */
    if (mini_slow(rep->flags & MINI_REP_INPROGRESS))
    {
        mini_chunkref_term(&rep->backtrace);
        rep->flags &= ~MINI_REP_INPROGRESS;
    }

    /*  Receive the request. */
    rc = mini_xrep_recv(&rep->xrep.sockbase, msg);
    if (mini_slow(rc == -EAGAIN))
        return -EAGAIN;
    errnum_assert(rc == 0, -rc);

    /*  Store the backtrace. */
    mini_chunkref_mv(&rep->backtrace, &msg->sphdr);
    mini_chunkref_init(&msg->sphdr, 0);
    rep->flags |= MINI_REP_INPROGRESS;

    return 0;
}

static int mini_rep_create(void *hint, struct mini_sockbase **sockbase)
{
    struct mini_rep *self;

    self = mini_alloc(sizeof(struct mini_rep), "socket (rep)");
    alloc_assert(self);
    mini_rep_init(self, &mini_rep_sockbase_vfptr, hint);
    *sockbase = &self->xrep.sockbase;

    return 0;
}

struct mini_socktype mini_rep_socktype = {
    AF_SP,
    MINI_REP,
    0,
    mini_rep_create,
    mini_xrep_ispeer,
};
