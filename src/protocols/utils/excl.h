/*
    Copyright (c) 2012-2013 Martin Sustrik  All rights reserved.

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#ifndef MINI_EXCL_INCLUDED
#define MINI_EXCL_INCLUDED

#include "../../protocol.h"

#include <stddef.h>

/*  This is an object to handle a single pipe. To be used by socket types that
    can work with precisely one connection, e.g. PAIR. */

struct mini_excl
{

    /*  The pipe being used at the moment. All other pipes will be rejected
        until this one terminates. NULL if there is no connected pipe. */
    struct mini_pipe *pipe;

    /*  Pipe ready for receiving. It's either equal to 'pipe' or NULL. */
    struct mini_pipe *inpipe;

    /*  Pipe ready for sending. It's either equal to 'pipe' or NULL. */
    struct mini_pipe *outpipe;
};

void mini_excl_init(struct mini_excl *self);
void mini_excl_term(struct mini_excl *self);
int mini_excl_add(struct mini_excl *self, struct mini_pipe *pipe);
void mini_excl_rm(struct mini_excl *self, struct mini_pipe *pipe);
void mini_excl_in(struct mini_excl *self, struct mini_pipe *pipe);
void mini_excl_out(struct mini_excl *self, struct mini_pipe *pipe);
int mini_excl_send(struct mini_excl *self, struct mini_msg *msg);
int mini_excl_recv(struct mini_excl *self, struct mini_msg *msg);
int mini_excl_can_send(struct mini_excl *self);
int mini_excl_can_recv(struct mini_excl *self);

#endif
