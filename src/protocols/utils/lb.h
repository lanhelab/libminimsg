/*
    Copyright (c) 2013 Martin Sustrik  All rights reserved.

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#ifndef MINI_LB_INCLUDED
#define MINI_LB_INCLUDED

#include "../../protocol.h"

#include "priolist.h"

/*  A load balancer. Round-robins messages to a set of pipes. */

struct mini_lb_data
{
    struct mini_priolist_data priodata;
};

struct mini_lb
{
    struct mini_priolist priolist;
};

void mini_lb_init(struct mini_lb *self);
void mini_lb_term(struct mini_lb *self);
void mini_lb_add(struct mini_lb *self, struct mini_lb_data *data,
                 struct mini_pipe *pipe, int priority);
void mini_lb_rm(struct mini_lb *self, struct mini_lb_data *data);
void mini_lb_out(struct mini_lb *self, struct mini_lb_data *data);
int mini_lb_can_send(struct mini_lb *self);
int mini_lb_get_priority(struct mini_lb *self);
int mini_lb_send(struct mini_lb *self, struct mini_msg *msg, struct mini_pipe **to);

#endif
