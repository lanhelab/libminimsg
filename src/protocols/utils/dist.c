/*
    Copyright (c) 2013 Martin Sustrik  All rights reserved.

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "dist.h"

#include "../../utils/err.h"
#include "../../utils/cont.h"
#include "../../utils/fast.h"
#include "../../utils/attr.h"

#include <stddef.h>

void mini_dist_init(struct mini_dist *self)
{
    self->count = 0;
    mini_list_init(&self->pipes);
}

void mini_dist_term(struct mini_dist *self)
{
    mini_assert(self->count == 0);
    mini_list_term(&self->pipes);
}

void mini_dist_add(MINI_UNUSED struct mini_dist *self,
                   struct mini_dist_data *data, struct mini_pipe *pipe)
{
    data->pipe = pipe;
    mini_list_item_init(&data->item);
}

void mini_dist_rm(struct mini_dist *self, struct mini_dist_data *data)
{
    if (mini_list_item_isinlist(&data->item))
    {
        --self->count;
        mini_list_erase(&self->pipes, &data->item);
    }
    mini_list_item_term(&data->item);
}

void mini_dist_out(struct mini_dist *self, struct mini_dist_data *data)
{
    ++self->count;
    mini_list_insert(&self->pipes, &data->item, mini_list_end(&self->pipes));
}

int mini_dist_send(struct mini_dist *self, struct mini_msg *msg,
                   struct mini_pipe *exclude)
{
    int rc;
    struct mini_list_item *it;
    struct mini_dist_data *data;
    struct mini_msg copy;

    /*  TODO: We can optimise for the case when there's only one outbound
        pipe here. No message copying is needed in such case. */

    /*  In the specific case when there are no outbound pipes. There's nowhere
        to send the message to. Deallocate it. */
    if (mini_slow(self->count) == 0)
    {
        mini_msg_term(msg);
        return 0;
    }

    /*  Send the message to all the subscribers. */
    mini_msg_bulkcopy_start(msg, self->count);
    it = mini_list_begin(&self->pipes);
    while (it != mini_list_end(&self->pipes))
    {
        data = mini_cont(it, struct mini_dist_data, item);
        mini_msg_bulkcopy_cp(&copy, msg);
        if (mini_fast(data->pipe == exclude))
        {
            mini_msg_term(&copy);
        }
        else
        {
            rc = mini_pipe_send(data->pipe, &copy);
            errnum_assert(rc >= 0, -rc);
            if (rc & MINI_PIPE_RELEASE)
            {
                --self->count;
                it = mini_list_erase(&self->pipes, it);
                continue;
            }
        }
        it = mini_list_next(&self->pipes, it);
    }
    mini_msg_term(msg);

    return 0;
}
