/*
    Copyright (c) 2013 Martin Sustrik  All rights reserved.

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#ifndef MINI_DIST_INCLUDED
#define MINI_DIST_INCLUDED

#include "../../protocol.h"

#include "../../utils/list.h"

/*  Distributor. Sends messages to all the pipes. */

struct mini_dist_data
{
    struct mini_list_item item;
    struct mini_pipe *pipe;
};

struct mini_dist
{
    uint32_t count;
    struct mini_list pipes;
};

void mini_dist_init(struct mini_dist *self);
void mini_dist_term(struct mini_dist *self);
void mini_dist_add(struct mini_dist *self,
                   struct mini_dist_data *data, struct mini_pipe *pipe);
void mini_dist_rm(struct mini_dist *self, struct mini_dist_data *data);
void mini_dist_out(struct mini_dist *self, struct mini_dist_data *data);

/*  Sends the message to all the attached pipes except the one specified
    by 'exclude' parameter. If 'exclude' is NULL, message is sent to all
    attached pipes. */
int mini_dist_send(struct mini_dist *self, struct mini_msg *msg,
                   struct mini_pipe *exclude);

#endif
