/*
    Copyright (c) 2012-2013 Martin Sustrik  All rights reserved.

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "excl.h"

#include "../../utils/fast.h"
#include "../../utils/err.h"
#include "../../utils/attr.h"

void mini_excl_init(struct mini_excl *self)
{
    self->pipe = NULL;
    self->inpipe = NULL;
    self->outpipe = NULL;
}

void mini_excl_term(struct mini_excl *self)
{
    mini_assert(!self->pipe);
    mini_assert(!self->inpipe);
    mini_assert(!self->outpipe);
}

int mini_excl_add(struct mini_excl *self, struct mini_pipe *pipe)
{
    /*  If there's a connection being used, reject any new connection. */
    if (self->pipe)
        return -EISCONN;

    /*  Remember that this pipe is the active one. */
    self->pipe = pipe;

    return 0;
}

void mini_excl_rm(struct mini_excl *self, MINI_UNUSED struct mini_pipe *pipe)
{
    mini_assert(self->pipe);
    self->pipe = NULL;
    self->inpipe = NULL;
    self->outpipe = NULL;
}

void mini_excl_in(struct mini_excl *self, struct mini_pipe *pipe)
{
    mini_assert(!self->inpipe);
    mini_assert(pipe == self->pipe);
    self->inpipe = pipe;
}

void mini_excl_out(struct mini_excl *self, struct mini_pipe *pipe)
{
    mini_assert(!self->outpipe);
    mini_assert(pipe == self->pipe);
    self->outpipe = pipe;
}

int mini_excl_send(struct mini_excl *self, struct mini_msg *msg)
{
    int rc;

    if (mini_slow(!self->outpipe))
        return -EAGAIN;

    rc = mini_pipe_send(self->outpipe, msg);
    errnum_assert(rc >= 0, -rc);

    if (rc & MINI_PIPE_RELEASE)
        self->outpipe = NULL;

    return rc & ~MINI_PIPE_RELEASE;
}

int mini_excl_recv(struct mini_excl *self, struct mini_msg *msg)
{
    int rc;

    if (mini_slow(!self->inpipe))
        return -EAGAIN;

    rc = mini_pipe_recv(self->inpipe, msg);
    errnum_assert(rc >= 0, -rc);

    if (rc & MINI_PIPE_RELEASE)
        self->inpipe = NULL;

    return rc & ~MINI_PIPE_RELEASE;
}

int mini_excl_can_send(struct mini_excl *self)
{
    return self->outpipe ? 1 : 0;
}

int mini_excl_can_recv(struct mini_excl *self)
{
    return self->inpipe ? 1 : 0;
}
