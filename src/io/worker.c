/*
    Copyright (c) 2013 Martin Sustrik  All rights reserved.

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "worker.h"

#if defined MINI_HAVE_WINDOWS
#include "worker_win.inc"
#else
#include "worker_posix.inc"
#endif

void mini_worker_timer_init(struct mini_worker_timer *self, struct mini_fsm *owner)
{
    self->owner = owner;
    mini_timerset_hndl_init(&self->hndl);
}

void mini_worker_timer_term(struct mini_worker_timer *self)
{
    mini_timerset_hndl_term(&self->hndl);
}

int mini_worker_timer_isactive(struct mini_worker_timer *self)
{
    return mini_timerset_hndl_isactive(&self->hndl);
}
