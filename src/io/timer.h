/*
    Copyright (c) 2013 Martin Sustrik  All rights reserved.

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#ifndef MINI_TIMER_INCLUDED
#define MINI_TIMER_INCLUDED

#include "fsm.h"
#include "worker.h"

#define MINI_TIMER_TIMEOUT 1
#define MINI_TIMER_STOPPED 2

struct mini_timer {
    struct mini_fsm fsm;
    int state;
    struct mini_worker_task start_task;
    struct mini_worker_task stop_task;
    struct mini_worker_timer wtimer;
    struct mini_fsm_event done;
    struct mini_worker *worker;
    int timeout;
};

void mini_timer_init (struct mini_timer *self, int src, struct mini_fsm *owner);
void mini_timer_term (struct mini_timer *self);

int mini_timer_isidle (struct mini_timer *self);
void mini_timer_start (struct mini_timer *self, int timeout);
void mini_timer_stop (struct mini_timer *self);

#endif
