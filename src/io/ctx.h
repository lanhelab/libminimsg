/*
    Copyright (c) 2013 Martin Sustrik  All rights reserved.

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#ifndef MINI_CTX_INCLUDED
#define MINI_CTX_INCLUDED

#include "../utils/mutex.h"
#include "../utils/queue.h"

#include "worker.h"
#include "pool.h"
#include "fsm.h"

/*  AIO context for objects using AIO subsystem. */

typedef void (*mini_ctx_onleave)(struct mini_ctx *self);

struct mini_ctx
{
    struct mini_mutex sync;
    struct mini_pool *pool;
    struct mini_queue events;
    struct mini_queue eventsto;
    mini_ctx_onleave onleave;
};

void mini_ctx_init(struct mini_ctx *self, struct mini_pool *pool,
                   mini_ctx_onleave onleave);
void mini_ctx_term(struct mini_ctx *self);

void mini_ctx_enter(struct mini_ctx *self);
void mini_ctx_leave(struct mini_ctx *self);

struct mini_worker *mini_ctx_choose_worker(struct mini_ctx *self);

void mini_ctx_raise(struct mini_ctx *self, struct mini_fsm_event *event);
void mini_ctx_raiseto(struct mini_ctx *self, struct mini_fsm_event *event);

#endif
