/*
    Copyright (c) 2013 Martin Sustrik  All rights reserved.
    Copyright (c) 2013 GoPivotal, Inc.  All rights reserved.

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#ifndef MINI_WORKER_INCLUDED
#define MINI_WORKER_INCLUDED

#include "fsm.h"
#include "timerset.h"

#if defined MINI_HAVE_WINDOWS
#include "worker_win.h"
#else
#include "worker_posix.h"
#endif

#define MINI_WORKER_TIMER_TIMEOUT 1

struct mini_worker_timer {
    struct mini_fsm *owner;
    struct mini_timerset_hndl hndl;
};

void mini_worker_timer_init (struct mini_worker_timer *self,
    struct mini_fsm *owner);
void mini_worker_timer_term (struct mini_worker_timer *self);
int mini_worker_timer_isactive (struct mini_worker_timer *self);

#define MINI_WORKER_TASK_EXECUTE 1

struct mini_worker_task;

void mini_worker_task_init (struct mini_worker_task *self, int src,
    struct mini_fsm *owner);
void mini_worker_task_term (struct mini_worker_task *self);

struct mini_worker;

int mini_worker_init (struct mini_worker *self);
void mini_worker_term (struct mini_worker *self);
void mini_worker_execute (struct mini_worker *self, struct mini_worker_task *task);
void mini_worker_cancel (struct mini_worker *self, struct mini_worker_task *task);

void mini_worker_add_timer (struct mini_worker *self, int timeout,
    struct mini_worker_timer *timer);
void mini_worker_rm_timer (struct mini_worker *self,
    struct mini_worker_timer *timer);

#endif

