/*
    Copyright (c) 2012 Martin Sustrik  All rights reserved.

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "../utils/fast.h"
#include "../utils/err.h"
#include "../utils/closefd.h"

#include <string.h>
#include <unistd.h>
#include <fcntl.h>

int mini_poller_init (struct mini_poller *self)
{
#ifndef EPOLL_CLOEXEC
    int rc;
#endif

#ifdef EPOLL_CLOEXEC
    self->ep = epoll_create1 (EPOLL_CLOEXEC);
#else
    /*  Size parameter is unused, we can safely set it to 1. */
    self->ep = epoll_create (1);
    rc = fcntl (self->ep, F_SETFD, FD_CLOEXEC);
    errno_assert (rc != -1);
#endif 
    if (self->ep == -1) {
        if (errno == ENFILE || errno == EMFILE)
            return -EMFILE;
        errno_assert (0);
    }
    self->nevents = 0;
    self->index = 0;

    return 0;
}

void mini_poller_term (struct mini_poller *self)
{
    mini_closefd (self->ep);
}

void mini_poller_add (struct mini_poller *self, int fd,
    struct mini_poller_hndl *hndl)
{
    struct epoll_event ev;

    /*  Initialise the handle and add the file descriptor to the pollset. */
    hndl->fd = fd;
    hndl->events = 0;
    memset (&ev, 0, sizeof (ev));
    ev.events = 0;
    ev.data.ptr = (void*) hndl;
    epoll_ctl (self->ep, EPOLL_CTL_ADD, fd, &ev);
}

void mini_poller_rm (struct mini_poller *self, struct mini_poller_hndl *hndl)
{
    int i;

    /*  Remove the file descriptor from the pollset. */
    epoll_ctl (self->ep, EPOLL_CTL_DEL, hndl->fd, NULL);

    /*  Invalidate any subsequent events on this file descriptor. */
    for (i = self->index; i != self->nevents; ++i)
        if (self->events [i].data.ptr == hndl)
            self->events [i].events = 0;
}

void mini_poller_set_in (struct mini_poller *self, struct mini_poller_hndl *hndl)
{
    struct epoll_event ev;

    /*  If already polling for IN, do nothing. */
    if (mini_slow (hndl->events & EPOLLIN))
        return;

    /*  Start polling for IN. */
    hndl->events |= EPOLLIN;
    memset (&ev, 0, sizeof (ev));
    ev.events = hndl->events;
    ev.data.ptr = (void*) hndl;
    epoll_ctl (self->ep, EPOLL_CTL_MOD, hndl->fd, &ev);
}

void mini_poller_reset_in (struct mini_poller *self, struct mini_poller_hndl *hndl)
{
    int i;
    struct epoll_event ev;

    /*  If not polling for IN, do nothing. */
    if (mini_slow (!(hndl->events & EPOLLIN)))
        return;

    /*  Stop polling for IN. */
    hndl->events &= ~EPOLLIN;
    memset (&ev, 0, sizeof (ev));
    ev.events = hndl->events;
    ev.data.ptr = (void*) hndl;
    epoll_ctl (self->ep, EPOLL_CTL_MOD, hndl->fd, &ev);

    /*  Invalidate any subsequent IN events on this file descriptor. */
    for (i = self->index; i != self->nevents; ++i)
        if (self->events [i].data.ptr == hndl)
            self->events [i].events &= ~EPOLLIN;
}

void mini_poller_set_out (struct mini_poller *self, struct mini_poller_hndl *hndl)
{
    struct epoll_event ev;
    int fd = hndl->fd;

    /*  If already polling for OUT, do nothing. */
    if (mini_slow (hndl->events & EPOLLOUT))
        return;

    /*  Start polling for OUT. */
    hndl->events |= EPOLLOUT;
    memset (&ev, 0, sizeof (ev));
    ev.events = hndl->events;
    ev.data.ptr = (void*) hndl;
    epoll_ctl (self->ep, EPOLL_CTL_MOD, fd, &ev);
}

void mini_poller_reset_out (struct mini_poller *self, struct mini_poller_hndl *hndl)
{
    int i;
    struct epoll_event ev;

    /*  If not polling for OUT, do nothing. */
    if (mini_slow (!(hndl->events & EPOLLOUT)))
        return;

    /*  Stop polling for OUT. */
    hndl->events &= ~EPOLLOUT;
    memset (&ev, 0, sizeof (ev));
    ev.events = hndl->events;
    ev.data.ptr = (void*) hndl;
    epoll_ctl (self->ep, EPOLL_CTL_MOD, hndl->fd, &ev);

    /*  Invalidate any subsequent OUT events on this file descriptor. */
    for (i = self->index; i != self->nevents; ++i)
        if (self->events [i].data.ptr == hndl)
            self->events [i].events &= ~EPOLLOUT;
}

int mini_poller_wait (struct mini_poller *self, int timeout)
{
    int nevents;

    /*  Clear all existing events. */
    self->nevents = 0;
    self->index = 0;

    /*  Wait for new events. */
    while (1) {
        nevents = epoll_wait (self->ep, self->events,
            MINI_POLLER_MAX_EVENTS, timeout);
        if (mini_slow (nevents == -1 && errno == EINTR))
            continue;
        break;
    }
    errno_assert (self->nevents != -1);
    self->nevents = nevents;
    return 0;
}

int mini_poller_event (struct mini_poller *self, int *event,
    struct mini_poller_hndl **hndl)
{
    /*  Skip over empty events. */
    while (self->index < self->nevents) {
        if (self->events [self->index].events != 0)
            break;
        ++self->index;
    }

    /*  If there is no stored event, let the caller know. */
    if (mini_slow (self->index >= self->nevents))
        return -EAGAIN;

    /*  Return next event to the caller. Remove the event from the set. */
    *hndl = (struct mini_poller_hndl*) self->events [self->index].data.ptr;
    if (mini_fast (self->events [self->index].events & EPOLLIN)) {
        *event = MINI_POLLER_IN;
        self->events [self->index].events &= ~EPOLLIN;
        return 0;
    }
    else if (mini_fast (self->events [self->index].events & EPOLLOUT)) {
        *event = MINI_POLLER_OUT;
        self->events [self->index].events &= ~EPOLLOUT;
        return 0;
    }
    else {
        *event = MINI_POLLER_ERR;
        ++self->index;
        return 0;
    }
}

