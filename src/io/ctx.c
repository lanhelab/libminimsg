/*
    Copyright (c) 2013 Martin Sustrik  All rights reserved.

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "ctx.h"

#include "../utils/err.h"
#include "../utils/cont.h"
#include "../utils/fast.h"

void mini_ctx_init(struct mini_ctx *self, struct mini_pool *pool,
                   mini_ctx_onleave onleave)
{
    mini_mutex_init(&self->sync);
    self->pool = pool;
    mini_queue_init(&self->events);
    mini_queue_init(&self->eventsto);
    self->onleave = onleave;
}

void mini_ctx_term(struct mini_ctx *self)
{
    mini_queue_term(&self->eventsto);
    mini_queue_term(&self->events);
    mini_mutex_term(&self->sync);
}

void mini_ctx_enter(struct mini_ctx *self)
{
    mini_mutex_lock(&self->sync);
}

void mini_ctx_leave(struct mini_ctx *self)
{
    struct mini_queue_item *item;
    struct mini_fsm_event *event;
    struct mini_queue eventsto;

    /*  Process any queued events before leaving the context. */
    while (1)
    {
        item = mini_queue_pop(&self->events);
        event = mini_cont(item, struct mini_fsm_event, item);
        if (!event)
            break;
        mini_fsm_event_process(event);
    }

    /*  Notify the owner that we are leaving the context. */
    if (mini_fast(self->onleave != NULL))
        self->onleave(self);

    /*  Shortcut in the case there are no external events. */
    if (mini_queue_empty(&self->eventsto))
    {
        mini_mutex_unlock(&self->sync);
        return;
    }

    /*  Make a copy of the queue of the external events so that it does not
        get corrupted once we unlock the context. */
    eventsto = self->eventsto;
    mini_queue_init(&self->eventsto);

    mini_mutex_unlock(&self->sync);

    /*  Process any queued external events. Before processing each event
        lock the context it belongs to. */
    while (1)
    {
        item = mini_queue_pop(&eventsto);
        event = mini_cont(item, struct mini_fsm_event, item);
        if (!event)
            break;
        mini_ctx_enter(event->fsm->ctx);
        mini_fsm_event_process(event);
        mini_ctx_leave(event->fsm->ctx);
    }

    mini_queue_term(&eventsto);
}

struct mini_worker *mini_ctx_choose_worker(struct mini_ctx *self)
{
    return mini_pool_choose_worker(self->pool);
}

void mini_ctx_raise(struct mini_ctx *self, struct mini_fsm_event *event)
{
    mini_queue_push(&self->events, &event->item);
}

void mini_ctx_raiseto(struct mini_ctx *self, struct mini_fsm_event *event)
{
    mini_queue_push(&self->eventsto, &event->item);
}
