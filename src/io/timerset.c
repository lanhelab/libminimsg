/*
    Copyright (c) 2012-2013 Martin Sustrik  All rights reserved.

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "timerset.h"

#include "../utils/fast.h"
#include "../utils/cont.h"
#include "../utils/clock.h"
#include "../utils/err.h"

void mini_timerset_init(struct mini_timerset *self)
{
    mini_list_init(&self->timeouts);
}

void mini_timerset_term(struct mini_timerset *self)
{
    mini_list_term(&self->timeouts);
}

int mini_timerset_add(struct mini_timerset *self, int timeout,
                      struct mini_timerset_hndl *hndl)
{
    struct mini_list_item *it;
    struct mini_timerset_hndl *ith;
    int first;

    /*  Compute the instant when the timeout will be due. */
    hndl->timeout = mini_clock_ms() + timeout;

    /*  Insert it into the ordered list of timeouts. */
    for (it = mini_list_begin(&self->timeouts);
         it != mini_list_end(&self->timeouts);
         it = mini_list_next(&self->timeouts, it))
    {
        ith = mini_cont(it, struct mini_timerset_hndl, list);
        if (hndl->timeout < ith->timeout)
            break;
    }

    /*  If the new timeout happens to be the first one to expire, let the user
        know that the current waiting interval has to be changed. */
    first = mini_list_begin(&self->timeouts) == it ? 1 : 0;
    mini_list_insert(&self->timeouts, &hndl->list, it);
    return first;
}

int mini_timerset_rm(struct mini_timerset *self, struct mini_timerset_hndl *hndl)
{
    int first;

    /*  Ignore if handle is not in the timeouts list. */
    if (!mini_list_item_isinlist(&hndl->list))
        return 0;

    /*  If it was the first timeout that was removed, the actual waiting time
        may have changed. We'll thus return 1 to let the user know. */
    first = mini_list_begin(&self->timeouts) == &hndl->list ? 1 : 0;
    mini_list_erase(&self->timeouts, &hndl->list);
    return first;
}

int mini_timerset_timeout(struct mini_timerset *self)
{
    int timeout;

    if (mini_fast(mini_list_empty(&self->timeouts)))
        return -1;

    timeout = (int)(mini_cont(mini_list_begin(&self->timeouts),
                              struct mini_timerset_hndl, list)
                        ->timeout -
                    mini_clock_ms());
    return timeout < 0 ? 0 : timeout;
}

int mini_timerset_event(struct mini_timerset *self, struct mini_timerset_hndl **hndl)
{
    struct mini_timerset_hndl *first;

    /*  If there's no timeout, there's no event to report. */
    if (mini_fast(mini_list_empty(&self->timeouts)))
        return -EAGAIN;

    /*  If no timeout have expired yet, there's no event to return. */
    first = mini_cont(mini_list_begin(&self->timeouts),
                      struct mini_timerset_hndl, list);
    if (first->timeout > mini_clock_ms())
        return -EAGAIN;

    /*  Return the first timeout and remove it from the list of active
        timeouts. */
    mini_list_erase(&self->timeouts, &first->list);
    *hndl = first;
    return 0;
}

void mini_timerset_hndl_init(struct mini_timerset_hndl *self)
{
    mini_list_item_init(&self->list);
}

void mini_timerset_hndl_term(struct mini_timerset_hndl *self)
{
    mini_list_item_term(&self->list);
}

int mini_timerset_hndl_isactive(struct mini_timerset_hndl *self)
{
    return mini_list_item_isinlist(&self->list);
}
