/*
    Copyright (c) 2013 Martin Sustrik  All rights reserved.
    Copyright 2018 Staysail Systems, Inc. <info@staysail.tech>
    Copyright 2018 Capitar IT Group BV <info@capitar.com>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "fsm.h"
#include "timerset.h"

#include "../utils/win.h"
#include "../utils/thread.h"

struct mini_worker_task
{
    int src;
    struct mini_fsm *owner;
};

#define MINI_WORKER_OP_DONE 1
#define MINI_WORKER_OP_ERROR 2

struct mini_worker_op
{
    int src;
    struct mini_fsm *owner;
    int state;

    /*  This structure is to be used by the user, not mini_worker_op itself.
        Actual usage is specific to the asynchronous operation in question. */
    OVERLAPPED olpd;

    /*  We might have transferred less than requested.  This keeps track. */
    size_t resid;
    char *buf;
    void *arg;
    void (*start)(struct mini_usock *);
    int zero_is_error;
};

void mini_worker_op_init(struct mini_worker_op *self, int src,
                         struct mini_fsm *owner);
void mini_worker_op_term(struct mini_worker_op *self);

/*  Call this function when asynchronous operation is started. */
void mini_worker_op_start(struct mini_worker_op *self);

int mini_worker_op_isidle(struct mini_worker_op *self);

struct mini_worker
{
    HANDLE cp;
    struct mini_timerset timerset;
    struct mini_thread thread;
};

HANDLE mini_worker_getcp(struct mini_worker *self);
