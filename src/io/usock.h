/*
    Copyright (c) 2013 Martin Sustrik  All rights reserved.

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#ifndef MINI_USOCK_INCLUDED
#define MINI_USOCK_INCLUDED

/*  Import the definition of mini_iovec. */
#include "minimsg.h"

/*  OS-level sockets. */

/*  Event types generated by mini_usock. */
#define MINI_USOCK_CONNECTED 1
#define MINI_USOCK_ACCEPTED 2
#define MINI_USOCK_SENT 3
#define MINI_USOCK_RECEIVED 4
#define MINI_USOCK_ERROR 5
#define MINI_USOCK_ACCEPT_ERROR 6
#define MINI_USOCK_STOPPED 7
#define MINI_USOCK_SHUTDOWN 8

/*  Maximum number of iovecs that can be passed to mini_usock_send function. */
#define MINI_USOCK_MAX_IOVCNT 3

/*  Size of the buffer used for batch-reads of inbound data. To keep the
    performance optimal make sure that this value is larger than network MTU. */
#define MINI_USOCK_BATCH_SIZE 2048

#if defined MINI_HAVE_WINDOWS
#include "usock_win.h"
#else
#include "usock_posix.h"
#endif

void mini_usock_init(struct mini_usock *self, int src, struct mini_fsm *owner);
void mini_usock_term(struct mini_usock *self);

int mini_usock_isidle(struct mini_usock *self);
int mini_usock_start(struct mini_usock *self,
                     int domain, int type, int protocol);
void mini_usock_start_fd(struct mini_usock *self, int fd);
void mini_usock_stop(struct mini_usock *self);

void mini_usock_swap_owner(struct mini_usock *self, struct mini_fsm_owner *owner);

int mini_usock_setsockopt(struct mini_usock *self, int level, int optname,
                          const void *optval, size_t optlen);

int mini_usock_bind(struct mini_usock *self, const struct sockaddr *addr,
                    size_t addrlen);
int mini_usock_listen(struct mini_usock *self, int backlog);

/*  Accept a new connection from a listener. When done, MINI_USOCK_ACCEPTED
    event will be delivered to the accepted socket. To cancel the operation,
    stop the socket being accepted. Listening socket should not be stopped
    while accepting a new socket is underway. */
void mini_usock_accept(struct mini_usock *self, struct mini_usock *listener);

/*  When all the tuning is done on the accepted socket, call this function
    to activate standard data transfer phase. */
void mini_usock_activate(struct mini_usock *self);

/*  Start connecting. Prior to this call the socket has to be bound to a local
    address. When connecting is done MINI_USOCK_CONNECTED event will be reaised.
    If connecting fails MINI_USOCK_ERROR event will be raised. */
void mini_usock_connect(struct mini_usock *self, const struct sockaddr *addr,
                        size_t addrlen);

void mini_usock_send(struct mini_usock *self, const struct mini_iovec *iov,
                     int iovcnt);
void mini_usock_recv(struct mini_usock *self, void *buf, size_t len, int *fd);

int mini_usock_geterrno(struct mini_usock *self);

#endif
