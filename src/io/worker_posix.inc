/*
    Copyright (c) 2013-2014 Martin Sustrik  All rights reserved.
    Copyright (c) 2013 GoPivotal, Inc.  All rights reserved.
    Copyright 2016 Garrett D'Amore <garrett@damore.org>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "ctx.h"

#include "../utils/err.h"
#include "../utils/fast.h"
#include "../utils/cont.h"
#include "../utils/attr.h"
#include "../utils/queue.h"

/*  Private functions. */
static void mini_worker_routine (void *arg);

void mini_worker_fd_init (struct mini_worker_fd *self, int src,
    struct mini_fsm *owner)
{
    self->src = src;
    self->owner = owner;
}

void mini_worker_fd_term (MINI_UNUSED struct mini_worker_fd *self)
{
}

void mini_worker_add_fd (struct mini_worker *self, int s, struct mini_worker_fd *fd)
{
    mini_poller_add (&self->poller, s, &fd->hndl);
}

void mini_worker_rm_fd (struct mini_worker *self, struct mini_worker_fd *fd)
{
    mini_poller_rm (&self->poller, &fd->hndl);
}

void mini_worker_set_in (struct mini_worker *self, struct mini_worker_fd *fd)
{
    mini_poller_set_in (&self->poller, &fd->hndl);
}

void mini_worker_reset_in (struct mini_worker *self, struct mini_worker_fd *fd)
{
    mini_poller_reset_in (&self->poller, &fd->hndl);
}

void mini_worker_set_out (struct mini_worker *self, struct mini_worker_fd *fd)
{
    mini_poller_set_out (&self->poller, &fd->hndl);
}

void mini_worker_reset_out (struct mini_worker *self, struct mini_worker_fd *fd)
{
    mini_poller_reset_out (&self->poller, &fd->hndl);
}

void mini_worker_add_timer (struct mini_worker *self, int timeout,
    struct mini_worker_timer *timer)
{
    mini_timerset_add (&self->timerset, timeout, &timer->hndl);
}

void mini_worker_rm_timer (struct mini_worker *self, struct mini_worker_timer *timer)
{
    mini_timerset_rm (&self->timerset, &timer->hndl);
}

void mini_worker_task_init (struct mini_worker_task *self, int src,
    struct mini_fsm *owner)
{
    self->src = src;
    self->owner = owner;
    mini_queue_item_init (&self->item);
}

void mini_worker_task_term (struct mini_worker_task *self)
{
    mini_queue_item_term (&self->item);
}

int mini_worker_init (struct mini_worker *self)
{
    int rc;

    rc = mini_efd_init (&self->efd);
    if (rc < 0)
        return rc;

    mini_mutex_init (&self->sync);
    mini_queue_init (&self->tasks);
    mini_queue_item_init (&self->stop);
    mini_poller_init (&self->poller);
    mini_poller_add (&self->poller, mini_efd_getfd (&self->efd), &self->efd_hndl);
    mini_poller_set_in (&self->poller, &self->efd_hndl);
    mini_timerset_init (&self->timerset);
    mini_thread_init (&self->thread, mini_worker_routine, self);

    return 0;
}

void mini_worker_term (struct mini_worker *self)
{
    /*  Ask worker thread to terminate. */
    mini_mutex_lock (&self->sync);
    mini_queue_push (&self->tasks, &self->stop);
    mini_efd_signal (&self->efd);
    mini_mutex_unlock (&self->sync);

    /*  Wait till worker thread terminates. */
    mini_thread_term (&self->thread);

    /*  Clean up. */
    mini_timerset_term (&self->timerset);
    mini_poller_term (&self->poller);
    mini_efd_term (&self->efd);
    mini_queue_item_term (&self->stop);
    mini_queue_term (&self->tasks);
    mini_mutex_term (&self->sync);
}

void mini_worker_execute (struct mini_worker *self, struct mini_worker_task *task)
{
    mini_mutex_lock (&self->sync);
    mini_queue_push (&self->tasks, &task->item);
    mini_efd_signal (&self->efd);
    mini_mutex_unlock (&self->sync);
}

void mini_worker_cancel (struct mini_worker *self, struct mini_worker_task *task)
{
    mini_mutex_lock (&self->sync);
    mini_queue_remove (&self->tasks, &task->item);
    mini_mutex_unlock (&self->sync);
}

static void mini_worker_routine (void *arg)
{
    int rc;
    struct mini_worker *self;
    int pevent;
    struct mini_poller_hndl *phndl;
    struct mini_timerset_hndl *thndl;
    struct mini_queue tasks;
    struct mini_queue_item *item;
    struct mini_worker_task *task;
    struct mini_worker_fd *fd;
    struct mini_worker_timer *timer;

    self = (struct mini_worker*) arg;

    /*  Infinite loop. It will be interrupted only when the object is
        shut down. */
    while (1) {

        /*  Wait for new events and/or timeouts. */
        rc = mini_poller_wait (&self->poller,
            mini_timerset_timeout (&self->timerset));
        errnum_assert (rc == 0, -rc);

        /*  Process all expired timers. */
        while (1) {
            rc = mini_timerset_event (&self->timerset, &thndl);
            if (rc == -EAGAIN)
                break;
            errnum_assert (rc == 0, -rc);
            timer = mini_cont (thndl, struct mini_worker_timer, hndl);
            mini_ctx_enter (timer->owner->ctx);
            mini_fsm_feed (timer->owner, -1, MINI_WORKER_TIMER_TIMEOUT, timer);
            mini_ctx_leave (timer->owner->ctx);
        }

        /*  Process all events from the poller. */
        while (1) {

            /*  Get next poller event, such as IN or OUT. */
            rc = mini_poller_event (&self->poller, &pevent, &phndl);
            if (mini_slow (rc == -EAGAIN))
                break;

            /*  If there are any new incoming worker tasks, process them. */
            if (phndl == &self->efd_hndl) {
                mini_assert (pevent == MINI_POLLER_IN);

                /*  Make a local copy of the task queue. This way
                    the application threads are not blocked and can post new
                    tasks while the existing tasks are being processed. Also,
                    new tasks can be posted from within task handlers. */
                mini_mutex_lock (&self->sync);
                mini_efd_unsignal (&self->efd);
                memcpy (&tasks, &self->tasks, sizeof (tasks));
                mini_queue_init (&self->tasks);
                mini_mutex_unlock (&self->sync);

                while (1) {

                    /*  Next worker task. */
                    item = mini_queue_pop (&tasks);
                    if (mini_slow (!item))
                        break;

                    /*  If the worker thread is asked to stop, do so. */
                    if (mini_slow (item == &self->stop)) {
                        /*  Make sure we remove all the other workers from
                            the queue, because we're not doing anything with
                            them. */
                        while (mini_queue_pop (&tasks) != NULL) {
                            continue;
                        }
                        mini_queue_term (&tasks);
                        return;
                    }

                    /*  It's a user-defined task. Notify the user that it has
                        arrived in the worker thread. */
                    task = mini_cont (item, struct mini_worker_task, item);
                    mini_ctx_enter (task->owner->ctx);
                    mini_fsm_feed (task->owner, task->src,
                        MINI_WORKER_TASK_EXECUTE, task);
                    mini_ctx_leave (task->owner->ctx);
                }
                mini_queue_term (&tasks);
                continue;
            }

            /*  It's a true I/O event. Invoke the handler. */
            fd = mini_cont (phndl, struct mini_worker_fd, hndl);
            mini_ctx_enter (fd->owner->ctx);
            mini_fsm_feed (fd->owner, fd->src, pevent, fd);
            mini_ctx_leave (fd->owner->ctx);
        }
    }
}

