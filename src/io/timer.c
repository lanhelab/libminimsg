/*
    Copyright (c) 2013 Martin Sustrik  All rights reserved.
    Copyright (c) 2013 GoPivotal, Inc.  All rights reserved.

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "timer.h"

#include "../utils/cont.h"
#include "../utils/fast.h"
#include "../utils/err.h"
#include "../utils/attr.h"

/*  Timer state reflects the state as seen by the user thread. It says nothing
    about the state of affairs in the worker thread. */
#define MINI_TIMER_STATE_IDLE 1
#define MINI_TIMER_STATE_ACTIVE 2
#define MINI_TIMER_STATE_STOPPING 3

#define MINI_TIMER_SRC_START_TASK 1
#define MINI_TIMER_SRC_STOP_TASK 2

/*  Private functions. */
static void mini_timer_handler(struct mini_fsm *self, int src, int type,
                               void *srcptr);
static void mini_timer_shutdown(struct mini_fsm *self, int src, int type,
                                void *srcptr);

void mini_timer_init(struct mini_timer *self, int src, struct mini_fsm *owner)
{
    mini_fsm_init(&self->fsm, mini_timer_handler, mini_timer_shutdown,
                  src, self, owner);
    self->state = MINI_TIMER_STATE_IDLE;
    mini_worker_task_init(&self->start_task, MINI_TIMER_SRC_START_TASK,
                          &self->fsm);
    mini_worker_task_init(&self->stop_task, MINI_TIMER_SRC_STOP_TASK, &self->fsm);
    mini_worker_timer_init(&self->wtimer, &self->fsm);
    mini_fsm_event_init(&self->done);
    self->worker = mini_fsm_choose_worker(&self->fsm);
    self->timeout = -1;
}

void mini_timer_term(struct mini_timer *self)
{
    mini_assert_state(self, MINI_TIMER_STATE_IDLE);

    mini_fsm_event_term(&self->done);
    mini_worker_timer_term(&self->wtimer);
    mini_worker_task_term(&self->stop_task);
    mini_worker_task_term(&self->start_task);
    mini_fsm_term(&self->fsm);
}

int mini_timer_isidle(struct mini_timer *self)
{
    return mini_fsm_isidle(&self->fsm);
}

void mini_timer_start(struct mini_timer *self, int timeout)
{
    /*  Negative timeout make no sense. */
    mini_assert(timeout >= 0);

    self->timeout = timeout;
    mini_fsm_start(&self->fsm);
}

void mini_timer_stop(struct mini_timer *self)
{
    mini_fsm_stop(&self->fsm);
}

static void mini_timer_shutdown(struct mini_fsm *self, int src, int type,
                                MINI_UNUSED void *srcptr)
{
    struct mini_timer *timer;

    timer = mini_cont(self, struct mini_timer, fsm);

    if (mini_slow(src == MINI_FSM_ACTION && type == MINI_FSM_STOP))
    {
        timer->state = MINI_TIMER_STATE_STOPPING;
        mini_worker_execute(timer->worker, &timer->stop_task);
        return;
    }
    if (mini_slow(timer->state == MINI_TIMER_STATE_STOPPING))
    {
        if (src != MINI_TIMER_SRC_STOP_TASK)
            return;
        mini_assert(type == MINI_WORKER_TASK_EXECUTE);
        mini_worker_rm_timer(timer->worker, &timer->wtimer);
        timer->state = MINI_TIMER_STATE_IDLE;
        mini_fsm_stopped(&timer->fsm, MINI_TIMER_STOPPED);
        return;
    }

    mini_fsm_bad_state(timer->state, src, type);
}

static void mini_timer_handler(struct mini_fsm *self, int src, int type,
                               void *srcptr)
{
    struct mini_timer *timer;

    timer = mini_cont(self, struct mini_timer, fsm);

    switch (timer->state)
    {

        /******************************************************************************/
        /*  IDLE state.                                                               */
        /******************************************************************************/
    case MINI_TIMER_STATE_IDLE:
        switch (src)
        {
        case MINI_FSM_ACTION:
            switch (type)
            {
            case MINI_FSM_START:

                /*  Send start event to the worker thread. */
                timer->state = MINI_TIMER_STATE_ACTIVE;
                mini_worker_execute(timer->worker, &timer->start_task);
                return;
            default:
                mini_fsm_bad_action(timer->state, src, type);
            }
        default:
            mini_fsm_bad_source(timer->state, src, type);
        }

        /******************************************************************************/
        /*  ACTIVE state.                                                             */
        /******************************************************************************/
    case MINI_TIMER_STATE_ACTIVE:
        if (src == MINI_TIMER_SRC_START_TASK)
        {
            mini_assert(type == MINI_WORKER_TASK_EXECUTE);
            mini_assert(timer->timeout >= 0);
            mini_worker_add_timer(timer->worker, timer->timeout,
                                  &timer->wtimer);
            timer->timeout = -1;
            return;
        }
        if (srcptr == &timer->wtimer)
        {
            switch (type)
            {
            case MINI_WORKER_TIMER_TIMEOUT:

                /*  Notify the user about the timeout. */
                mini_assert(timer->timeout == -1);
                mini_fsm_raise(&timer->fsm, &timer->done, MINI_TIMER_TIMEOUT);
                return;

            default:
                mini_fsm_bad_action(timer->state, src, type);
            }
        }
        mini_fsm_bad_source(timer->state, src, type);

        /******************************************************************************/
        /*  Invalid state.                                                            */
        /******************************************************************************/
    default:
        mini_fsm_bad_state(timer->state, src, type);
    }
}
