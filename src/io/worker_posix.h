/*
    Copyright (c) 2013 Martin Sustrik  All rights reserved.
    Copyright (c) 2013 GoPivotal, Inc.  All rights reserved.

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "../utils/queue.h"
#include "../utils/mutex.h"
#include "../utils/thread.h"
#include "../utils/efd.h"

#include "poller.h"

#define MINI_WORKER_FD_IN MINI_POLLER_IN
#define MINI_WORKER_FD_OUT MINI_POLLER_OUT
#define MINI_WORKER_FD_ERR MINI_POLLER_ERR

struct mini_worker_fd
{
    int src;
    struct mini_fsm *owner;
    struct mini_poller_hndl hndl;
};

void mini_worker_fd_init(struct mini_worker_fd *self, int src,
                         struct mini_fsm *owner);
void mini_worker_fd_term(struct mini_worker_fd *self);

struct mini_worker_task
{
    int src;
    struct mini_fsm *owner;
    struct mini_queue_item item;
};

struct mini_worker
{
    struct mini_mutex sync;
    struct mini_queue tasks;
    struct mini_queue_item stop;
    struct mini_efd efd;
    struct mini_poller poller;
    struct mini_poller_hndl efd_hndl;
    struct mini_timerset timerset;
    struct mini_thread thread;
};

void mini_worker_add_fd(struct mini_worker *self, int s, struct mini_worker_fd *fd);
void mini_worker_rm_fd(struct mini_worker *self, struct mini_worker_fd *fd);
void mini_worker_set_in(struct mini_worker *self, struct mini_worker_fd *fd);
void mini_worker_reset_in(struct mini_worker *self, struct mini_worker_fd *fd);
void mini_worker_set_out(struct mini_worker *self, struct mini_worker_fd *fd);
void mini_worker_reset_out(struct mini_worker *self, struct mini_worker_fd *fd);
