/*
    Copyright (c) 2013 Martin Sustrik  All rights reserved.
    Copyright 2016 Garrett D'Amore <garrett@damore.org>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "stcp.h"

#include "../../utils/err.h"
#include "../../utils/cont.h"
#include "../../utils/fast.h"
#include "../../utils/wire.h"
#include "../../utils/attr.h"

/*  States of the object as a whole. */
#define MINI_STCP_STATE_IDLE 1
#define MINI_STCP_STATE_PROTOHDR 2
#define MINI_STCP_STATE_STOPPING_STREAMHDR 3
#define MINI_STCP_STATE_ACTIVE 4
#define MINI_STCP_STATE_SHUTTING_DOWN 5
#define MINI_STCP_STATE_DONE 6
#define MINI_STCP_STATE_STOPPING 7

/*  Possible states of the inbound part of the object. */
#define MINI_STCP_INSTATE_HDR 1
#define MINI_STCP_INSTATE_BODY 2
#define MINI_STCP_INSTATE_HASMSG 3

/*  Possible states of the outbound part of the object. */
#define MINI_STCP_OUTSTATE_IDLE 1
#define MINI_STCP_OUTSTATE_SENDING 2

/*  Subordinate srcptr objects. */
#define MINI_STCP_SRC_USOCK 1
#define MINI_STCP_SRC_STREAMHDR 2

/*  Stream is a special type of pipe. Implementation of the virtual pipe API. */
static int mini_stcp_send(struct mini_pipebase *self, struct mini_msg *msg);
static int mini_stcp_recv(struct mini_pipebase *self, struct mini_msg *msg);
const struct mini_pipebase_vfptr mini_stcp_pipebase_vfptr = {
    mini_stcp_send,
    mini_stcp_recv};

/*  Private functions. */
static void mini_stcp_handler(struct mini_fsm *self, int src, int type,
                              void *srcptr);
static void mini_stcp_shutdown(struct mini_fsm *self, int src, int type,
                               void *srcptr);

void mini_stcp_init(struct mini_stcp *self, int src,
                    struct mini_ep *ep, struct mini_fsm *owner)
{
    mini_fsm_init(&self->fsm, mini_stcp_handler, mini_stcp_shutdown,
                  src, self, owner);
    self->state = MINI_STCP_STATE_IDLE;
    mini_streamhdr_init(&self->streamhdr, MINI_STCP_SRC_STREAMHDR, &self->fsm);
    self->usock = NULL;
    self->usock_owner.src = -1;
    self->usock_owner.fsm = NULL;
    mini_pipebase_init(&self->pipebase, &mini_stcp_pipebase_vfptr, ep);
    self->instate = -1;
    mini_msg_init(&self->inmsg, 0);
    self->outstate = -1;
    mini_msg_init(&self->outmsg, 0);
    mini_fsm_event_init(&self->done);
}

void mini_stcp_term(struct mini_stcp *self)
{
    mini_assert_state(self, MINI_STCP_STATE_IDLE);

    mini_fsm_event_term(&self->done);
    mini_msg_term(&self->outmsg);
    mini_msg_term(&self->inmsg);
    mini_pipebase_term(&self->pipebase);
    mini_streamhdr_term(&self->streamhdr);
    mini_fsm_term(&self->fsm);
}

int mini_stcp_isidle(struct mini_stcp *self)
{
    return mini_fsm_isidle(&self->fsm);
}

void mini_stcp_start(struct mini_stcp *self, struct mini_usock *usock)
{
    /*  Take ownership of the underlying socket. */
    mini_assert(self->usock == NULL && self->usock_owner.fsm == NULL);
    self->usock_owner.src = MINI_STCP_SRC_USOCK;
    self->usock_owner.fsm = &self->fsm;
    mini_usock_swap_owner(usock, &self->usock_owner);
    self->usock = usock;

    /*  Launch the state machine. */
    mini_fsm_start(&self->fsm);
}

void mini_stcp_stop(struct mini_stcp *self)
{
    mini_fsm_stop(&self->fsm);
}

static int mini_stcp_send(struct mini_pipebase *self, struct mini_msg *msg)
{
    struct mini_stcp *stcp;
    struct mini_iovec iov[3];

    stcp = mini_cont(self, struct mini_stcp, pipebase);

    mini_assert_state(stcp, MINI_STCP_STATE_ACTIVE);
    mini_assert(stcp->outstate == MINI_STCP_OUTSTATE_IDLE);

    /*  Move the message to the local storage. */
    mini_msg_term(&stcp->outmsg);
    mini_msg_mv(&stcp->outmsg, msg);

    /*  Serialise the message header. */
    mini_putll(stcp->outhdr, mini_chunkref_size(&stcp->outmsg.sphdr) +
                                 mini_chunkref_size(&stcp->outmsg.body));

    /*  Start async sending. */
    iov[0].iov_base = stcp->outhdr;
    iov[0].iov_len = sizeof(stcp->outhdr);
    iov[1].iov_base = mini_chunkref_data(&stcp->outmsg.sphdr);
    iov[1].iov_len = mini_chunkref_size(&stcp->outmsg.sphdr);
    iov[2].iov_base = mini_chunkref_data(&stcp->outmsg.body);
    iov[2].iov_len = mini_chunkref_size(&stcp->outmsg.body);
    mini_usock_send(stcp->usock, iov, 3);

    stcp->outstate = MINI_STCP_OUTSTATE_SENDING;

    return 0;
}

static int mini_stcp_recv(struct mini_pipebase *self, struct mini_msg *msg)
{
    struct mini_stcp *stcp;

    stcp = mini_cont(self, struct mini_stcp, pipebase);

    mini_assert_state(stcp, MINI_STCP_STATE_ACTIVE);
    mini_assert(stcp->instate == MINI_STCP_INSTATE_HASMSG);

    /*  Move received message to the user. */
    mini_msg_mv(msg, &stcp->inmsg);
    mini_msg_init(&stcp->inmsg, 0);

    /*  Start receiving new message. */
    stcp->instate = MINI_STCP_INSTATE_HDR;
    mini_usock_recv(stcp->usock, stcp->inhdr, sizeof(stcp->inhdr), NULL);

    return 0;
}

static void mini_stcp_shutdown(struct mini_fsm *self, int src, int type,
                               MINI_UNUSED void *srcptr)
{
    struct mini_stcp *stcp;

    stcp = mini_cont(self, struct mini_stcp, fsm);

    if (mini_slow(src == MINI_FSM_ACTION && type == MINI_FSM_STOP))
    {
        mini_pipebase_stop(&stcp->pipebase);
        mini_streamhdr_stop(&stcp->streamhdr);
        stcp->state = MINI_STCP_STATE_STOPPING;
    }
    if (mini_slow(stcp->state == MINI_STCP_STATE_STOPPING))
    {
        if (mini_streamhdr_isidle(&stcp->streamhdr))
        {
            mini_usock_swap_owner(stcp->usock, &stcp->usock_owner);
            stcp->usock = NULL;
            stcp->usock_owner.src = -1;
            stcp->usock_owner.fsm = NULL;
            stcp->state = MINI_STCP_STATE_IDLE;
            mini_fsm_stopped(&stcp->fsm, MINI_STCP_STOPPED);
            return;
        }
        return;
    }

    mini_fsm_bad_state(stcp->state, src, type);
}

static void mini_stcp_handler(struct mini_fsm *self, int src, int type,
                              MINI_UNUSED void *srcptr)
{
    int rc;
    struct mini_stcp *stcp;
    uint64_t size;
    int opt;
    size_t opt_sz = sizeof(opt);

    stcp = mini_cont(self, struct mini_stcp, fsm);

    switch (stcp->state)
    {

        /******************************************************************************/
        /*  IDLE state.                                                               */
        /******************************************************************************/
    case MINI_STCP_STATE_IDLE:
        switch (src)
        {

        case MINI_FSM_ACTION:
            switch (type)
            {
            case MINI_FSM_START:
                mini_streamhdr_start(&stcp->streamhdr, stcp->usock,
                                     &stcp->pipebase);
                stcp->state = MINI_STCP_STATE_PROTOHDR;
                return;
            default:
                mini_fsm_bad_action(stcp->state, src, type);
            }

        default:
            mini_fsm_bad_source(stcp->state, src, type);
        }

        /******************************************************************************/
        /*  PROTOHDR state.                                                           */
        /******************************************************************************/
    case MINI_STCP_STATE_PROTOHDR:
        switch (src)
        {

        case MINI_STCP_SRC_STREAMHDR:
            switch (type)
            {
            case MINI_STREAMHDR_OK:

                /*  Before moving to the active state stop the streamhdr
                    state machine. */
                mini_streamhdr_stop(&stcp->streamhdr);
                stcp->state = MINI_STCP_STATE_STOPPING_STREAMHDR;
                return;

            case MINI_STREAMHDR_ERROR:

                /* Raise the error and move directly to the DONE state.
                   streamhdr object will be stopped later on. */
                stcp->state = MINI_STCP_STATE_DONE;
                mini_fsm_raise(&stcp->fsm, &stcp->done, MINI_STCP_ERROR);
                return;

            default:
                mini_fsm_bad_action(stcp->state, src, type);
            }

        default:
            mini_fsm_bad_source(stcp->state, src, type);
        }

        /******************************************************************************/
        /*  STOPPING_STREAMHDR state.                                                 */
        /******************************************************************************/
    case MINI_STCP_STATE_STOPPING_STREAMHDR:
        switch (src)
        {

        case MINI_STCP_SRC_STREAMHDR:
            switch (type)
            {
            case MINI_STREAMHDR_STOPPED:

                /*  Start the pipe. */
                rc = mini_pipebase_start(&stcp->pipebase);
                if (mini_slow(rc < 0))
                {
                    stcp->state = MINI_STCP_STATE_DONE;
                    mini_fsm_raise(&stcp->fsm, &stcp->done, MINI_STCP_ERROR);
                    return;
                }

                /*  Start receiving a message in asynchronous manner. */
                stcp->instate = MINI_STCP_INSTATE_HDR;
                mini_usock_recv(stcp->usock, &stcp->inhdr,
                                sizeof(stcp->inhdr), NULL);

                /*  Mark the pipe as available for sending. */
                stcp->outstate = MINI_STCP_OUTSTATE_IDLE;

                stcp->state = MINI_STCP_STATE_ACTIVE;
                return;

            default:
                mini_fsm_bad_action(stcp->state, src, type);
            }

        default:
            mini_fsm_bad_source(stcp->state, src, type);
        }

        /******************************************************************************/
        /*  ACTIVE state.                                                             */
        /******************************************************************************/
    case MINI_STCP_STATE_ACTIVE:
        switch (src)
        {

        case MINI_STCP_SRC_USOCK:
            switch (type)
            {
            case MINI_USOCK_SENT:

                /*  The message is now fully sent. */
                mini_assert(stcp->outstate == MINI_STCP_OUTSTATE_SENDING);
                stcp->outstate = MINI_STCP_OUTSTATE_IDLE;
                mini_msg_term(&stcp->outmsg);
                mini_msg_init(&stcp->outmsg, 0);
                mini_pipebase_sent(&stcp->pipebase);
                return;

            case MINI_USOCK_RECEIVED:

                switch (stcp->instate)
                {
                case MINI_STCP_INSTATE_HDR:

                    /*  Message header was received. Check that message size
                        is acceptable by comparing with MINI_RCVMAXSIZE;
                        if it's too large, drop the connection. */
                    size = mini_getll(stcp->inhdr);

                    mini_pipebase_getopt(&stcp->pipebase, MINI_SOL_SOCKET,
                                         MINI_RCVMAXSIZE, &opt, &opt_sz);

                    if (opt >= 0 && size > (unsigned)opt)
                    {
                        stcp->state = MINI_STCP_STATE_DONE;
                        mini_fsm_raise(&stcp->fsm, &stcp->done, MINI_STCP_ERROR);
                        return;
                    }

                    /*  Allocate memory for the message. */
                    mini_msg_term(&stcp->inmsg);
                    mini_msg_init(&stcp->inmsg, (size_t)size);

                    /*  Special case when size of the message body is 0. */
                    if (!size)
                    {
                        stcp->instate = MINI_STCP_INSTATE_HASMSG;
                        mini_pipebase_received(&stcp->pipebase);
                        return;
                    }

                    /*  Start receiving the message body. */
                    stcp->instate = MINI_STCP_INSTATE_BODY;
                    mini_usock_recv(stcp->usock,
                                    mini_chunkref_data(&stcp->inmsg.body),
                                    (size_t)size, NULL);

                    return;

                case MINI_STCP_INSTATE_BODY:

                    /*  Message body was received. Notify the owner that it
                        can receive it. */
                    stcp->instate = MINI_STCP_INSTATE_HASMSG;
                    mini_pipebase_received(&stcp->pipebase);

                    return;

                default:
                    mini_fsm_error("Unexpected socket instate",
                                   stcp->state, src, type);
                }

            case MINI_USOCK_SHUTDOWN:
                mini_pipebase_stop(&stcp->pipebase);
                stcp->state = MINI_STCP_STATE_SHUTTING_DOWN;
                return;

            case MINI_USOCK_ERROR:
                mini_pipebase_stop(&stcp->pipebase);
                stcp->state = MINI_STCP_STATE_DONE;
                mini_fsm_raise(&stcp->fsm, &stcp->done, MINI_STCP_ERROR);
                return;

            default:
                mini_fsm_bad_action(stcp->state, src, type);
            }

        default:
            mini_fsm_bad_source(stcp->state, src, type);
        }

        /******************************************************************************/
        /*  SHUTTING_DOWN state.                                                      */
        /*  The underlying connection is closed. We are just waiting that underlying  */
        /*  usock being closed                                                        */
        /******************************************************************************/
    case MINI_STCP_STATE_SHUTTING_DOWN:
        switch (src)
        {

        case MINI_STCP_SRC_USOCK:
            switch (type)
            {
            case MINI_USOCK_ERROR:
                stcp->state = MINI_STCP_STATE_DONE;
                mini_fsm_raise(&stcp->fsm, &stcp->done, MINI_STCP_ERROR);
                return;
            default:
                mini_fsm_bad_action(stcp->state, src, type);
            }

        default:
            mini_fsm_bad_source(stcp->state, src, type);
        }

        /******************************************************************************/
        /*  DONE state.                                                               */
        /*  The underlying connection is closed. There's nothing that can be done in  */
        /*  this state except stopping the object.                                    */
        /******************************************************************************/
    case MINI_STCP_STATE_DONE:
        mini_fsm_bad_source(stcp->state, src, type);

        /******************************************************************************/
        /*  Invalid state.                                                            */
        /******************************************************************************/
    default:
        mini_fsm_bad_state(stcp->state, src, type);
    }
}
