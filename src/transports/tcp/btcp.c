/*
    Copyright (c) 2012-2013 Martin Sustrik  All rights reserved.
    Copyright 2016 Garrett D'Amore <garrett@damore.org>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "btcp.h"
#include "atcp.h"

#include "../utils/port.h"
#include "../utils/iface.h"

#include "../../io/fsm.h"
#include "../../io/usock.h"

#include "../utils/backoff.h"

#include "../../utils/err.h"
#include "../../utils/cont.h"
#include "../../utils/alloc.h"
#include "../../utils/list.h"
#include "../../utils/fast.h"

#include <string.h>

#if defined MINI_HAVE_WINDOWS
#include "../../utils/win.h"
#else
#include <unistd.h>
#include <netinet/in.h>
#endif

/*  The backlog is set relatively high so that there are not too many failed
    connection attempts during re-connection storms. */
#define MINI_BTCP_BACKLOG 100

#define MINI_BTCP_STATE_IDLE 1
#define MINI_BTCP_STATE_ACTIVE 2
#define MINI_BTCP_STATE_STOPPING_ATCP 3
#define MINI_BTCP_STATE_STOPPING_USOCK 4
#define MINI_BTCP_STATE_STOPPING_ATCPS 5

#define MINI_BTCP_SRC_USOCK 1
#define MINI_BTCP_SRC_ATCP 2
#define MINI_BTCP_SRC_BTCP 3

#define MINI_BTCP_TYPE_LISTEN_ERR 1

struct mini_btcp
{

    /*  The state machine. */
    struct mini_fsm fsm;
    struct mini_fsm_event listen_error;
    int state;

    struct mini_ep *ep;

    /*  The underlying listening TCP socket. */
    struct mini_usock usock;

    /*  The connection being accepted at the moment. */
    struct mini_atcp *atcp;

    /*  List of accepted connections. */
    struct mini_list atcps;
};

/*  mini_ep virtual interface implementation. */
static void mini_btcp_stop(void *);
static void mini_btcp_destroy(void *);
const struct mini_ep_ops mini_btcp_ep_ops = {
    mini_btcp_stop,
    mini_btcp_destroy};

/*  Private functions. */
static void mini_btcp_handler(struct mini_fsm *self, int src, int type,
                              void *srcptr);
static void mini_btcp_shutdown(struct mini_fsm *self, int src, int type,
                               void *srcptr);
static int mini_btcp_listen(struct mini_btcp *self);
static void mini_btcp_start_accepting(struct mini_btcp *self);

int mini_btcp_create(struct mini_ep *ep)
{
    int rc;
    struct mini_btcp *self;
    const char *addr;
    const char *end;
    const char *pos;
    struct sockaddr_storage ss;
    size_t sslen;
    int ipv4only;
    size_t ipv4onlylen;

    /*  Allocate the new endpoint object. */
    self = mini_alloc(sizeof(struct mini_btcp), "btcp");
    alloc_assert(self);
    self->ep = ep;

    mini_ep_tran_setup(ep, &mini_btcp_ep_ops, self);
    addr = mini_ep_getaddr(ep);

    /*  Parse the port. */
    end = addr + strlen(addr);
    pos = strrchr(addr, ':');
    if (pos == NULL)
    {
        mini_free(self);
        return -EINVAL;
    }
    ++pos;
    rc = mini_port_resolve(pos, end - pos);
    if (rc < 0)
    {
        mini_free(self);
        return -EINVAL;
    }

    /*  Check whether IPv6 is to be used. */
    ipv4onlylen = sizeof(ipv4only);
    mini_ep_getopt(ep, MINI_SOL_SOCKET, MINI_IPV4ONLY, &ipv4only, &ipv4onlylen);
    mini_assert(ipv4onlylen == sizeof(ipv4only));

    /*  Parse the address. */
    rc = mini_iface_resolve(addr, pos - addr - 1, ipv4only, &ss, &sslen);
    if (mini_slow(rc < 0))
    {
        mini_free(self);
        return -ENODEV;
    }

    /*  Initialise the structure. */
    mini_fsm_init_root(&self->fsm, mini_btcp_handler, mini_btcp_shutdown,
                       mini_ep_getctx(ep));
    mini_fsm_event_init(&self->listen_error);
    self->state = MINI_BTCP_STATE_IDLE;
    self->atcp = NULL;
    mini_list_init(&self->atcps);

    /*  Start the state machine. */
    mini_fsm_start(&self->fsm);

    mini_usock_init(&self->usock, MINI_BTCP_SRC_USOCK, &self->fsm);

    rc = mini_btcp_listen(self);
    if (rc != 0)
    {
        mini_fsm_raise_from_src(&self->fsm, &self->listen_error,
                                MINI_BTCP_SRC_BTCP, MINI_BTCP_TYPE_LISTEN_ERR);
        return rc;
    }

    return 0;
}

static void mini_btcp_stop(void *self)
{
    struct mini_btcp *btcp = self;

    mini_fsm_stop(&btcp->fsm);
}

static void mini_btcp_destroy(void *self)
{
    struct mini_btcp *btcp = self;

    mini_assert_state(btcp, MINI_BTCP_STATE_IDLE);
    mini_list_term(&btcp->atcps);
    mini_assert(btcp->atcp == NULL);
    mini_usock_term(&btcp->usock);
    mini_fsm_term(&btcp->fsm);

    mini_free(btcp);
}

static void mini_btcp_shutdown(struct mini_fsm *self, int src, int type,
                               void *srcptr)
{
    struct mini_btcp *btcp;
    struct mini_list_item *it;
    struct mini_atcp *atcp;

    btcp = mini_cont(self, struct mini_btcp, fsm);

    if (mini_slow(src == MINI_FSM_ACTION && type == MINI_FSM_STOP))
    {
        if (btcp->atcp)
        {
            mini_atcp_stop(btcp->atcp);
            btcp->state = MINI_BTCP_STATE_STOPPING_ATCP;
        }
        else
        {
            btcp->state = MINI_BTCP_STATE_STOPPING_USOCK;
        }
    }
    if (mini_slow(btcp->state == MINI_BTCP_STATE_STOPPING_ATCP))
    {
        if (!mini_atcp_isidle(btcp->atcp))
            return;
        mini_atcp_term(btcp->atcp);
        mini_free(btcp->atcp);
        btcp->atcp = NULL;
        mini_usock_stop(&btcp->usock);
        btcp->state = MINI_BTCP_STATE_STOPPING_USOCK;
    }
    if (mini_slow(btcp->state == MINI_BTCP_STATE_STOPPING_USOCK))
    {
        if (!mini_usock_isidle(&btcp->usock))
            return;
        for (it = mini_list_begin(&btcp->atcps);
             it != mini_list_end(&btcp->atcps);
             it = mini_list_next(&btcp->atcps, it))
        {
            atcp = mini_cont(it, struct mini_atcp, item);
            mini_atcp_stop(atcp);
        }
        btcp->state = MINI_BTCP_STATE_STOPPING_ATCPS;
        goto atcps_stopping;
    }
    if (mini_slow(btcp->state == MINI_BTCP_STATE_STOPPING_ATCPS))
    {
        mini_assert(src == MINI_BTCP_SRC_ATCP && type == MINI_ATCP_STOPPED);
        atcp = (struct mini_atcp *)srcptr;
        mini_list_erase(&btcp->atcps, &atcp->item);
        mini_atcp_term(atcp);
        mini_free(atcp);

        /*  If there are no more atcp state machines, we can stop the whole
            btcp object. */
    atcps_stopping:
        if (mini_list_empty(&btcp->atcps))
        {
            btcp->state = MINI_BTCP_STATE_IDLE;
            mini_fsm_stopped_noevent(&btcp->fsm);
            mini_ep_stopped(btcp->ep);
            return;
        }

        return;
    }

    mini_fsm_bad_action(btcp->state, src, type);
}

static void mini_btcp_handler(struct mini_fsm *self, int src, int type,
                              void *srcptr)
{
    struct mini_btcp *btcp;
    struct mini_atcp *atcp;

    btcp = mini_cont(self, struct mini_btcp, fsm);

    switch (btcp->state)
    {

        /******************************************************************************/
        /*  IDLE state.                                                               */
        /******************************************************************************/
    case MINI_BTCP_STATE_IDLE:
        mini_assert(src == MINI_FSM_ACTION);
        mini_assert(type == MINI_FSM_START);
        btcp->state = MINI_BTCP_STATE_ACTIVE;
        return;

        /******************************************************************************/
        /*  ACTIVE state.                                                             */
        /*  The execution is yielded to the atcp state machine in this state.         */
        /******************************************************************************/
    case MINI_BTCP_STATE_ACTIVE:
        if (src == MINI_BTCP_SRC_BTCP)
        {
            mini_assert(type == MINI_BTCP_TYPE_LISTEN_ERR);
            mini_free(btcp);
            return;
        }

        if (src == MINI_BTCP_SRC_USOCK)
        {
            /*  usock object cleaning up */
            mini_assert(type == MINI_USOCK_SHUTDOWN || type == MINI_USOCK_STOPPED);
            return;
        }

        /*  All other events come from child atcp objects. */
        mini_assert(src == MINI_BTCP_SRC_ATCP);
        atcp = (struct mini_atcp *)srcptr;
        switch (type)
        {
        case MINI_ATCP_ACCEPTED:
            mini_assert(btcp->atcp == atcp);
            mini_list_insert(&btcp->atcps, &atcp->item,
                             mini_list_end(&btcp->atcps));
            btcp->atcp = NULL;
            mini_btcp_start_accepting(btcp);
            return;
        case MINI_ATCP_ERROR:
            mini_atcp_stop(atcp);
            return;
        case MINI_ATCP_STOPPED:
            mini_list_erase(&btcp->atcps, &atcp->item);
            mini_atcp_term(atcp);
            mini_free(atcp);
            return;
        default:
            mini_fsm_bad_action(btcp->state, src, type);
        }

        /******************************************************************************/
        /*  Invalid state.                                                            */
        /******************************************************************************/
    default:
        mini_fsm_bad_state(btcp->state, src, type);
    }
}

static int mini_btcp_listen(struct mini_btcp *self)
{
    int rc;
    struct sockaddr_storage ss;
    size_t sslen;
    int ipv4only;
    size_t ipv4onlylen;
    const char *addr;
    const char *end;
    const char *pos;
    uint16_t port;

    /*  First, resolve the IP address. */
    addr = mini_ep_getaddr(self->ep);
    memset(&ss, 0, sizeof(ss));

    /*  Parse the port. */
    end = addr + strlen(addr);
    pos = strrchr(addr, ':');
    if (pos == NULL)
    {
        return -EINVAL;
    }
    ++pos;
    rc = mini_port_resolve(pos, end - pos);
    if (rc <= 0)
        return rc;
    port = (uint16_t)rc;

    /*  Parse the address. */
    ipv4onlylen = sizeof(ipv4only);
    mini_ep_getopt(self->ep, MINI_SOL_SOCKET, MINI_IPV4ONLY,
                   &ipv4only, &ipv4onlylen);
    mini_assert(ipv4onlylen == sizeof(ipv4only));
    rc = mini_iface_resolve(addr, pos - addr - 1, ipv4only, &ss, &sslen);
    if (rc < 0)
    {
        return rc;
    }

    /*  Combine the port and the address. */
    switch (ss.ss_family)
    {
    case AF_INET:
        ((struct sockaddr_in *)&ss)->sin_port = htons(port);
        sslen = sizeof(struct sockaddr_in);
        break;
    case AF_INET6:
        ((struct sockaddr_in6 *)&ss)->sin6_port = htons(port);
        sslen = sizeof(struct sockaddr_in6);
        break;
    default:
        mini_assert(0);
    }

    /*  Start listening for incoming connections. */
    rc = mini_usock_start(&self->usock, ss.ss_family, SOCK_STREAM, 0);
    if (rc < 0)
    {
        return rc;
    }

    rc = mini_usock_bind(&self->usock, (struct sockaddr *)&ss, (size_t)sslen);
    if (rc < 0)
    {
        mini_usock_stop(&self->usock);
        return rc;
    }

    rc = mini_usock_listen(&self->usock, MINI_BTCP_BACKLOG);
    if (rc < 0)
    {
        mini_usock_stop(&self->usock);
        return rc;
    }
    mini_btcp_start_accepting(self);

    return 0;
}

/******************************************************************************/
/*  State machine actions.                                                    */
/******************************************************************************/

static void mini_btcp_start_accepting(struct mini_btcp *self)
{
    mini_assert(self->atcp == NULL);

    /*  Allocate new atcp state machine. */
    self->atcp = mini_alloc(sizeof(struct mini_atcp), "atcp");
    alloc_assert(self->atcp);
    mini_atcp_init(self->atcp, MINI_BTCP_SRC_ATCP, self->ep, &self->fsm);

    /*  Start waiting for a new incoming connection. */
    mini_atcp_start(self->atcp, &self->usock);
}
