/*
    Copyright (c) 2012-2013 Martin Sustrik  All rights reserved.
    Copyright 2016 Garrett D'Amore <garrett@damore.org>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "atcp.h"

#include "../../tcp.h"

#include "../../utils/err.h"
#include "../../utils/cont.h"
#include "../../utils/attr.h"

#if defined MINI_HAVE_WINDOWS
#include "../../utils/win.h"
#else
#include <netinet/in.h>
#include <netinet/tcp.h>
#endif

#define MINI_ATCP_STATE_IDLE 1
#define MINI_ATCP_STATE_ACCEPTING 2
#define MINI_ATCP_STATE_ACTIVE 3
#define MINI_ATCP_STATE_STOPPING_STCP 4
#define MINI_ATCP_STATE_STOPPING_USOCK 5
#define MINI_ATCP_STATE_DONE 6
#define MINI_ATCP_STATE_STOPPING_STCP_FINAL 7
#define MINI_ATCP_STATE_STOPPING 8

#define MINI_ATCP_SRC_USOCK 1
#define MINI_ATCP_SRC_STCP 2
#define MINI_ATCP_SRC_LISTENER 3

/*  Private functions. */
static void mini_atcp_handler(struct mini_fsm *self, int src, int type,
                              void *srcptr);
static void mini_atcp_shutdown(struct mini_fsm *self, int src, int type,
                               void *srcptr);

void mini_atcp_init(struct mini_atcp *self, int src,
                    struct mini_ep *ep, struct mini_fsm *owner)
{
    mini_fsm_init(&self->fsm, mini_atcp_handler, mini_atcp_shutdown,
                  src, self, owner);
    self->state = MINI_ATCP_STATE_IDLE;
    self->ep = ep;
    mini_usock_init(&self->usock, MINI_ATCP_SRC_USOCK, &self->fsm);
    self->listener = NULL;
    self->listener_owner.src = -1;
    self->listener_owner.fsm = NULL;
    mini_stcp_init(&self->stcp, MINI_ATCP_SRC_STCP, ep, &self->fsm);
    mini_fsm_event_init(&self->accepted);
    mini_fsm_event_init(&self->done);
    mini_list_item_init(&self->item);
}

void mini_atcp_term(struct mini_atcp *self)
{
    mini_assert_state(self, MINI_ATCP_STATE_IDLE);

    mini_list_item_term(&self->item);
    mini_fsm_event_term(&self->done);
    mini_fsm_event_term(&self->accepted);
    mini_stcp_term(&self->stcp);
    mini_usock_term(&self->usock);
    mini_fsm_term(&self->fsm);
}

int mini_atcp_isidle(struct mini_atcp *self)
{
    return mini_fsm_isidle(&self->fsm);
}

void mini_atcp_start(struct mini_atcp *self, struct mini_usock *listener)
{
    mini_assert_state(self, MINI_ATCP_STATE_IDLE);

    /*  Take ownership of the listener socket. */
    self->listener = listener;
    self->listener_owner.src = MINI_ATCP_SRC_LISTENER;
    self->listener_owner.fsm = &self->fsm;
    mini_usock_swap_owner(listener, &self->listener_owner);

    /*  Start the state machine. */
    mini_fsm_start(&self->fsm);
}

void mini_atcp_stop(struct mini_atcp *self)
{
    mini_fsm_stop(&self->fsm);
}

static void mini_atcp_shutdown(struct mini_fsm *self, int src, int type,
                               MINI_UNUSED void *srcptr)
{
    struct mini_atcp *atcp;

    atcp = mini_cont(self, struct mini_atcp, fsm);

    if (mini_slow(src == MINI_FSM_ACTION && type == MINI_FSM_STOP))
    {
        if (!mini_stcp_isidle(&atcp->stcp))
        {
            mini_ep_stat_increment(atcp->ep, MINI_STAT_DROPPED_CONNECTIONS, 1);
            mini_stcp_stop(&atcp->stcp);
        }
        atcp->state = MINI_ATCP_STATE_STOPPING_STCP_FINAL;
    }
    if (mini_slow(atcp->state == MINI_ATCP_STATE_STOPPING_STCP_FINAL))
    {
        if (!mini_stcp_isidle(&atcp->stcp))
            return;
        mini_usock_stop(&atcp->usock);
        atcp->state = MINI_ATCP_STATE_STOPPING;
    }
    if (mini_slow(atcp->state == MINI_ATCP_STATE_STOPPING))
    {
        if (!mini_usock_isidle(&atcp->usock))
            return;
        if (atcp->listener)
        {
            mini_assert(atcp->listener_owner.fsm);
            mini_usock_swap_owner(atcp->listener, &atcp->listener_owner);
            atcp->listener = NULL;
            atcp->listener_owner.src = -1;
            atcp->listener_owner.fsm = NULL;
        }
        atcp->state = MINI_ATCP_STATE_IDLE;
        mini_fsm_stopped(&atcp->fsm, MINI_ATCP_STOPPED);
        return;
    }

    mini_fsm_bad_action(atcp->state, src, type);
}

static void mini_atcp_handler(struct mini_fsm *self, int src, int type,
                              MINI_UNUSED void *srcptr)
{
    struct mini_atcp *atcp;
    int val;
    size_t sz;

    atcp = mini_cont(self, struct mini_atcp, fsm);

    switch (atcp->state)
    {

        /******************************************************************************/
        /*  IDLE state.                                                               */
        /*  The state machine wasn't yet started.                                     */
        /******************************************************************************/
    case MINI_ATCP_STATE_IDLE:
        switch (src)
        {

        case MINI_FSM_ACTION:
            switch (type)
            {
            case MINI_FSM_START:
                mini_usock_accept(&atcp->usock, atcp->listener);
                atcp->state = MINI_ATCP_STATE_ACCEPTING;
                return;
            default:
                mini_fsm_bad_action(atcp->state, src, type);
            }

        default:
            mini_fsm_bad_source(atcp->state, src, type);
        }

        /******************************************************************************/
        /*  ACCEPTING state.                                                          */
        /*  Waiting for incoming connection.                                          */
        /******************************************************************************/
    case MINI_ATCP_STATE_ACCEPTING:
        switch (src)
        {

        case MINI_ATCP_SRC_USOCK:
            switch (type)
            {
            case MINI_USOCK_ACCEPTED:
                mini_ep_clear_error(atcp->ep);

                /*  Set the relevant socket options. */
                sz = sizeof(val);
                mini_ep_getopt(atcp->ep, MINI_SOL_SOCKET, MINI_SNDBUF, &val, &sz);
                mini_assert(sz == sizeof(val));
                mini_usock_setsockopt(&atcp->usock, SOL_SOCKET, SO_SNDBUF,
                                      &val, sizeof(val));
                sz = sizeof(val);
                mini_ep_getopt(atcp->ep, MINI_SOL_SOCKET, MINI_RCVBUF, &val, &sz);
                mini_assert(sz == sizeof(val));
                mini_usock_setsockopt(&atcp->usock, SOL_SOCKET, SO_RCVBUF,
                                      &val, sizeof(val));
                sz = sizeof(val);
                mini_ep_getopt(atcp->ep, MINI_TCP, MINI_TCP_NODELAY, &val, &sz);
                mini_assert(sz == sizeof(val));
                mini_usock_setsockopt(&atcp->usock, IPPROTO_TCP, TCP_NODELAY,
                                      &val, sizeof(val));

                /*  Return ownership of the listening socket to the parent. */
                mini_usock_swap_owner(atcp->listener, &atcp->listener_owner);
                atcp->listener = NULL;
                atcp->listener_owner.src = -1;
                atcp->listener_owner.fsm = NULL;
                mini_fsm_raise(&atcp->fsm, &atcp->accepted, MINI_ATCP_ACCEPTED);

                /*  Start the stcp state machine. */
                mini_usock_activate(&atcp->usock);
                mini_stcp_start(&atcp->stcp, &atcp->usock);
                atcp->state = MINI_ATCP_STATE_ACTIVE;

                mini_ep_stat_increment(atcp->ep,
                                       MINI_STAT_ACCEPTED_CONNECTIONS, 1);

                return;

            default:
                mini_fsm_bad_action(atcp->state, src, type);
            }

        case MINI_ATCP_SRC_LISTENER:
            switch (type)
            {

            case MINI_USOCK_ACCEPT_ERROR:
                mini_ep_set_error(atcp->ep, mini_usock_geterrno(atcp->listener));
                mini_ep_stat_increment(atcp->ep, MINI_STAT_ACCEPT_ERRORS, 1);
                mini_usock_accept(&atcp->usock, atcp->listener);
                return;

            default:
                mini_fsm_bad_action(atcp->state, src, type);
            }

        default:
            mini_fsm_bad_source(atcp->state, src, type);
        }

        /******************************************************************************/
        /*  ACTIVE state.                                                             */
        /******************************************************************************/
    case MINI_ATCP_STATE_ACTIVE:
        switch (src)
        {

        case MINI_ATCP_SRC_STCP:
            switch (type)
            {
            case MINI_STCP_ERROR:
                mini_stcp_stop(&atcp->stcp);
                atcp->state = MINI_ATCP_STATE_STOPPING_STCP;
                mini_ep_stat_increment(atcp->ep, MINI_STAT_BROKEN_CONNECTIONS, 1);
                return;
            default:
                mini_fsm_bad_action(atcp->state, src, type);
            }

        default:
            mini_fsm_bad_source(atcp->state, src, type);
        }

        /******************************************************************************/
        /*  STOPPING_STCP state.                                                      */
        /******************************************************************************/
    case MINI_ATCP_STATE_STOPPING_STCP:
        switch (src)
        {

        case MINI_ATCP_SRC_STCP:
            switch (type)
            {
            case MINI_USOCK_SHUTDOWN:
                return;
            case MINI_STCP_STOPPED:
                mini_usock_stop(&atcp->usock);
                atcp->state = MINI_ATCP_STATE_STOPPING_USOCK;
                return;
            default:
                mini_fsm_bad_action(atcp->state, src, type);
            }

        default:
            mini_fsm_bad_source(atcp->state, src, type);
        }

        /******************************************************************************/
        /*  STOPPING_USOCK state.                                                      */
        /******************************************************************************/
    case MINI_ATCP_STATE_STOPPING_USOCK:
        switch (src)
        {

        case MINI_ATCP_SRC_USOCK:
            switch (type)
            {
            case MINI_USOCK_SHUTDOWN:
                return;
            case MINI_USOCK_STOPPED:
                mini_fsm_raise(&atcp->fsm, &atcp->done, MINI_ATCP_ERROR);
                atcp->state = MINI_ATCP_STATE_DONE;
                return;
            default:
                mini_fsm_bad_action(atcp->state, src, type);
            }

        default:
            mini_fsm_bad_source(atcp->state, src, type);
        }

        /******************************************************************************/
        /*  Invalid state.                                                            */
        /******************************************************************************/
    default:
        mini_fsm_bad_state(atcp->state, src, type);
    }
}
