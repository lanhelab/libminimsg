/*
    Copyright (c) 2013 Martin Sustrik  All rights reserved.
    Copyright 2016 Garrett D'Amore <garrett@damore.org>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#ifndef MINI_STCP_INCLUDED
#define MINI_STCP_INCLUDED

#include "../../transport.h"

#include "../../io/fsm.h"
#include "../../io/usock.h"

#include "../utils/streamhdr.h"

#include "../../utils/msg.h"

/*  This state machine handles TCP connection from the point where it is
    established to the point when it is broken. */

#define MINI_STCP_ERROR 1
#define MINI_STCP_STOPPED 2

struct mini_stcp
{

    /*  The state machine. */
    struct mini_fsm fsm;
    int state;

    /*  The underlying socket. */
    struct mini_usock *usock;

    /*  Child state machine to do protocol header exchange. */
    struct mini_streamhdr streamhdr;

    /*  The original owner of the underlying socket. */
    struct mini_fsm_owner usock_owner;

    /*  Pipe connecting this TCP connection to the minimsg core. */
    struct mini_pipebase pipebase;

    /*  State of inbound state machine. */
    int instate;

    /*  Buffer used to store the header of incoming message. */
    uint8_t inhdr[8];

    /*  Message being received at the moment. */
    struct mini_msg inmsg;

    /*  State of the outbound state machine. */
    int outstate;

    /*  Buffer used to store the header of outgoing message. */
    uint8_t outhdr[8];

    /*  Message being sent at the moment. */
    struct mini_msg outmsg;

    /*  Event raised when the state machine ends. */
    struct mini_fsm_event done;
};

void mini_stcp_init(struct mini_stcp *self, int src,
                    struct mini_ep *ep, struct mini_fsm *owner);
void mini_stcp_term(struct mini_stcp *self);

int mini_stcp_isidle(struct mini_stcp *self);
void mini_stcp_start(struct mini_stcp *self, struct mini_usock *usock);
void mini_stcp_stop(struct mini_stcp *self);

#endif
