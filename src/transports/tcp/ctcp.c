/*
    Copyright (c) 2012-2013 Martin Sustrik  All rights reserved.
    Copyright 2016 Garrett D'Amore <garrett@damore.org>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "ctcp.h"
#include "stcp.h"

#include "../../tcp.h"

#include "../utils/dns.h"
#include "../utils/port.h"
#include "../utils/iface.h"
#include "../utils/backoff.h"
#include "../utils/literal.h"

#include "../../io/fsm.h"
#include "../../io/usock.h"

#include "../../utils/err.h"
#include "../../utils/cont.h"
#include "../../utils/alloc.h"
#include "../../utils/fast.h"
#include "../../utils/attr.h"

#include <string.h>

#if defined MINI_HAVE_WINDOWS
#include "../../utils/win.h"
#else
#include <unistd.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#endif

#define MINI_CTCP_STATE_IDLE 1
#define MINI_CTCP_STATE_RESOLVING 2
#define MINI_CTCP_STATE_STOPPING_DNS 3
#define MINI_CTCP_STATE_CONNECTING 4
#define MINI_CTCP_STATE_ACTIVE 5
#define MINI_CTCP_STATE_STOPPING_STCP 6
#define MINI_CTCP_STATE_STOPPING_USOCK 7
#define MINI_CTCP_STATE_WAITING 8
#define MINI_CTCP_STATE_STOPPING_BACKOFF 9
#define MINI_CTCP_STATE_STOPPING_STCP_FINAL 10
#define MINI_CTCP_STATE_STOPPING 11

#define MINI_CTCP_SRC_USOCK 1
#define MINI_CTCP_SRC_RECONNECT_TIMER 2
#define MINI_CTCP_SRC_DNS 3
#define MINI_CTCP_SRC_STCP 4

struct mini_ctcp
{

    /*  The state machine. */
    struct mini_fsm fsm;
    int state;

    struct mini_ep *ep;

    /*  The underlying TCP socket. */
    struct mini_usock usock;

    /*  Used to wait before retrying to connect. */
    struct mini_backoff retry;

    /*  State machine that handles the active part of the connection
        lifetime. */
    struct mini_stcp stcp;

    /*  DNS resolver used to convert textual address into actual IP address
        along with the variable to hold the result. */
    struct mini_dns dns;
    struct mini_dns_result dns_result;
};

/*  mini_ep virtual interface implementation. */
static void mini_ctcp_stop(void *);
static void mini_ctcp_destroy(void *);
const struct mini_ep_ops mini_ctcp_ep_ops = {
    mini_ctcp_stop,
    mini_ctcp_destroy};

/*  Private functions. */
static void mini_ctcp_handler(struct mini_fsm *self, int src, int type,
                              void *srcptr);
static void mini_ctcp_shutdown(struct mini_fsm *self, int src, int type,
                               void *srcptr);
static void mini_ctcp_start_resolving(struct mini_ctcp *self);
static void mini_ctcp_start_connecting(struct mini_ctcp *self,
                                       struct sockaddr_storage *ss, size_t sslen);

int mini_ctcp_create(struct mini_ep *ep)
{
    int rc;
    const char *addr;
    size_t addrlen;
    const char *semicolon;
    const char *hostname;
    const char *colon;
    const char *end;
    struct sockaddr_storage ss;
    size_t sslen;
    int ipv4only;
    size_t ipv4onlylen;
    struct mini_ctcp *self;
    int reconnect_ivl;
    int reconnect_ivl_max;
    size_t sz;

    /*  Allocate the new endpoint object. */
    self = mini_alloc(sizeof(struct mini_ctcp), "ctcp");
    alloc_assert(self);

    /*  Initalise the endpoint. */
    self->ep = ep;
    mini_ep_tran_setup(ep, &mini_ctcp_ep_ops, self);

    /*  Check whether IPv6 is to be used. */
    ipv4onlylen = sizeof(ipv4only);
    mini_ep_getopt(ep, MINI_SOL_SOCKET, MINI_IPV4ONLY, &ipv4only, &ipv4onlylen);
    mini_assert(ipv4onlylen == sizeof(ipv4only));

    /*  Start parsing the address. */
    addr = mini_ep_getaddr(ep);
    addrlen = strlen(addr);
    semicolon = strchr(addr, ';');
    hostname = semicolon ? semicolon + 1 : addr;
    colon = strrchr(addr, ':');
    end = addr + addrlen;

    /*  Parse the port. */
    if (!colon)
    {
        mini_free(self);
        return -EINVAL;
    }
    rc = mini_port_resolve(colon + 1, end - colon - 1);
    if (rc < 0)
    {
        mini_free(self);
        return -EINVAL;
    }

    /*  Check whether the host portion of the address is either a literal
        or a valid hostname. */
    if (mini_dns_check_hostname(hostname, colon - hostname) < 0 &&
        mini_literal_resolve(hostname, colon - hostname, ipv4only,
                             &ss, &sslen) < 0)
    {
        mini_free(self);
        return -EINVAL;
    }

    /*  If local address is specified, check whether it is valid. */
    if (semicolon)
    {
        rc = mini_iface_resolve(addr, semicolon - addr, ipv4only, &ss, &sslen);
        if (rc < 0)
        {
            mini_free(self);
            return -ENODEV;
        }
    }

    /*  Initialise the structure. */
    mini_fsm_init_root(&self->fsm, mini_ctcp_handler, mini_ctcp_shutdown,
                       mini_ep_getctx(ep));
    self->state = MINI_CTCP_STATE_IDLE;
    mini_usock_init(&self->usock, MINI_CTCP_SRC_USOCK, &self->fsm);
    sz = sizeof(reconnect_ivl);
    mini_ep_getopt(ep, MINI_SOL_SOCKET, MINI_RECONNECT_IVL, &reconnect_ivl, &sz);
    mini_assert(sz == sizeof(reconnect_ivl));
    sz = sizeof(reconnect_ivl_max);
    mini_ep_getopt(ep, MINI_SOL_SOCKET, MINI_RECONNECT_IVL_MAX,
                   &reconnect_ivl_max, &sz);
    mini_assert(sz == sizeof(reconnect_ivl_max));
    if (reconnect_ivl_max == 0)
        reconnect_ivl_max = reconnect_ivl;
    mini_backoff_init(&self->retry, MINI_CTCP_SRC_RECONNECT_TIMER,
                      reconnect_ivl, reconnect_ivl_max, &self->fsm);
    mini_stcp_init(&self->stcp, MINI_CTCP_SRC_STCP, ep, &self->fsm);
    mini_dns_init(&self->dns, MINI_CTCP_SRC_DNS, &self->fsm);

    /*  Start the state machine. */
    mini_fsm_start(&self->fsm);

    return 0;
}

static void mini_ctcp_stop(void *self)
{
    struct mini_ctcp *ctcp = self;

    mini_fsm_stop(&ctcp->fsm);
}

static void mini_ctcp_destroy(void *self)
{
    struct mini_ctcp *ctcp = self;

    mini_dns_term(&ctcp->dns);
    mini_stcp_term(&ctcp->stcp);
    mini_backoff_term(&ctcp->retry);
    mini_usock_term(&ctcp->usock);
    mini_fsm_term(&ctcp->fsm);

    mini_free(ctcp);
}

static void mini_ctcp_shutdown(struct mini_fsm *self, int src, int type,
                               MINI_UNUSED void *srcptr)
{
    struct mini_ctcp *ctcp;

    ctcp = mini_cont(self, struct mini_ctcp, fsm);

    if (src == MINI_FSM_ACTION && type == MINI_FSM_STOP)
    {
        if (!mini_stcp_isidle(&ctcp->stcp))
        {
            mini_ep_stat_increment(ctcp->ep, MINI_STAT_DROPPED_CONNECTIONS, 1);
            mini_stcp_stop(&ctcp->stcp);
        }
        ctcp->state = MINI_CTCP_STATE_STOPPING_STCP_FINAL;
    }
    if (ctcp->state == MINI_CTCP_STATE_STOPPING_STCP_FINAL)
    {
        if (!mini_stcp_isidle(&ctcp->stcp))
            return;
        mini_backoff_stop(&ctcp->retry);
        mini_usock_stop(&ctcp->usock);
        mini_dns_stop(&ctcp->dns);
        ctcp->state = MINI_CTCP_STATE_STOPPING;
    }
    if (mini_slow(ctcp->state == MINI_CTCP_STATE_STOPPING))
    {
        if (!mini_backoff_isidle(&ctcp->retry) ||
            !mini_usock_isidle(&ctcp->usock) ||
            !mini_dns_isidle(&ctcp->dns))
            return;
        ctcp->state = MINI_CTCP_STATE_IDLE;
        mini_fsm_stopped_noevent(&ctcp->fsm);
        mini_ep_stopped(ctcp->ep);
        return;
    }

    mini_fsm_bad_state(ctcp->state, src, type);
}

static void mini_ctcp_handler(struct mini_fsm *self, int src, int type,
                              MINI_UNUSED void *srcptr)
{
    struct mini_ctcp *ctcp;

    ctcp = mini_cont(self, struct mini_ctcp, fsm);

    switch (ctcp->state)
    {

        /******************************************************************************/
        /*  IDLE state.                                                               */
        /*  The state machine wasn't yet started.                                     */
        /******************************************************************************/
    case MINI_CTCP_STATE_IDLE:
        switch (src)
        {

        case MINI_FSM_ACTION:
            switch (type)
            {
            case MINI_FSM_START:
                mini_ctcp_start_resolving(ctcp);
                return;
            default:
                mini_fsm_bad_action(ctcp->state, src, type);
            }

        default:
            mini_fsm_bad_source(ctcp->state, src, type);
        }

        /******************************************************************************/
        /*  RESOLVING state.                                                          */
        /*  Name of the host to connect to is being resolved to get an IP address.    */
        /******************************************************************************/
    case MINI_CTCP_STATE_RESOLVING:
        switch (src)
        {

        case MINI_CTCP_SRC_DNS:
            switch (type)
            {
            case MINI_DNS_DONE:
                mini_dns_stop(&ctcp->dns);
                ctcp->state = MINI_CTCP_STATE_STOPPING_DNS;
                return;
            default:
                mini_fsm_bad_action(ctcp->state, src, type);
            }

        default:
            mini_fsm_bad_source(ctcp->state, src, type);
        }

        /******************************************************************************/
        /*  STOPPING_DNS state.                                                       */
        /*  dns object was asked to stop but it haven't stopped yet.                  */
        /******************************************************************************/
    case MINI_CTCP_STATE_STOPPING_DNS:
        switch (src)
        {

        case MINI_CTCP_SRC_DNS:
            switch (type)
            {
            case MINI_DNS_STOPPED:
                if (ctcp->dns_result.error == 0)
                {
                    mini_ctcp_start_connecting(ctcp, &ctcp->dns_result.addr,
                                               ctcp->dns_result.addrlen);
                    return;
                }
                mini_backoff_start(&ctcp->retry);
                ctcp->state = MINI_CTCP_STATE_WAITING;
                return;
            default:
                mini_fsm_bad_action(ctcp->state, src, type);
            }

        default:
            mini_fsm_bad_source(ctcp->state, src, type);
        }

        /******************************************************************************/
        /*  CONNECTING state.                                                         */
        /*  Non-blocking connect is under way.                                        */
        /******************************************************************************/
    case MINI_CTCP_STATE_CONNECTING:
        switch (src)
        {

        case MINI_CTCP_SRC_USOCK:
            switch (type)
            {
            case MINI_USOCK_CONNECTED:
                mini_stcp_start(&ctcp->stcp, &ctcp->usock);
                ctcp->state = MINI_CTCP_STATE_ACTIVE;
                mini_ep_stat_increment(ctcp->ep,
                                       MINI_STAT_INPROGRESS_CONNECTIONS, -1);
                mini_ep_stat_increment(ctcp->ep,
                                       MINI_STAT_ESTABLISHED_CONNECTIONS, 1);
                mini_ep_clear_error(ctcp->ep);
                return;
            case MINI_USOCK_ERROR:
                mini_ep_set_error(ctcp->ep, mini_usock_geterrno(&ctcp->usock));
                mini_usock_stop(&ctcp->usock);
                ctcp->state = MINI_CTCP_STATE_STOPPING_USOCK;
                mini_ep_stat_increment(ctcp->ep,
                                       MINI_STAT_INPROGRESS_CONNECTIONS, -1);
                mini_ep_stat_increment(ctcp->ep, MINI_STAT_CONNECT_ERRORS, 1);
                return;
            default:
                mini_fsm_bad_action(ctcp->state, src, type);
            }

        default:
            mini_fsm_bad_source(ctcp->state, src, type);
        }

        /******************************************************************************/
        /*  ACTIVE state.                                                             */
        /*  Connection is established and handled by the stcp state machine.          */
        /******************************************************************************/
    case MINI_CTCP_STATE_ACTIVE:
        switch (src)
        {

        case MINI_CTCP_SRC_STCP:
            switch (type)
            {
            case MINI_STCP_ERROR:
                mini_stcp_stop(&ctcp->stcp);
                ctcp->state = MINI_CTCP_STATE_STOPPING_STCP;
                mini_ep_stat_increment(ctcp->ep, MINI_STAT_BROKEN_CONNECTIONS, 1);
                return;
            default:
                mini_fsm_bad_action(ctcp->state, src, type);
            }

        default:
            mini_fsm_bad_source(ctcp->state, src, type);
        }

        /******************************************************************************/
        /*  STOPPING_STCP state.                                                      */
        /*  stcp object was asked to stop but it haven't stopped yet.                 */
        /******************************************************************************/
    case MINI_CTCP_STATE_STOPPING_STCP:
        switch (src)
        {

        case MINI_CTCP_SRC_STCP:
            switch (type)
            {
            case MINI_USOCK_SHUTDOWN:
                return;
            case MINI_STCP_STOPPED:
                mini_usock_stop(&ctcp->usock);
                ctcp->state = MINI_CTCP_STATE_STOPPING_USOCK;
                return;
            default:
                mini_fsm_bad_action(ctcp->state, src, type);
            }

        default:
            mini_fsm_bad_source(ctcp->state, src, type);
        }

        /******************************************************************************/
        /*  STOPPING_USOCK state.                                                     */
        /*  usock object was asked to stop but it haven't stopped yet.                */
        /******************************************************************************/
    case MINI_CTCP_STATE_STOPPING_USOCK:
        switch (src)
        {

        case MINI_CTCP_SRC_USOCK:
            switch (type)
            {
            case MINI_USOCK_SHUTDOWN:
                return;
            case MINI_USOCK_STOPPED:
                mini_backoff_start(&ctcp->retry);
                ctcp->state = MINI_CTCP_STATE_WAITING;
                return;
            default:
                mini_fsm_bad_action(ctcp->state, src, type);
            }

        default:
            mini_fsm_bad_source(ctcp->state, src, type);
        }

        /******************************************************************************/
        /*  WAITING state.                                                            */
        /*  Waiting before re-connection is attempted. This way we won't overload     */
        /*  the system by continuous re-connection attempts.                          */
        /******************************************************************************/
    case MINI_CTCP_STATE_WAITING:
        switch (src)
        {

        case MINI_CTCP_SRC_RECONNECT_TIMER:
            switch (type)
            {
            case MINI_BACKOFF_TIMEOUT:
                mini_backoff_stop(&ctcp->retry);
                ctcp->state = MINI_CTCP_STATE_STOPPING_BACKOFF;
                return;
            default:
                mini_fsm_bad_action(ctcp->state, src, type);
            }

        default:
            mini_fsm_bad_source(ctcp->state, src, type);
        }

        /******************************************************************************/
        /*  STOPPING_BACKOFF state.                                                   */
        /*  backoff object was asked to stop, but it haven't stopped yet.             */
        /******************************************************************************/
    case MINI_CTCP_STATE_STOPPING_BACKOFF:
        switch (src)
        {

        case MINI_CTCP_SRC_RECONNECT_TIMER:
            switch (type)
            {
            case MINI_BACKOFF_STOPPED:
                mini_ctcp_start_resolving(ctcp);
                return;
            default:
                mini_fsm_bad_action(ctcp->state, src, type);
            }

        default:
            mini_fsm_bad_source(ctcp->state, src, type);
        }

        /******************************************************************************/
        /*  Invalid state.                                                            */
        /******************************************************************************/
    default:
        mini_fsm_bad_state(ctcp->state, src, type);
    }
}

/******************************************************************************/
/*  State machine actions.                                                    */
/******************************************************************************/

static void mini_ctcp_start_resolving(struct mini_ctcp *self)
{
    const char *addr;
    const char *begin;
    const char *end;
    int ipv4only;
    size_t ipv4onlylen;

    /*  Extract the hostname part from address string. */
    addr = mini_ep_getaddr(self->ep);
    begin = strchr(addr, ';');
    if (!begin)
        begin = addr;
    else
        ++begin;
    end = strrchr(addr, ':');
    mini_assert(end);

    /*  Check whether IPv6 is to be used. */
    ipv4onlylen = sizeof(ipv4only);
    mini_ep_getopt(self->ep, MINI_SOL_SOCKET, MINI_IPV4ONLY,
                   &ipv4only, &ipv4onlylen);
    mini_assert(ipv4onlylen == sizeof(ipv4only));

    /*  TODO: Get the actual value of IPV4ONLY option. */
    mini_dns_start(&self->dns, begin, end - begin, ipv4only, &self->dns_result);

    self->state = MINI_CTCP_STATE_RESOLVING;
}

static void mini_ctcp_start_connecting(struct mini_ctcp *self,
                                       struct sockaddr_storage *ss, size_t sslen)
{
    int rc;
    struct sockaddr_storage remote;
    size_t remotelen;
    struct sockaddr_storage local;
    size_t locallen;
    const char *addr;
    const char *end;
    const char *colon;
    const char *semicolon;
    uint16_t port;
    int ipv4only;
    size_t ipv4onlylen;
    int val;
    size_t sz;

    /*  Create IP address from the address string. */
    addr = mini_ep_getaddr(self->ep);
    memset(&remote, 0, sizeof(remote));

    /*  Parse the port. */
    end = addr + strlen(addr);
    colon = strrchr(addr, ':');
    rc = mini_port_resolve(colon + 1, end - colon - 1);
    errnum_assert(rc > 0, -rc);
    port = rc;

    /*  Check whether IPv6 is to be used. */
    ipv4onlylen = sizeof(ipv4only);
    mini_ep_getopt(self->ep, MINI_SOL_SOCKET, MINI_IPV4ONLY,
                   &ipv4only, &ipv4onlylen);
    mini_assert(ipv4onlylen == sizeof(ipv4only));

    /*  Parse the local address, if any. */
    semicolon = strchr(addr, ';');
    memset(&local, 0, sizeof(local));
    if (semicolon)
        rc = mini_iface_resolve(addr, semicolon - addr, ipv4only,
                                &local, &locallen);
    else
        rc = mini_iface_resolve("*", 1, ipv4only, &local, &locallen);
    if (mini_slow(rc < 0))
    {
        mini_backoff_start(&self->retry);
        self->state = MINI_CTCP_STATE_WAITING;
        return;
    }

    /*  Combine the remote address and the port. */
    remote = *ss;
    remotelen = sslen;
    if (remote.ss_family == AF_INET)
        ((struct sockaddr_in *)&remote)->sin_port = htons(port);
    else if (remote.ss_family == AF_INET6)
        ((struct sockaddr_in6 *)&remote)->sin6_port = htons(port);
    else
        mini_assert(0);

    /*  Try to start the underlying socket. */
    rc = mini_usock_start(&self->usock, remote.ss_family, SOCK_STREAM, 0);
    if (mini_slow(rc < 0))
    {
        mini_backoff_start(&self->retry);
        self->state = MINI_CTCP_STATE_WAITING;
        return;
    }

    /*  Set the relevant socket options. */
    sz = sizeof(val);
    mini_ep_getopt(self->ep, MINI_SOL_SOCKET, MINI_SNDBUF, &val, &sz);
    mini_assert(sz == sizeof(val));
    mini_usock_setsockopt(&self->usock, SOL_SOCKET, SO_SNDBUF,
                          &val, sizeof(val));
    sz = sizeof(val);
    mini_ep_getopt(self->ep, MINI_SOL_SOCKET, MINI_RCVBUF, &val, &sz);
    mini_assert(sz == sizeof(val));
    mini_usock_setsockopt(&self->usock, SOL_SOCKET, SO_RCVBUF,
                          &val, sizeof(val));
    sz = sizeof(val);
    mini_ep_getopt(self->ep, MINI_TCP, MINI_TCP_NODELAY, &val, &sz);
    mini_assert(sz == sizeof(val));
    mini_usock_setsockopt(&self->usock, IPPROTO_TCP, TCP_NODELAY,
                          &val, sizeof(val));

    /*  Bind the socket to the local network interface. */
    rc = mini_usock_bind(&self->usock, (struct sockaddr *)&local, locallen);
    if (mini_slow(rc != 0))
    {
        mini_backoff_start(&self->retry);
        self->state = MINI_CTCP_STATE_WAITING;
        return;
    }

    /*  Start connecting. */
    mini_usock_connect(&self->usock, (struct sockaddr *)&remote, remotelen);
    self->state = MINI_CTCP_STATE_CONNECTING;
    mini_ep_stat_increment(self->ep, MINI_STAT_INPROGRESS_CONNECTIONS, 1);
}
