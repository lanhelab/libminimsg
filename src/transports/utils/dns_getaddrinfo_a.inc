/*
    Copyright (c) 2012-2013 Martin Sustrik  All rights reserved.

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "literal.h"

#include "../../utils/err.h"
#include "../../utils/cont.h"
#include "../../utils/attr.h"

#include "../../io/ctx.h"

#include <signal.h>

#define MINI_DNS_STATE_IDLE 1
#define MINI_DNS_STATE_RESOLVING 2
#define MINI_DNS_STATE_DONE 3
#define MINI_DNS_STATE_STOPPING 4

#define MINI_DNS_ACTION_DONE 1
#define MINI_DNS_ACTION_CANCELLED 2

/*  Private functions. */
static void mini_dns_notify(union sigval);
static void mini_dns_handler(struct mini_fsm *self, int src, int type,
                             void *srcptr);
static void mini_dns_shutdown(struct mini_fsm *self, int src, int type,
                              void *srcptr);

void mini_dns_init(struct mini_dns *self, int src, struct mini_fsm *owner)
{
    mini_fsm_init(&self->fsm, mini_dns_handler, mini_dns_shutdown, src, self, owner);
    self->state = MINI_DNS_STATE_IDLE;
    mini_fsm_event_init(&self->done);
}

void mini_dns_term(struct mini_dns *self)
{
    mini_assert_state(self, MINI_DNS_STATE_IDLE);

    mini_fsm_event_term(&self->done);
    mini_fsm_term(&self->fsm);
}

int mini_dns_isidle(struct mini_dns *self)
{
    return mini_fsm_isidle(&self->fsm);
}

void mini_dns_start(struct mini_dns *self, const char *addr, size_t addrlen,
                    int ipv4only, struct mini_dns_result *result)
{
    int rc;
    struct gaicb *pgcb;
    struct sigevent sev;

    mini_assert_state(self, MINI_DNS_STATE_IDLE);

    self->result = result;

    /*  Try to resolve the supplied string as a literal address. In this case,
        there's no DNS lookup involved. */
    rc = mini_literal_resolve(addr, addrlen, ipv4only, &self->result->addr,
                              &self->result->addrlen);
    if (rc == 0)
    {
        self->result->error = 0;
        mini_fsm_start(&self->fsm);
        return;
    }
    errnum_assert(rc == -EINVAL, -rc);

    /*  Make a zero-terminated copy of the address string. */
    mini_assert(sizeof(self->hostname) > addrlen);
    memcpy(self->hostname, addr, addrlen);
    self->hostname[addrlen] = 0;

    /*  Start asynchronous DNS lookup. */
    memset(&self->request, 0, sizeof(self->request));
    if (ipv4only)
        self->request.ai_family = AF_INET;
    else
    {
        self->request.ai_family = AF_INET6;
#ifdef AI_V4MAPPED
        self->request.ai_flags = AI_V4MAPPED;
#endif
    }
    self->request.ai_socktype = SOCK_STREAM;

    memset(&self->gcb, 0, sizeof(self->gcb));
    self->gcb.ar_name = self->hostname;
    self->gcb.ar_service = NULL;
    self->gcb.ar_request = &self->request;
    self->gcb.ar_result = NULL;
    pgcb = &self->gcb;

    memset(&sev, 0, sizeof(sev));
    sev.sigev_notify = SIGEV_THREAD;
    sev.sigev_notify_function = mini_dns_notify;
    sev.sigev_value.sival_ptr = self;

    rc = getaddrinfo_a(GAI_NOWAIT, &pgcb, 1, &sev);
    mini_assert(rc == 0);

    self->result->error = EINPROGRESS;
    mini_fsm_start(&self->fsm);
}

void mini_dns_stop(struct mini_dns *self)
{
    mini_fsm_stop(&self->fsm);
}

static void mini_dns_notify(union sigval sval)
{
    int rc;
    struct mini_dns *self;

    self = (struct mini_dns *)sval.sival_ptr;

    mini_ctx_enter(self->fsm.ctx);
    rc = gai_error(&self->gcb);
    if (rc == EAI_CANCELED)
    {
        mini_fsm_action(&self->fsm, MINI_DNS_ACTION_CANCELLED);
    }
    else if (rc != 0)
    {
        self->result->error = EINVAL;
        mini_fsm_action(&self->fsm, MINI_DNS_ACTION_DONE);
    }
    else
    {
        self->result->error = 0;
        mini_assert(self->gcb.ar_result->ai_addrlen <=
                    sizeof(struct sockaddr_storage));
        memcpy(&self->result->addr, self->gcb.ar_result->ai_addr,
               self->gcb.ar_result->ai_addrlen);
        self->result->addrlen = (size_t)self->gcb.ar_result->ai_addrlen;
        freeaddrinfo(self->gcb.ar_result);
        mini_fsm_action(&self->fsm, MINI_DNS_ACTION_DONE);
    }
    mini_ctx_leave(self->fsm.ctx);
}

static void mini_dns_shutdown(struct mini_fsm *self, int src, int type,
                              MINI_UNUSED void *srcptr)
{
    int rc;
    struct mini_dns *dns;

    dns = mini_cont(self, struct mini_dns, fsm);

    if (mini_slow(src == MINI_FSM_ACTION && type == MINI_FSM_STOP))
    {
        if (dns->state == MINI_DNS_STATE_RESOLVING)
        {
            rc = gai_cancel(&dns->gcb);
            if (rc == EAI_CANCELED)
            {
                mini_fsm_stopped(&dns->fsm, MINI_DNS_STOPPED);
                dns->state = MINI_DNS_STATE_IDLE;
                return;
            }
            if (rc == EAI_NOTCANCELED || rc == EAI_ALLDONE)
            {
                dns->state = MINI_DNS_STATE_STOPPING;
                return;
            }
            mini_assert(0);
        }
        mini_fsm_stopped(&dns->fsm, MINI_DNS_STOPPED);
        dns->state = MINI_DNS_STATE_IDLE;
        return;
    }
    if (mini_slow(dns->state == MINI_DNS_STATE_STOPPING))
    {
        if (src == MINI_FSM_ACTION && (type == MINI_DNS_ACTION_CANCELLED ||
                                       type == MINI_DNS_ACTION_DONE))
        {
            mini_fsm_stopped(&dns->fsm, MINI_DNS_STOPPED);
            dns->state = MINI_DNS_STATE_IDLE;
            return;
        }
        return;
    }

    mini_fsm_bad_state(dns->state, src, type);
}

static void mini_dns_handler(struct mini_fsm *self, int src, int type,
                             MINI_UNUSED void *srcptr)
{
    struct mini_dns *dns;

    dns = mini_cont(self, struct mini_dns, fsm);

    switch (dns->state)
    {
        /******************************************************************************/
        /*  IDLE state.                                                               */
        /******************************************************************************/
    case MINI_DNS_STATE_IDLE:
        switch (src)
        {
        case MINI_FSM_ACTION:
            switch (type)
            {
            case MINI_FSM_START:
                if (dns->result->error == EINPROGRESS)
                {
                    dns->state = MINI_DNS_STATE_RESOLVING;
                    return;
                }
                mini_fsm_raise(&dns->fsm, &dns->done, MINI_DNS_DONE);
                dns->state = MINI_DNS_STATE_DONE;
                return;
            default:
                mini_fsm_bad_action(dns->state, src, type);
            }
        default:
            mini_fsm_bad_source(dns->state, src, type);
        }

        /******************************************************************************/
        /*  RESOLVING state.                                                          */
        /******************************************************************************/
    case MINI_DNS_STATE_RESOLVING:
        switch (src)
        {
        case MINI_FSM_ACTION:
            switch (type)
            {
            case MINI_DNS_ACTION_DONE:
                mini_fsm_raise(&dns->fsm, &dns->done, MINI_DNS_DONE);
                dns->state = MINI_DNS_STATE_DONE;
                return;
            default:
                mini_fsm_bad_action(dns->state, src, type);
            }
        default:
            mini_fsm_bad_source(dns->state, src, type);
        }

        /******************************************************************************/
        /*  DONE state.                                                               */
        /******************************************************************************/
    case MINI_DNS_STATE_DONE:
        mini_fsm_bad_source(dns->state, src, type);

        /******************************************************************************/
        /*  Invalid state.                                                            */
        /******************************************************************************/
    default:
        mini_fsm_bad_state(dns->state, src, type);
    }
}
