/*
    Copyright (c) 2013 Martin Sustrik  All rights reserved.

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#ifndef MINI_BACKOFF_INCLUDED
#define MINI_BACKOFF_INCLUDED

#include "../../io/timer.h"

/*  Timer with exponential backoff. Actual wating time is (2^n-1)*minivl,
    meaning that first wait is 0 ms long, second one is minivl ms long etc. */

#define MINI_BACKOFF_TIMEOUT MINI_TIMER_TIMEOUT
#define MINI_BACKOFF_STOPPED MINI_TIMER_STOPPED

struct mini_backoff
{
    struct mini_timer timer;
    int minivl;
    int maxivl;
    int n;
};

void mini_backoff_init(struct mini_backoff *self, int src, int minivl, int maxivl,
                       struct mini_fsm *owner);
void mini_backoff_term(struct mini_backoff *self);

int mini_backoff_isidle(struct mini_backoff *self);
void mini_backoff_start(struct mini_backoff *self);
void mini_backoff_stop(struct mini_backoff *self);

void mini_backoff_reset(struct mini_backoff *self);

#endif
