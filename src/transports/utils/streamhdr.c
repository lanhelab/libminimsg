/*
    Copyright (c) 2013 Martin Sustrik  All rights reserved.
    Copyright 2017 Garrett D'Amore <garrett@damore.org>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "streamhdr.h"

#include "../../io/timer.h"

#include "../../utils/err.h"
#include "../../utils/cont.h"
#include "../../utils/fast.h"
#include "../../utils/wire.h"
#include "../../utils/attr.h"

#include <stddef.h>
#include <string.h>

#define MINI_STREAMHDR_STATE_IDLE 1
#define MINI_STREAMHDR_STATE_SENDING 2
#define MINI_STREAMHDR_STATE_RECEIVING 3
#define MINI_STREAMHDR_STATE_STOPPING_TIMER_ERROR 4
#define MINI_STREAMHDR_STATE_STOPPING_TIMER_DONE 5
#define MINI_STREAMHDR_STATE_DONE 6
#define MINI_STREAMHDR_STATE_STOPPING 7

#define MINI_STREAMHDR_SRC_USOCK 1
#define MINI_STREAMHDR_SRC_TIMER 2

/*  Private functions. */
static void mini_streamhdr_handler(struct mini_fsm *self, int src, int type,
                                   void *srcptr);
static void mini_streamhdr_shutdown(struct mini_fsm *self, int src, int type,
                                    void *srcptr);

void mini_streamhdr_init(struct mini_streamhdr *self, int src,
                         struct mini_fsm *owner)
{
    mini_fsm_init(&self->fsm, mini_streamhdr_handler, mini_streamhdr_shutdown,
                  src, self, owner);
    self->state = MINI_STREAMHDR_STATE_IDLE;
    mini_timer_init(&self->timer, MINI_STREAMHDR_SRC_TIMER, &self->fsm);
    mini_fsm_event_init(&self->done);

    self->usock = NULL;
    self->usock_owner.src = -1;
    self->usock_owner.fsm = NULL;
    self->pipebase = NULL;
}

void mini_streamhdr_term(struct mini_streamhdr *self)
{
    mini_assert_state(self, MINI_STREAMHDR_STATE_IDLE);

    mini_fsm_event_term(&self->done);
    mini_timer_term(&self->timer);
    mini_fsm_term(&self->fsm);
}

int mini_streamhdr_isidle(struct mini_streamhdr *self)
{
    return mini_fsm_isidle(&self->fsm);
}

void mini_streamhdr_start(struct mini_streamhdr *self, struct mini_usock *usock,
                          struct mini_pipebase *pipebase)
{
    size_t sz;
    int protocol;

    /*  Take ownership of the underlying socket. */
    mini_assert(self->usock == NULL && self->usock_owner.fsm == NULL);
    self->usock_owner.src = MINI_STREAMHDR_SRC_USOCK;
    self->usock_owner.fsm = &self->fsm;
    mini_usock_swap_owner(usock, &self->usock_owner);
    self->usock = usock;
    self->pipebase = pipebase;

    /*  Get the protocol identifier. */
    sz = sizeof(protocol);
    mini_pipebase_getopt(pipebase, MINI_SOL_SOCKET, MINI_PROTOCOL, &protocol, &sz);
    mini_assert(sz == sizeof(protocol));

    /*  Compose the protocol header. */
    memcpy(self->protohdr, "\0SP\0\0\0\0\0", 8);
    mini_puts(self->protohdr + 4, (uint16_t)protocol);

    /*  Launch the state machine. */
    mini_fsm_start(&self->fsm);
}

void mini_streamhdr_stop(struct mini_streamhdr *self)
{
    mini_fsm_stop(&self->fsm);
}

static void mini_streamhdr_shutdown(struct mini_fsm *self, int src, int type,
                                    MINI_UNUSED void *srcptr)
{
    struct mini_streamhdr *streamhdr;

    streamhdr = mini_cont(self, struct mini_streamhdr, fsm);

    if (mini_slow(src == MINI_FSM_ACTION && type == MINI_FSM_STOP))
    {
        mini_timer_stop(&streamhdr->timer);
        streamhdr->state = MINI_STREAMHDR_STATE_STOPPING;
    }
    if (mini_slow(streamhdr->state == MINI_STREAMHDR_STATE_STOPPING))
    {
        if (!mini_timer_isidle(&streamhdr->timer))
            return;
        streamhdr->state = MINI_STREAMHDR_STATE_IDLE;
        mini_fsm_stopped(&streamhdr->fsm, MINI_STREAMHDR_STOPPED);
        return;
    }

    mini_fsm_bad_state(streamhdr->state, src, type);
}

static void mini_streamhdr_handler(struct mini_fsm *self, int src, int type,
                                   MINI_UNUSED void *srcptr)
{
    struct mini_streamhdr *streamhdr;
    struct mini_iovec iovec;
    int protocol;

    streamhdr = mini_cont(self, struct mini_streamhdr, fsm);

    switch (streamhdr->state)
    {

        /******************************************************************************/
        /*  IDLE state.                                                               */
        /******************************************************************************/
    case MINI_STREAMHDR_STATE_IDLE:
        switch (src)
        {

        case MINI_FSM_ACTION:
            switch (type)
            {
            case MINI_FSM_START:
                mini_timer_start(&streamhdr->timer, 1000);
                iovec.iov_base = streamhdr->protohdr;
                iovec.iov_len = sizeof(streamhdr->protohdr);
                mini_usock_send(streamhdr->usock, &iovec, 1);
                streamhdr->state = MINI_STREAMHDR_STATE_SENDING;
                return;
            default:
                mini_fsm_bad_action(streamhdr->state, src, type);
            }

        default:
            mini_fsm_bad_source(streamhdr->state, src, type);
        }

        /******************************************************************************/
        /*  SENDING state.                                                            */
        /******************************************************************************/
    case MINI_STREAMHDR_STATE_SENDING:
        switch (src)
        {

        case MINI_STREAMHDR_SRC_USOCK:
            switch (type)
            {
            case MINI_USOCK_SENT:
                mini_usock_recv(streamhdr->usock, streamhdr->protohdr,
                                sizeof(streamhdr->protohdr), NULL);
                streamhdr->state = MINI_STREAMHDR_STATE_RECEIVING;
                return;
            case MINI_USOCK_SHUTDOWN:
                /*  Ignore it. Wait for ERROR event  */
                return;
            case MINI_USOCK_ERROR:
                mini_timer_stop(&streamhdr->timer);
                streamhdr->state = MINI_STREAMHDR_STATE_STOPPING_TIMER_ERROR;
                return;
            default:
                mini_fsm_bad_action(streamhdr->state, src, type);
            }

        case MINI_STREAMHDR_SRC_TIMER:
            switch (type)
            {
            case MINI_TIMER_TIMEOUT:
                mini_timer_stop(&streamhdr->timer);
                streamhdr->state = MINI_STREAMHDR_STATE_STOPPING_TIMER_ERROR;
                return;
            default:
                mini_fsm_bad_action(streamhdr->state, src, type);
            }

        default:
            mini_fsm_bad_source(streamhdr->state, src, type);
        }

        /******************************************************************************/
        /*  RECEIVING state.                                                          */
        /******************************************************************************/
    case MINI_STREAMHDR_STATE_RECEIVING:
        switch (src)
        {

        case MINI_STREAMHDR_SRC_USOCK:
            switch (type)
            {
            case MINI_USOCK_RECEIVED:

                /*  Here we are checking whether the peer speaks the same
                    protocol as this socket. */
                if (memcmp(streamhdr->protohdr, "\0SP\0", 4) != 0)
                    goto invalidhdr;
                protocol = mini_gets(streamhdr->protohdr + 4);
                if (!mini_pipebase_ispeer(streamhdr->pipebase, protocol))
                    goto invalidhdr;
                mini_timer_stop(&streamhdr->timer);
                streamhdr->state = MINI_STREAMHDR_STATE_STOPPING_TIMER_DONE;
                return;
            case MINI_USOCK_SHUTDOWN:
                /*  Ignore it. Wait for ERROR event  */
                return;
            case MINI_USOCK_ERROR:
            invalidhdr:
                mini_timer_stop(&streamhdr->timer);
                streamhdr->state = MINI_STREAMHDR_STATE_STOPPING_TIMER_ERROR;
                return;
            default:
                mini_assert(0);
                return;
            }

        case MINI_STREAMHDR_SRC_TIMER:
            switch (type)
            {
            case MINI_TIMER_TIMEOUT:
                mini_timer_stop(&streamhdr->timer);
                streamhdr->state = MINI_STREAMHDR_STATE_STOPPING_TIMER_ERROR;
                return;
            default:
                mini_fsm_bad_action(streamhdr->state, src, type);
            }

        default:
            mini_fsm_bad_source(streamhdr->state, src, type);
        }

        /******************************************************************************/
        /*  STOPPING_TIMER_ERROR state.                                               */
        /******************************************************************************/
    case MINI_STREAMHDR_STATE_STOPPING_TIMER_ERROR:
        switch (src)
        {

        case MINI_STREAMHDR_SRC_USOCK:
            /*  It's safe to ignore usock event when we are stopping, but there
                is only a subset of events that are plausible. */
            return;

        case MINI_STREAMHDR_SRC_TIMER:
            switch (type)
            {
            case MINI_TIMER_STOPPED:
                mini_usock_swap_owner(streamhdr->usock, &streamhdr->usock_owner);
                streamhdr->usock = NULL;
                streamhdr->usock_owner.src = -1;
                streamhdr->usock_owner.fsm = NULL;
                streamhdr->state = MINI_STREAMHDR_STATE_DONE;
                mini_fsm_raise(&streamhdr->fsm, &streamhdr->done,
                               MINI_STREAMHDR_ERROR);
                return;
            default:
                mini_fsm_bad_action(streamhdr->state, src, type);
            }

        default:
            mini_fsm_bad_source(streamhdr->state, src, type);
        }

        /******************************************************************************/
        /*  STOPPING_TIMER_DONE state.                                                */
        /******************************************************************************/
    case MINI_STREAMHDR_STATE_STOPPING_TIMER_DONE:
        switch (src)
        {

        case MINI_STREAMHDR_SRC_USOCK:
            /*  It's safe to ignore usock event when we are stopping. */
            return;

        case MINI_STREAMHDR_SRC_TIMER:
            switch (type)
            {
            case MINI_TIMER_STOPPED:
                mini_usock_swap_owner(streamhdr->usock, &streamhdr->usock_owner);
                streamhdr->usock = NULL;
                streamhdr->usock_owner.src = -1;
                streamhdr->usock_owner.fsm = NULL;
                streamhdr->state = MINI_STREAMHDR_STATE_DONE;
                mini_fsm_raise(&streamhdr->fsm, &streamhdr->done,
                               MINI_STREAMHDR_OK);
                return;
            default:
                mini_fsm_bad_action(streamhdr->state, src, type);
            }

        default:
            mini_fsm_bad_source(streamhdr->state, src, type);
        }

        /******************************************************************************/
        /*  DONE state.                                                               */
        /*  The header exchange was either done successfully of failed. There's       */
        /*  nothing that can be done in this state except stopping the object.        */
        /******************************************************************************/
    case MINI_STREAMHDR_STATE_DONE:
        mini_fsm_bad_source(streamhdr->state, src, type);

        /******************************************************************************/
        /*  Invalid state.                                                            */
        /******************************************************************************/
    default:
        mini_fsm_bad_state(streamhdr->state, src, type);
    }
}
