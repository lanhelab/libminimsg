/*
    Copyright (c) 2012-2013 250bpm s.r.o.  All rights reserved.
    Copyright (c) 2014-2016 Jack R. Dunaway. All rights reserved.
    Copyright 2016 Garrett D'Amore <garrett@damore.org>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "cws.h"
#include "sws.h"

#include "../../ws.h"

#include "../utils/dns.h"
#include "../utils/port.h"
#include "../utils/iface.h"
#include "../utils/backoff.h"
#include "../utils/literal.h"

#include "../../io/fsm.h"
#include "../../io/usock.h"

#include "../../utils/err.h"
#include "../../utils/cont.h"
#include "../../utils/alloc.h"
#include "../../utils/fast.h"
#include "../../utils/attr.h"

#include <string.h>

#if defined MINI_HAVE_WINDOWS
#include "../../utils/win.h"
#else
#include <unistd.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#endif

#define MINI_CWS_STATE_IDLE 1
#define MINI_CWS_STATE_RESOLVING 2
#define MINI_CWS_STATE_STOPPING_DNS 3
#define MINI_CWS_STATE_CONNECTING 4
#define MINI_CWS_STATE_ACTIVE 5
#define MINI_CWS_STATE_STOPPING_SWS 6
#define MINI_CWS_STATE_STOPPING_USOCK 7
#define MINI_CWS_STATE_WAITING 8
#define MINI_CWS_STATE_STOPPING_BACKOFF 9
#define MINI_CWS_STATE_STOPPING_SWS_FINAL 10
#define MINI_CWS_STATE_STOPPING 11

#define MINI_CWS_SRC_USOCK 1
#define MINI_CWS_SRC_RECONNECT_TIMER 2
#define MINI_CWS_SRC_DNS 3
#define MINI_CWS_SRC_SWS 4

struct mini_cws
{

    /*  The state machine. */
    struct mini_fsm fsm;
    int state;

    struct mini_ep *ep;

    /*  The underlying WS socket. */
    struct mini_usock usock;

    /*  Used to wait before retrying to connect. */
    struct mini_backoff retry;

    /*  Defines message validation and framing. */
    uint8_t msg_type;

    /*  State machine that handles the active part of the connection
        lifetime. */
    struct mini_sws sws;

    /*  Parsed parts of the connection URI. */
    struct mini_chunkref resource;
    struct mini_chunkref remote_host;
    struct mini_chunkref nic;
    int remote_port;
    size_t remote_hostname_len;

    /*  If a close handshake is performed, this flag signals to not
        begin automatic reconnect retries. */
    int peer_gone;

    /*  DNS resolver used to convert textual address into actual IP address
        along with the variable to hold the result. */
    struct mini_dns dns;
    struct mini_dns_result dns_result;
};

/*  mini_ep virtual interface implementation. */
static void mini_cws_stop(void *);
static void mini_cws_destroy(void *);
const struct mini_ep_ops mini_cws_ep_ops = {
    mini_cws_stop,
    mini_cws_destroy};

/*  Private functions. */
static void mini_cws_handler(struct mini_fsm *self, int src, int type,
                             void *srcptr);
static void mini_cws_shutdown(struct mini_fsm *self, int src, int type,
                              void *srcptr);
static void mini_cws_start_resolving(struct mini_cws *self);
static void mini_cws_start_connecting(struct mini_cws *self,
                                      struct sockaddr_storage *ss, size_t sslen);

int mini_cws_create(struct mini_ep *ep)
{
    int rc;
    const char *addr;
    size_t addrlen;
    const char *semicolon;
    const char *hostname;
    size_t hostlen;
    const char *colon;
    const char *slash;
    const char *resource;
    size_t resourcelen;
    struct sockaddr_storage ss;
    size_t sslen;
    int ipv4only;
    size_t ipv4onlylen;
    struct mini_cws *self;
    int reconnect_ivl;
    int reconnect_ivl_max;
    int msg_type;
    size_t sz;

    /*  Allocate the new endpoint object. */
    self = mini_alloc(sizeof(struct mini_cws), "cws");
    alloc_assert(self);
    self->ep = ep;
    self->peer_gone = 0;

    /*  Initalise the endpoint. */
    mini_ep_tran_setup(ep, &mini_cws_ep_ops, self);

    /*  Check whether IPv6 is to be used. */
    ipv4onlylen = sizeof(ipv4only);
    mini_ep_getopt(ep, MINI_SOL_SOCKET, MINI_IPV4ONLY, &ipv4only, &ipv4onlylen);
    mini_assert(ipv4onlylen == sizeof(ipv4only));

    /*  Start parsing the address. */
    addr = mini_ep_getaddr(ep);
    addrlen = strlen(addr);
    semicolon = strchr(addr, ';');
    hostname = semicolon ? semicolon + 1 : addr;
    colon = strrchr(addr, ':');
    slash = colon ? strchr(colon, '/') : strchr(addr, '/');
    resource = slash ? slash : addr + addrlen;
    self->remote_hostname_len = colon ? colon - hostname : resource - hostname;

    /*  Host contains both hostname and port. */
    hostlen = resource - hostname;

    /*  Parse the port; assume port 80 if not explicitly declared. */
    if (colon != NULL)
    {
        rc = mini_port_resolve(colon + 1, resource - colon - 1);
        if (rc < 0)
        {
            mini_free(self);
            return -EINVAL;
        }
        self->remote_port = rc;
    }
    else
    {
        self->remote_port = 80;
    }

    /*  Check whether the host portion of the address is either a literal
        or a valid hostname. */
    if (mini_dns_check_hostname(hostname, self->remote_hostname_len) < 0 &&
        mini_literal_resolve(hostname, self->remote_hostname_len, ipv4only,
                             &ss, &sslen) < 0)
    {
        mini_free(self);
        return -EINVAL;
    }

    /*  If local address is specified, check whether it is valid. */
    if (semicolon)
    {
        rc = mini_iface_resolve(addr, semicolon - addr, ipv4only, &ss, &sslen);
        if (rc < 0)
        {
            mini_free(self);
            return -ENODEV;
        }
    }

    /*  At this point, the address is valid, so begin allocating resources. */
    mini_chunkref_init(&self->remote_host, hostlen + 1);
    memcpy(mini_chunkref_data(&self->remote_host), hostname, hostlen);
    ((uint8_t *)mini_chunkref_data(&self->remote_host))[hostlen] = '\0';

    if (semicolon)
    {
        mini_chunkref_init(&self->nic, semicolon - addr);
        memcpy(mini_chunkref_data(&self->nic),
               addr, semicolon - addr);
    }
    else
    {
        mini_chunkref_init(&self->nic, 1);
        memcpy(mini_chunkref_data(&self->nic), "*", 1);
    }

    /*  The requested resource is used in opening handshake. */
    resourcelen = strlen(resource);
    if (resourcelen)
    {
        mini_chunkref_init(&self->resource, resourcelen + 1);
        strncpy(mini_chunkref_data(&self->resource),
                resource, resourcelen + 1);
    }
    else
    {
        /*  No resource specified, so allocate base path. */
        mini_chunkref_init(&self->resource, 2);
        strncpy(mini_chunkref_data(&self->resource), "/", 2);
    }

    /*  Initialise the structure. */
    mini_fsm_init_root(&self->fsm, mini_cws_handler, mini_cws_shutdown,
                       mini_ep_getctx(ep));
    self->state = MINI_CWS_STATE_IDLE;
    mini_usock_init(&self->usock, MINI_CWS_SRC_USOCK, &self->fsm);

    sz = sizeof(msg_type);
    mini_ep_getopt(ep, MINI_WS, MINI_WS_MSG_TYPE, &msg_type, &sz);
    mini_assert(sz == sizeof(msg_type));
    self->msg_type = (uint8_t)msg_type;

    sz = sizeof(reconnect_ivl);
    mini_ep_getopt(ep, MINI_SOL_SOCKET, MINI_RECONNECT_IVL, &reconnect_ivl, &sz);
    mini_assert(sz == sizeof(reconnect_ivl));
    sz = sizeof(reconnect_ivl_max);
    mini_ep_getopt(ep, MINI_SOL_SOCKET, MINI_RECONNECT_IVL_MAX,
                   &reconnect_ivl_max, &sz);
    mini_assert(sz == sizeof(reconnect_ivl_max));
    if (reconnect_ivl_max == 0)
        reconnect_ivl_max = reconnect_ivl;
    mini_backoff_init(&self->retry, MINI_CWS_SRC_RECONNECT_TIMER,
                      reconnect_ivl, reconnect_ivl_max, &self->fsm);

    mini_sws_init(&self->sws, MINI_CWS_SRC_SWS, ep, &self->fsm);
    mini_dns_init(&self->dns, MINI_CWS_SRC_DNS, &self->fsm);

    /*  Start the state machine. */
    mini_fsm_start(&self->fsm);

    return 0;
}

static void mini_cws_stop(void *self)
{
    struct mini_cws *cws = self;

    mini_fsm_stop(&cws->fsm);
}

static void mini_cws_destroy(void *self)
{
    struct mini_cws *cws = self;

    mini_chunkref_term(&cws->resource);
    mini_chunkref_term(&cws->remote_host);
    mini_chunkref_term(&cws->nic);
    mini_dns_term(&cws->dns);
    mini_sws_term(&cws->sws);
    mini_backoff_term(&cws->retry);
    mini_usock_term(&cws->usock);
    mini_fsm_term(&cws->fsm);

    mini_free(cws);
}

static void mini_cws_shutdown(struct mini_fsm *self, int src, int type,
                              MINI_UNUSED void *srcptr)
{
    struct mini_cws *cws;

    cws = mini_cont(self, struct mini_cws, fsm);

    if (mini_slow(src == MINI_FSM_ACTION && type == MINI_FSM_STOP))
    {
        if (!mini_sws_isidle(&cws->sws))
        {
            mini_ep_stat_increment(cws->ep, MINI_STAT_DROPPED_CONNECTIONS, 1);
            mini_sws_stop(&cws->sws);
        }
        cws->state = MINI_CWS_STATE_STOPPING_SWS_FINAL;
    }
    if (mini_slow(cws->state == MINI_CWS_STATE_STOPPING_SWS_FINAL))
    {
        if (!mini_sws_isidle(&cws->sws))
            return;
        mini_backoff_stop(&cws->retry);
        mini_usock_stop(&cws->usock);
        mini_dns_stop(&cws->dns);
        cws->state = MINI_CWS_STATE_STOPPING;
    }
    if (mini_slow(cws->state == MINI_CWS_STATE_STOPPING))
    {
        if (!mini_backoff_isidle(&cws->retry) ||
            !mini_usock_isidle(&cws->usock) ||
            !mini_dns_isidle(&cws->dns))
            return;
        cws->state = MINI_CWS_STATE_IDLE;
        mini_fsm_stopped_noevent(&cws->fsm);
        mini_ep_stopped(cws->ep);
        return;
    }

    mini_fsm_bad_state(cws->state, src, type);
}

static void mini_cws_handler(struct mini_fsm *self, int src, int type,
                             MINI_UNUSED void *srcptr)
{
    struct mini_cws *cws;

    cws = mini_cont(self, struct mini_cws, fsm);

    switch (cws->state)
    {

        /******************************************************************************/
        /*  IDLE state.                                                               */
        /*  The state machine wasn't yet started.                                     */
        /******************************************************************************/
    case MINI_CWS_STATE_IDLE:
        switch (src)
        {

        case MINI_FSM_ACTION:
            switch (type)
            {
            case MINI_FSM_START:
                mini_cws_start_resolving(cws);
                return;
            default:
                mini_fsm_bad_action(cws->state, src, type);
            }

        default:
            mini_fsm_bad_source(cws->state, src, type);
        }

        /******************************************************************************/
        /*  RESOLVING state.                                                          */
        /*  Name of the host to connect to is being resolved to get an IP address.    */
        /******************************************************************************/
    case MINI_CWS_STATE_RESOLVING:
        switch (src)
        {

        case MINI_CWS_SRC_DNS:
            switch (type)
            {
            case MINI_DNS_DONE:
                mini_dns_stop(&cws->dns);
                cws->state = MINI_CWS_STATE_STOPPING_DNS;
                return;
            default:
                mini_fsm_bad_action(cws->state, src, type);
            }

        default:
            mini_fsm_bad_source(cws->state, src, type);
        }

        /******************************************************************************/
        /*  STOPPING_DNS state.                                                       */
        /*  dns object was asked to stop but it haven't stopped yet.                  */
        /******************************************************************************/
    case MINI_CWS_STATE_STOPPING_DNS:
        switch (src)
        {

        case MINI_CWS_SRC_DNS:
            switch (type)
            {
            case MINI_DNS_STOPPED:
                if (cws->dns_result.error == 0)
                {
                    mini_cws_start_connecting(cws, &cws->dns_result.addr,
                                              cws->dns_result.addrlen);
                    return;
                }
                mini_backoff_start(&cws->retry);
                cws->state = MINI_CWS_STATE_WAITING;
                return;
            default:
                mini_fsm_bad_action(cws->state, src, type);
            }

        default:
            mini_fsm_bad_source(cws->state, src, type);
        }

        /******************************************************************************/
        /*  CONNECTING state.                                                         */
        /*  Non-blocking connect is under way.                                        */
        /******************************************************************************/
    case MINI_CWS_STATE_CONNECTING:
        switch (src)
        {

        case MINI_CWS_SRC_USOCK:
            switch (type)
            {
            case MINI_USOCK_CONNECTED:
                mini_sws_start(&cws->sws, &cws->usock, MINI_WS_CLIENT,
                               mini_chunkref_data(&cws->resource),
                               mini_chunkref_data(&cws->remote_host), cws->msg_type);
                cws->state = MINI_CWS_STATE_ACTIVE;
                cws->peer_gone = 0;
                mini_ep_stat_increment(cws->ep,
                                       MINI_STAT_INPROGRESS_CONNECTIONS, -1);
                mini_ep_stat_increment(cws->ep,
                                       MINI_STAT_ESTABLISHED_CONNECTIONS, 1);
                mini_ep_clear_error(cws->ep);
                return;
            case MINI_USOCK_ERROR:
                mini_ep_set_error(cws->ep, mini_usock_geterrno(&cws->usock));
                mini_usock_stop(&cws->usock);
                cws->state = MINI_CWS_STATE_STOPPING_USOCK;
                mini_ep_stat_increment(cws->ep,
                                       MINI_STAT_INPROGRESS_CONNECTIONS, -1);
                mini_ep_stat_increment(cws->ep, MINI_STAT_CONNECT_ERRORS, 1);
                return;
            default:
                mini_fsm_bad_action(cws->state, src, type);
            }

        default:
            mini_fsm_bad_source(cws->state, src, type);
        }

        /******************************************************************************/
        /*  ACTIVE state.                                                             */
        /*  Connection is established and handled by the sws state machine.           */
        /******************************************************************************/
    case MINI_CWS_STATE_ACTIVE:
        switch (src)
        {

        case MINI_CWS_SRC_SWS:
            switch (type)
            {
            case MINI_SWS_RETURN_CLOSE_HANDSHAKE:
                /*  Peer closed connection without intention to reconnect, or
                    local endpoint failed remote because of invalid data. */
                mini_sws_stop(&cws->sws);
                cws->state = MINI_CWS_STATE_STOPPING_SWS;
                cws->peer_gone = 1;
                return;
            case MINI_SWS_RETURN_ERROR:
                mini_sws_stop(&cws->sws);
                cws->state = MINI_CWS_STATE_STOPPING_SWS;
                mini_ep_stat_increment(cws->ep, MINI_STAT_BROKEN_CONNECTIONS, 1);
                return;
            default:
                mini_fsm_bad_action(cws->state, src, type);
            }

        default:
            mini_fsm_bad_source(cws->state, src, type);
        }

        /******************************************************************************/
        /*  STOPPING_SWS state.                                                       */
        /*  sws object was asked to stop but it haven't stopped yet.                  */
        /******************************************************************************/
    case MINI_CWS_STATE_STOPPING_SWS:
        switch (src)
        {

        case MINI_CWS_SRC_SWS:
            switch (type)
            {
            case MINI_USOCK_SHUTDOWN:
                return;
            case MINI_SWS_RETURN_STOPPED:
                mini_usock_stop(&cws->usock);
                cws->state = MINI_CWS_STATE_STOPPING_USOCK;
                return;
            default:
                mini_fsm_bad_action(cws->state, src, type);
            }

        default:
            mini_fsm_bad_source(cws->state, src, type);
        }

        /******************************************************************************/
        /*  STOPPING_USOCK state.                                                     */
        /*  usock object was asked to stop but it haven't stopped yet.                */
        /******************************************************************************/
    case MINI_CWS_STATE_STOPPING_USOCK:
        switch (src)
        {

        case MINI_CWS_SRC_USOCK:
            switch (type)
            {
            case MINI_USOCK_SHUTDOWN:
                return;
            case MINI_USOCK_STOPPED:
                /*  If the peer has confirmed itself gone with a Closing
                    Handshake, or if the local endpoint failed the remote,
                    don't try to reconnect. */
                if (!cws->peer_gone)
                {
                    mini_backoff_start(&cws->retry);
                    cws->state = MINI_CWS_STATE_WAITING;
                }
                return;
            default:
                mini_fsm_bad_action(cws->state, src, type);
            }

        default:
            mini_fsm_bad_source(cws->state, src, type);
        }

        /******************************************************************************/
        /*  WAITING state.                                                            */
        /*  Waiting before re-connection is attempted. This way we won't overload     */
        /*  the system by continuous re-connection attempts.                          */
        /******************************************************************************/
    case MINI_CWS_STATE_WAITING:
        switch (src)
        {

        case MINI_CWS_SRC_RECONNECT_TIMER:
            switch (type)
            {
            case MINI_BACKOFF_TIMEOUT:
                mini_backoff_stop(&cws->retry);
                cws->state = MINI_CWS_STATE_STOPPING_BACKOFF;
                return;
            default:
                mini_fsm_bad_action(cws->state, src, type);
            }

        default:
            mini_fsm_bad_source(cws->state, src, type);
        }

        /******************************************************************************/
        /*  STOPPING_BACKOFF state.                                                   */
        /*  backoff object was asked to stop, but it haven't stopped yet.             */
        /******************************************************************************/
    case MINI_CWS_STATE_STOPPING_BACKOFF:
        switch (src)
        {

        case MINI_CWS_SRC_RECONNECT_TIMER:
            switch (type)
            {
            case MINI_BACKOFF_STOPPED:
                mini_cws_start_resolving(cws);
                return;
            default:
                mini_fsm_bad_action(cws->state, src, type);
            }

        default:
            mini_fsm_bad_source(cws->state, src, type);
        }

        /******************************************************************************/
        /*  Invalid state.                                                            */
        /******************************************************************************/
    default:
        mini_fsm_bad_state(cws->state, src, type);
    }
}

/******************************************************************************/
/*  State machine actions.                                                    */
/******************************************************************************/

static void mini_cws_start_resolving(struct mini_cws *self)
{
    int ipv4only;
    size_t ipv4onlylen;
    char *host;

    /*  Check whether IPv6 is to be used. */
    ipv4onlylen = sizeof(ipv4only);
    mini_ep_getopt(self->ep, MINI_SOL_SOCKET, MINI_IPV4ONLY,
                   &ipv4only, &ipv4onlylen);
    mini_assert(ipv4onlylen == sizeof(ipv4only));

    host = mini_chunkref_data(&self->remote_host);
    mini_assert(strlen(host) > 0);

    mini_dns_start(&self->dns, host, self->remote_hostname_len, ipv4only,
                   &self->dns_result);

    self->state = MINI_CWS_STATE_RESOLVING;
}

static void mini_cws_start_connecting(struct mini_cws *self,
                                      struct sockaddr_storage *ss, size_t sslen)
{
    int rc;
    struct sockaddr_storage remote;
    size_t remotelen;
    struct sockaddr_storage local;
    size_t locallen;
    int ipv4only;
    int val;
    size_t sz;

    memset(&remote, 0, sizeof(remote));
    memset(&local, 0, sizeof(local));

    /*  Check whether IPv6 is to be used. */
    sz = sizeof(ipv4only);
    mini_ep_getopt(self->ep, MINI_SOL_SOCKET, MINI_IPV4ONLY, &ipv4only, &sz);
    mini_assert(sz == sizeof(ipv4only));

    rc = mini_iface_resolve(mini_chunkref_data(&self->nic),
                            mini_chunkref_size(&self->nic), ipv4only, &local, &locallen);

    if (mini_slow(rc < 0))
    {
        mini_backoff_start(&self->retry);
        self->state = MINI_CWS_STATE_WAITING;
        return;
    }

    /*  Combine the remote address and the port. */
    remote = *ss;
    remotelen = sslen;
    if (remote.ss_family == AF_INET)
        ((struct sockaddr_in *)&remote)->sin_port = htons(self->remote_port);
    else if (remote.ss_family == AF_INET6)
        ((struct sockaddr_in6 *)&remote)->sin6_port = htons(self->remote_port);
    else
        mini_assert(0);

    /*  Try to start the underlying socket. */
    rc = mini_usock_start(&self->usock, remote.ss_family, SOCK_STREAM, 0);
    if (mini_slow(rc < 0))
    {
        mini_backoff_start(&self->retry);
        self->state = MINI_CWS_STATE_WAITING;
        return;
    }

    /*  Set the relevant socket options. */
    sz = sizeof(val);
    mini_ep_getopt(self->ep, MINI_SOL_SOCKET, MINI_SNDBUF, &val, &sz);
    mini_assert(sz == sizeof(val));
    mini_usock_setsockopt(&self->usock, SOL_SOCKET, SO_SNDBUF,
                          &val, sizeof(val));
    sz = sizeof(val);
    mini_ep_getopt(self->ep, MINI_SOL_SOCKET, MINI_RCVBUF, &val, &sz);
    mini_assert(sz == sizeof(val));
    mini_usock_setsockopt(&self->usock, SOL_SOCKET, SO_RCVBUF,
                          &val, sizeof(val));

    /*  Bind the socket to the local network interface. */
    rc = mini_usock_bind(&self->usock, (struct sockaddr *)&local, locallen);
    if (mini_slow(rc != 0))
    {
        mini_backoff_start(&self->retry);
        self->state = MINI_CWS_STATE_WAITING;
        return;
    }

    /*  Start connecting. */
    mini_usock_connect(&self->usock, (struct sockaddr *)&remote, remotelen);
    self->state = MINI_CWS_STATE_CONNECTING;
    mini_ep_stat_increment(self->ep, MINI_STAT_INPROGRESS_CONNECTIONS, 1);
}
