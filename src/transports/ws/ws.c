/*
    Copyright (c) 2012-2013 250bpm s.r.o.  All rights reserved.
    Copyright (c) 2013 GoPivotal, Inc.  All rights reserved.
    Copyright (c) 2014 Wirebird Labs LLC.  All rights reserved.
    Copyright 2016 Garrett D'Amore <garrett@damore.org>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "bws.h"
#include "cws.h"
#include "sws.h"

#include "../../ws.h"

#include "../utils/port.h"
#include "../utils/iface.h"

#include "../../utils/err.h"
#include "../../utils/alloc.h"
#include "../../utils/fast.h"
#include "../../utils/cont.h"

#include <string.h>

#if defined MINI_HAVE_WINDOWS
#include "../../utils/win.h"
#else
#include <unistd.h>
#endif

/*  WebSocket-specific socket options. */
struct mini_ws_optset
{
    struct mini_optset base;
    int msg_type;
};

static void mini_ws_optset_destroy(struct mini_optset *self);
static int mini_ws_optset_setopt(struct mini_optset *self, int option,
                                 const void *optval, size_t optvallen);
static int mini_ws_optset_getopt(struct mini_optset *self, int option,
                                 void *optval, size_t *optvallen);
static const struct mini_optset_vfptr mini_ws_optset_vfptr = {
    mini_ws_optset_destroy,
    mini_ws_optset_setopt,
    mini_ws_optset_getopt};

/*  mini_transport interface. */
static int mini_ws_bind(struct mini_ep *);
static int mini_ws_connect(struct mini_ep *);
static struct mini_optset *mini_ws_optset(void);

struct mini_transport mini_ws = {
    "ws",
    MINI_WS,
    NULL,
    NULL,
    mini_ws_bind,
    mini_ws_connect,
    mini_ws_optset,
};

static int mini_ws_bind(struct mini_ep *ep)
{
    return mini_bws_create(ep);
}

static int mini_ws_connect(struct mini_ep *ep)
{
    return mini_cws_create(ep);
}

static struct mini_optset *mini_ws_optset()
{
    struct mini_ws_optset *optset;

    optset = mini_alloc(sizeof(struct mini_ws_optset), "optset (ws)");
    alloc_assert(optset);
    optset->base.vfptr = &mini_ws_optset_vfptr;

    /*  Default values for WebSocket options. */
    optset->msg_type = MINI_WS_MSG_TYPE_BINARY;

    return &optset->base;
}

static void mini_ws_optset_destroy(struct mini_optset *self)
{
    struct mini_ws_optset *optset;

    optset = mini_cont(self, struct mini_ws_optset, base);
    mini_free(optset);
}

static int mini_ws_optset_setopt(struct mini_optset *self, int option,
                                 const void *optval, size_t optvallen)
{
    struct mini_ws_optset *optset;
    int val;

    optset = mini_cont(self, struct mini_ws_optset, base);
    if (optvallen != sizeof(int))
    {
        return -EINVAL;
    }
    val = *(int *)optval;

    switch (option)
    {
    case MINI_WS_MSG_TYPE:
        switch (val)
        {
        case MINI_WS_MSG_TYPE_TEXT:
        case MINI_WS_MSG_TYPE_BINARY:
            optset->msg_type = val;
            return 0;
        default:
            return -EINVAL;
        }
    default:
        return -ENOPROTOOPT;
    }
}

static int mini_ws_optset_getopt(struct mini_optset *self, int option,
                                 void *optval, size_t *optvallen)
{
    struct mini_ws_optset *optset;

    optset = mini_cont(self, struct mini_ws_optset, base);

    switch (option)
    {
    case MINI_WS_MSG_TYPE:
        memcpy(optval, &optset->msg_type,
               *optvallen < sizeof(int) ? *optvallen : sizeof(int));
        *optvallen = sizeof(int);
        return 0;
    default:
        return -ENOPROTOOPT;
    }
}
