/*
    Copyright (c) 2012-2013 250bpm s.r.o.  All rights reserved.
    Copyright (c) 2014-2016 Jack R. Dunaway. All rights reserved.
    Copyright 2016 Garrett D'Amore <garrett@damore.org>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "bws.h"
#include "aws.h"

#include "../utils/port.h"
#include "../utils/iface.h"

#include "../../io/fsm.h"
#include "../../io/usock.h"

#include "../../utils/err.h"
#include "../../utils/cont.h"
#include "../../utils/alloc.h"
#include "../../utils/list.h"
#include "../../utils/fast.h"

#include <string.h>

#if defined MINI_HAVE_WINDOWS
#include "../../utils/win.h"
#else
#include <unistd.h>
#include <netinet/in.h>
#endif

/*  The backlog is set relatively high so that there are not too many failed
    connection attempts during re-connection storms. */
#define MINI_BWS_BACKLOG 100

#define MINI_BWS_STATE_IDLE 1
#define MINI_BWS_STATE_ACTIVE 2
#define MINI_BWS_STATE_STOPPING_AWS 3
#define MINI_BWS_STATE_STOPPING_USOCK 4
#define MINI_BWS_STATE_STOPPING_AWSS 5

#define MINI_BWS_SRC_USOCK 1
#define MINI_BWS_SRC_AWS 2

struct mini_bws
{

    /*  The state machine. */
    struct mini_fsm fsm;
    int state;

    struct mini_ep *ep;

    /*  The underlying listening WS socket. */
    struct mini_usock usock;

    /*  The connection being accepted at the moment. */
    struct mini_aws *aws;

    /*  List of accepted connections. */
    struct mini_list awss;
};

/*  mini_ep virtual interface implementation. */
static void mini_bws_stop(void *);
static void mini_bws_destroy(void *);
const struct mini_ep_ops mini_bws_ep_ops = {
    mini_bws_stop,
    mini_bws_destroy};

/*  Private functions. */
static void mini_bws_handler(struct mini_fsm *self, int src, int type,
                             void *srcptr);
static void mini_bws_shutdown(struct mini_fsm *self, int src, int type,
                              void *srcptr);
static int mini_bws_listen(struct mini_bws *self);
static void mini_bws_start_accepting(struct mini_bws *self);

int mini_bws_create(struct mini_ep *ep)
{
    int rc;
    struct mini_bws *self;
    const char *addr;
    const char *end;
    const char *pos;
    struct sockaddr_storage ss;
    size_t sslen;
    int ipv4only;
    size_t ipv4onlylen;

    /*  Allocate the new endpoint object. */
    self = mini_alloc(sizeof(struct mini_bws), "bws");
    alloc_assert(self);
    self->ep = ep;

    mini_ep_tran_setup(ep, &mini_bws_ep_ops, self);
    addr = mini_ep_getaddr(ep);

    /*  Parse the port. */
    end = addr + strlen(addr);
    pos = strrchr(addr, ':');
    if (!pos)
    {
        return -EINVAL;
    }
    ++pos;
    rc = mini_port_resolve(pos, end - pos);
    if (rc < 0)
    {
        return -EINVAL;
    }

    /*  Check whether IPv6 is to be used. */
    ipv4onlylen = sizeof(ipv4only);
    mini_ep_getopt(ep, MINI_SOL_SOCKET, MINI_IPV4ONLY, &ipv4only, &ipv4onlylen);
    mini_assert(ipv4onlylen == sizeof(ipv4only));

    /*  Parse the address. */
    rc = mini_iface_resolve(addr, pos - addr - 1, ipv4only, &ss, &sslen);
    if (rc < 0)
    {
        return -ENODEV;
    }

    /*  Initialise the structure. */
    mini_fsm_init_root(&self->fsm, mini_bws_handler, mini_bws_shutdown,
                       mini_ep_getctx(ep));
    self->state = MINI_BWS_STATE_IDLE;
    self->aws = NULL;
    mini_list_init(&self->awss);

    /*  Start the state machine. */
    mini_fsm_start(&self->fsm);

    mini_usock_init(&self->usock, MINI_BWS_SRC_USOCK, &self->fsm);

    rc = mini_bws_listen(self);
    if (rc != 0)
    {
        return rc;
    }

    return 0;
}

static void mini_bws_stop(void *self)
{
    struct mini_bws *bws = self;

    mini_fsm_stop(&bws->fsm);
}

static void mini_bws_destroy(void *self)
{
    struct mini_bws *bws = self;

    mini_assert_state(bws, MINI_BWS_STATE_IDLE);
    mini_list_term(&bws->awss);
    mini_assert(bws->aws == NULL);
    mini_usock_term(&bws->usock);
    mini_fsm_term(&bws->fsm);

    mini_free(bws);
}

static void mini_bws_shutdown(struct mini_fsm *self, int src, int type,
                              void *srcptr)
{
    struct mini_bws *bws;
    struct mini_list_item *it;
    struct mini_aws *aws;

    bws = mini_cont(self, struct mini_bws, fsm);

    if (src == MINI_FSM_ACTION && type == MINI_FSM_STOP)
    {
        if (bws->aws)
        {
            mini_aws_stop(bws->aws);
            bws->state = MINI_BWS_STATE_STOPPING_AWS;
        }
        else
        {
            bws->state = MINI_BWS_STATE_STOPPING_USOCK;
        }
    }
    if (bws->state == MINI_BWS_STATE_STOPPING_AWS)
    {
        if (!mini_aws_isidle(bws->aws))
            return;
        mini_aws_term(bws->aws);
        mini_free(bws->aws);
        bws->aws = NULL;
        mini_usock_stop(&bws->usock);
        bws->state = MINI_BWS_STATE_STOPPING_USOCK;
    }
    if (bws->state == MINI_BWS_STATE_STOPPING_USOCK)
    {
        if (!mini_usock_isidle(&bws->usock))
            return;
        for (it = mini_list_begin(&bws->awss);
             it != mini_list_end(&bws->awss);
             it = mini_list_next(&bws->awss, it))
        {
            aws = mini_cont(it, struct mini_aws, item);
            mini_aws_stop(aws);
        }
        bws->state = MINI_BWS_STATE_STOPPING_AWSS;
        goto awss_stopping;
    }
    if (bws->state == MINI_BWS_STATE_STOPPING_AWSS)
    {
        mini_assert(src == MINI_BWS_SRC_AWS && type == MINI_AWS_STOPPED);
        aws = (struct mini_aws *)srcptr;
        mini_list_erase(&bws->awss, &aws->item);
        mini_aws_term(aws);
        mini_free(aws);

        /*  If there are no more aws state machines, we can stop the whole
            bws object. */
    awss_stopping:
        if (mini_list_empty(&bws->awss))
        {
            bws->state = MINI_BWS_STATE_IDLE;
            mini_fsm_stopped_noevent(&bws->fsm);
            mini_ep_stopped(bws->ep);
            return;
        }

        return;
    }

    mini_fsm_bad_action(bws->state, src, type);
}

static void mini_bws_handler(struct mini_fsm *self, int src, int type,
                             void *srcptr)
{
    struct mini_bws *bws;
    struct mini_aws *aws;

    bws = mini_cont(self, struct mini_bws, fsm);

    switch (bws->state)
    {

        /******************************************************************************/
        /*  IDLE state.                                                               */
        /******************************************************************************/
    case MINI_BWS_STATE_IDLE:
        mini_assert(src == MINI_FSM_ACTION);
        mini_assert(type == MINI_FSM_START);
        bws->state = MINI_BWS_STATE_ACTIVE;
        return;

        /******************************************************************************/
        /*  ACTIVE state.                                                             */
        /*  The execution is yielded to the aws state machine in this state.          */
        /******************************************************************************/
    case MINI_BWS_STATE_ACTIVE:
        if (src == MINI_BWS_SRC_USOCK)
        {
            mini_assert(type == MINI_USOCK_SHUTDOWN || type == MINI_USOCK_STOPPED);
            return;
        }

        /*  For all remaining events we'll assume they are coming from one
            of remaining child aws objects. */
        mini_assert(src == MINI_BWS_SRC_AWS);
        aws = (struct mini_aws *)srcptr;
        switch (type)
        {
        case MINI_AWS_ACCEPTED:

            /*  Move the newly created connection to the list of existing
                connections. */
            mini_list_insert(&bws->awss, &bws->aws->item,
                             mini_list_end(&bws->awss));
            bws->aws = NULL;

            /*  Start waiting for a new incoming connection. */
            mini_bws_start_accepting(bws);
            return;

        case MINI_AWS_ERROR:
            mini_aws_stop(aws);
            return;
        case MINI_AWS_STOPPED:
            mini_list_erase(&bws->awss, &aws->item);
            mini_aws_term(aws);
            mini_free(aws);
            return;
        default:
            mini_fsm_bad_action(bws->state, src, type);
        }

        /******************************************************************************/
        /*  Invalid state.                                                            */
        /******************************************************************************/
    default:
        mini_fsm_bad_state(bws->state, src, type);
    }
}

static int mini_bws_listen(struct mini_bws *self)
{
    int rc;
    struct sockaddr_storage ss;
    size_t sslen;
    int ipv4only;
    size_t ipv4onlylen;
    const char *addr;
    const char *end;
    const char *pos;
    uint16_t port;

    /*  First, resolve the IP address. */
    addr = mini_ep_getaddr(self->ep);
    memset(&ss, 0, sizeof(ss));

    /*  Parse the port. */
    end = addr + strlen(addr);
    pos = strrchr(addr, ':');
    mini_assert(pos);
    ++pos;
    rc = mini_port_resolve(pos, end - pos);
    if (rc < 0)
    {
        return rc;
    }
    port = (uint16_t)rc;

    /*  Parse the address. */
    ipv4onlylen = sizeof(ipv4only);
    mini_ep_getopt(self->ep, MINI_SOL_SOCKET, MINI_IPV4ONLY,
                   &ipv4only, &ipv4onlylen);
    mini_assert(ipv4onlylen == sizeof(ipv4only));
    rc = mini_iface_resolve(addr, pos - addr - 1, ipv4only, &ss, &sslen);
    if (rc < 0)
    {
        return rc;
    }

    /*  Combine the port and the address. */
    if (ss.ss_family == AF_INET)
    {
        ((struct sockaddr_in *)&ss)->sin_port = htons(port);
        sslen = sizeof(struct sockaddr_in);
    }
    else if (ss.ss_family == AF_INET6)
    {
        ((struct sockaddr_in6 *)&ss)->sin6_port = htons(port);
        sslen = sizeof(struct sockaddr_in6);
    }
    else
        mini_assert(0);

    /*  Start listening for incoming connections. */
    rc = mini_usock_start(&self->usock, ss.ss_family, SOCK_STREAM, 0);
    if (rc < 0)
    {
        return rc;
    }

    rc = mini_usock_bind(&self->usock, (struct sockaddr *)&ss, (size_t)sslen);
    if (rc < 0)
    {
        mini_usock_stop(&self->usock);
        return rc;
    }

    rc = mini_usock_listen(&self->usock, MINI_BWS_BACKLOG);
    if (rc < 0)
    {
        mini_usock_stop(&self->usock);
        return rc;
    }
    mini_bws_start_accepting(self);

    return 0;
}

/******************************************************************************/
/*  State machine actions.                                                    */
/******************************************************************************/

static void mini_bws_start_accepting(struct mini_bws *self)
{
    mini_assert(self->aws == NULL);

    /*  Allocate new aws state machine. */
    self->aws = mini_alloc(sizeof(struct mini_aws), "aws");
    alloc_assert(self->aws);
    mini_aws_init(self->aws, MINI_BWS_SRC_AWS, self->ep, &self->fsm);

    /*  Start waiting for a new incoming connection. */
    mini_aws_start(self->aws, &self->usock);
}
