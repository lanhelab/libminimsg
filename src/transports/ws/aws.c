/*
    Copyright (c) 2012-2013 250bpm s.r.o.  All rights reserved.
    Copyright (c) 2014-2016 Jack R. Dunaway. All rights reserved.
    Copyright 2016 Garrett D'Amore <garrett@damore.org>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "aws.h"

#include "../../utils/err.h"
#include "../../utils/cont.h"
#include "../../utils/attr.h"
#include "../../ws.h"

#define MINI_AWS_STATE_IDLE 1
#define MINI_AWS_STATE_ACCEPTING 2
#define MINI_AWS_STATE_ACTIVE 3
#define MINI_AWS_STATE_STOPPING_SWS 4
#define MINI_AWS_STATE_STOPPING_USOCK 5
#define MINI_AWS_STATE_DONE 6
#define MINI_AWS_STATE_STOPPING_SWS_FINAL 7
#define MINI_AWS_STATE_STOPPING 8

#define MINI_AWS_SRC_USOCK 1
#define MINI_AWS_SRC_SWS 2
#define MINI_AWS_SRC_LISTENER 3

/*  Private functions. */
static void mini_aws_handler(struct mini_fsm *self, int src, int type,
                             void *srcptr);
static void mini_aws_shutdown(struct mini_fsm *self, int src, int type,
                              void *srcptr);

void mini_aws_init(struct mini_aws *self, int src,
                   struct mini_ep *ep, struct mini_fsm *owner)
{
    mini_fsm_init(&self->fsm, mini_aws_handler, mini_aws_shutdown,
                  src, self, owner);
    self->state = MINI_AWS_STATE_IDLE;
    self->ep = ep;
    mini_usock_init(&self->usock, MINI_AWS_SRC_USOCK, &self->fsm);
    self->listener = NULL;
    self->listener_owner.src = -1;
    self->listener_owner.fsm = NULL;
    mini_sws_init(&self->sws, MINI_AWS_SRC_SWS, ep, &self->fsm);
    mini_fsm_event_init(&self->accepted);
    mini_fsm_event_init(&self->done);
    mini_list_item_init(&self->item);
}

void mini_aws_term(struct mini_aws *self)
{
    mini_assert_state(self, MINI_AWS_STATE_IDLE);

    mini_list_item_term(&self->item);
    mini_fsm_event_term(&self->done);
    mini_fsm_event_term(&self->accepted);
    mini_sws_term(&self->sws);
    mini_usock_term(&self->usock);
    mini_fsm_term(&self->fsm);
}

int mini_aws_isidle(struct mini_aws *self)
{
    return mini_fsm_isidle(&self->fsm);
}

void mini_aws_start(struct mini_aws *self, struct mini_usock *listener)
{
    mini_assert_state(self, MINI_AWS_STATE_IDLE);

    /*  Take ownership of the listener socket. */
    self->listener = listener;
    self->listener_owner.src = MINI_AWS_SRC_LISTENER;
    self->listener_owner.fsm = &self->fsm;
    mini_usock_swap_owner(listener, &self->listener_owner);

    /*  Start the state machine. */
    mini_fsm_start(&self->fsm);
}

void mini_aws_stop(struct mini_aws *self)
{
    mini_fsm_stop(&self->fsm);
}

static void mini_aws_shutdown(struct mini_fsm *self, int src, int type,
                              MINI_UNUSED void *srcptr)
{
    struct mini_aws *aws;

    aws = mini_cont(self, struct mini_aws, fsm);

    if (mini_slow(src == MINI_FSM_ACTION && type == MINI_FSM_STOP))
    {
        if (!mini_sws_isidle(&aws->sws))
        {
            mini_ep_stat_increment(aws->ep, MINI_STAT_DROPPED_CONNECTIONS, 1);
            mini_sws_stop(&aws->sws);
        }
        aws->state = MINI_AWS_STATE_STOPPING_SWS_FINAL;
    }
    if (mini_slow(aws->state == MINI_AWS_STATE_STOPPING_SWS_FINAL))
    {
        if (!mini_sws_isidle(&aws->sws))
            return;
        mini_usock_stop(&aws->usock);
        aws->state = MINI_AWS_STATE_STOPPING;
    }
    if (mini_slow(aws->state == MINI_AWS_STATE_STOPPING))
    {
        if (!mini_usock_isidle(&aws->usock))
            return;
        if (aws->listener)
        {
            mini_assert(aws->listener_owner.fsm);
            mini_usock_swap_owner(aws->listener, &aws->listener_owner);
            aws->listener = NULL;
            aws->listener_owner.src = -1;
            aws->listener_owner.fsm = NULL;
        }
        aws->state = MINI_AWS_STATE_IDLE;
        mini_fsm_stopped(&aws->fsm, MINI_AWS_STOPPED);
        return;
    }

    mini_fsm_bad_action(aws->state, src, type);
}

static void mini_aws_handler(struct mini_fsm *self, int src, int type,
                             MINI_UNUSED void *srcptr)
{
    struct mini_aws *aws;
    int val;
    size_t sz;
    uint8_t msg_type;

    aws = mini_cont(self, struct mini_aws, fsm);

    switch (aws->state)
    {

        /******************************************************************************/
        /*  IDLE state.                                                               */
        /*  The state machine wasn't yet started.                                     */
        /******************************************************************************/
    case MINI_AWS_STATE_IDLE:
        switch (src)
        {

        case MINI_FSM_ACTION:
            switch (type)
            {
            case MINI_FSM_START:
                mini_usock_accept(&aws->usock, aws->listener);
                aws->state = MINI_AWS_STATE_ACCEPTING;
                return;
            default:
                mini_fsm_bad_action(aws->state, src, type);
            }

        default:
            mini_fsm_bad_source(aws->state, src, type);
        }

        /******************************************************************************/
        /*  ACCEPTING state.                                                          */
        /*  Waiting for incoming connection.                                          */
        /******************************************************************************/
    case MINI_AWS_STATE_ACCEPTING:
        switch (src)
        {

        case MINI_AWS_SRC_USOCK:
            switch (type)
            {
            case MINI_USOCK_ACCEPTED:
                mini_ep_clear_error(aws->ep);

                /*  Set the relevant socket options. */
                sz = sizeof(val);
                mini_ep_getopt(aws->ep, MINI_SOL_SOCKET, MINI_SNDBUF, &val, &sz);
                mini_assert(sz == sizeof(val));
                mini_usock_setsockopt(&aws->usock, SOL_SOCKET, SO_SNDBUF,
                                      &val, sizeof(val));
                sz = sizeof(val);
                mini_ep_getopt(aws->ep, MINI_SOL_SOCKET, MINI_RCVBUF, &val, &sz);
                mini_assert(sz == sizeof(val));
                mini_usock_setsockopt(&aws->usock, SOL_SOCKET, SO_RCVBUF,
                                      &val, sizeof(val));
                sz = sizeof(val);
                mini_ep_getopt(aws->ep, MINI_WS, MINI_WS_MSG_TYPE, &val, &sz);
                msg_type = (uint8_t)val;

                /*   Since the WebSocket handshake must poll, the receive
                     timeout is set to zero. Later, it will be set again
                     to the value specified by the socket option. */
                val = 0;
                sz = sizeof(val);
                mini_usock_setsockopt(&aws->usock, SOL_SOCKET, SO_RCVTIMEO,
                                      &val, sizeof(val));

                /*  Return ownership of the listening socket to the parent. */
                mini_usock_swap_owner(aws->listener, &aws->listener_owner);
                aws->listener = NULL;
                aws->listener_owner.src = -1;
                aws->listener_owner.fsm = NULL;
                mini_fsm_raise(&aws->fsm, &aws->accepted, MINI_AWS_ACCEPTED);

                /*  Start the sws state machine. */
                mini_usock_activate(&aws->usock);
                mini_sws_start(&aws->sws, &aws->usock, MINI_WS_SERVER,
                               NULL, NULL, msg_type);
                aws->state = MINI_AWS_STATE_ACTIVE;

                mini_ep_stat_increment(aws->ep, MINI_STAT_ACCEPTED_CONNECTIONS, 1);

                return;

            default:
                mini_fsm_bad_action(aws->state, src, type);
            }

        case MINI_AWS_SRC_LISTENER:
            switch (type)
            {

            case MINI_USOCK_ACCEPT_ERROR:
                mini_ep_set_error(aws->ep, mini_usock_geterrno(aws->listener));
                mini_ep_stat_increment(aws->ep, MINI_STAT_ACCEPT_ERRORS, 1);
                mini_usock_accept(&aws->usock, aws->listener);
                return;

            default:
                mini_fsm_bad_action(aws->state, src, type);
            }

        default:
            mini_fsm_bad_source(aws->state, src, type);
        }

        /******************************************************************************/
        /*  ACTIVE state.                                                             */
        /******************************************************************************/
    case MINI_AWS_STATE_ACTIVE:
        switch (src)
        {

        case MINI_AWS_SRC_SWS:
            switch (type)
            {
            case MINI_SWS_RETURN_CLOSE_HANDSHAKE:
                /*  Peer closed connection without intention to reconnect, or
                    local endpoint failed remote because of invalid data. */
                mini_sws_stop(&aws->sws);
                aws->state = MINI_AWS_STATE_STOPPING_SWS;
                return;
            case MINI_SWS_RETURN_ERROR:
                mini_sws_stop(&aws->sws);
                aws->state = MINI_AWS_STATE_STOPPING_SWS;
                mini_ep_stat_increment(aws->ep, MINI_STAT_BROKEN_CONNECTIONS, 1);
                return;
            default:
                mini_fsm_bad_action(aws->state, src, type);
            }

        default:
            mini_fsm_bad_source(aws->state, src, type);
        }

        /******************************************************************************/
        /*  STOPPING_SWS state.                                                       */
        /******************************************************************************/
    case MINI_AWS_STATE_STOPPING_SWS:
        switch (src)
        {

        case MINI_AWS_SRC_SWS:
            switch (type)
            {
            case MINI_USOCK_SHUTDOWN:
                return;
            case MINI_SWS_RETURN_STOPPED:
                mini_usock_stop(&aws->usock);
                aws->state = MINI_AWS_STATE_STOPPING_USOCK;
                return;
            default:
                mini_fsm_bad_action(aws->state, src, type);
            }

        default:
            mini_fsm_bad_source(aws->state, src, type);
        }

        /******************************************************************************/
        /*  STOPPING_USOCK state.                                                     */
        /******************************************************************************/
    case MINI_AWS_STATE_STOPPING_USOCK:
        switch (src)
        {

        case MINI_AWS_SRC_USOCK:
            switch (type)
            {
            case MINI_USOCK_SHUTDOWN:
                return;
            case MINI_USOCK_STOPPED:
                mini_fsm_raise(&aws->fsm, &aws->done, MINI_AWS_ERROR);
                aws->state = MINI_AWS_STATE_DONE;
                return;
            default:
                mini_fsm_bad_action(aws->state, src, type);
            }

        default:
            mini_fsm_bad_source(aws->state, src, type);
        }

        /******************************************************************************/
        /*  Invalid state.                                                            */
        /******************************************************************************/
    default:
        mini_fsm_bad_state(aws->state, src, type);
    }
}
