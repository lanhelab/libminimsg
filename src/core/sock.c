/*
    Copyright (c) 2012-2014 Martin Sustrik  All rights reserved.
    Copyright (c) 2013 GoPivotal, Inc.  All rights reserved.
    Copyright 2017 Garrett D'Amore <garrett@damore.org>
    Copyright 2017 Capitar IT Group BV <info@capitar.com>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "../protocol.h"
#include "../transport.h"

#include "sock.h"
#include "global.h"
#include "ep.h"

#include "../utils/err.h"
#include "../utils/cont.h"
#include "../utils/clock.h"
#include "../utils/fast.h"
#include "../utils/alloc.h"
#include "../utils/msg.h"

#include <limits.h>

/*  These bits specify whether individual efds are signalled or not at
    the moment. Storing this information allows us to avoid redundant signalling
    and unsignalling of the efd objects. */
#define MINI_SOCK_FLAG_IN 1
#define MINI_SOCK_FLAG_OUT 2

/*  Possible states of the socket. */
#define MINI_SOCK_STATE_INIT 1
#define MINI_SOCK_STATE_ACTIVE 2
#define MINI_SOCK_STATE_STOPPING_EPS 3
#define MINI_SOCK_STATE_STOPPING 4
#define MINI_SOCK_STATE_FINI 5

/*  Events sent to the state machine. */
#define MINI_SOCK_ACTION_STOPPED 1

/*  Subordinated source objects. */
#define MINI_SOCK_SRC_EP 1

/*  Private functions. */
static struct mini_optset *mini_sock_optset(struct mini_sock *self, int id);
static int mini_sock_setopt_inner(struct mini_sock *self, int level,
                                  int option, const void *optval, size_t optvallen);
static void mini_sock_onleave(struct mini_ctx *self);
static void mini_sock_handler(struct mini_fsm *self, int src, int type,
                              void *srcptr);
static void mini_sock_shutdown(struct mini_fsm *self, int src, int type,
                               void *srcptr);

/*  Initialize a socket.  A hold is placed on the initialized socket for
    the caller as well. */
int mini_sock_init(struct mini_sock *self, const struct mini_socktype *socktype,
                   int fd)
{
    int rc;
    int i;

    /* Make sure that at least one message direction is supported. */
    mini_assert(!(socktype->flags & MINI_SOCKTYPE_FLAG_NOSEND) ||
                !(socktype->flags & MINI_SOCKTYPE_FLAG_NORECV));

    /*  Create the AIO context for the SP socket. */
    mini_ctx_init(&self->ctx, mini_global_getpool(), mini_sock_onleave);

    /*  Initialise the state machine. */
    mini_fsm_init_root(&self->fsm, mini_sock_handler,
                       mini_sock_shutdown, &self->ctx);
    self->state = MINI_SOCK_STATE_INIT;

    /*  Open the MINI_SNDFD and MINI_RCVFD efds. Do so, only if the socket type
        supports send/recv, as appropriate. */
    if (socktype->flags & MINI_SOCKTYPE_FLAG_NOSEND)
        memset(&self->sndfd, 0xcd, sizeof(self->sndfd));
    else
    {
        rc = mini_efd_init(&self->sndfd);
        if (mini_slow(rc < 0))
            return rc;
    }
    if (socktype->flags & MINI_SOCKTYPE_FLAG_NORECV)
        memset(&self->rcvfd, 0xcd, sizeof(self->rcvfd));
    else
    {
        rc = mini_efd_init(&self->rcvfd);
        if (mini_slow(rc < 0))
        {
            if (!(socktype->flags & MINI_SOCKTYPE_FLAG_NOSEND))
                mini_efd_term(&self->sndfd);
            return rc;
        }
    }
    mini_sem_init(&self->termsem);
    mini_sem_init(&self->relesem);
    if (mini_slow(rc < 0))
    {
        if (!(socktype->flags & MINI_SOCKTYPE_FLAG_NORECV))
            mini_efd_term(&self->rcvfd);
        if (!(socktype->flags & MINI_SOCKTYPE_FLAG_NOSEND))
            mini_efd_term(&self->sndfd);
        return rc;
    }

    self->holds = 1; /*  Callers hold. */
    self->flags = 0;
    mini_list_init(&self->eps);
    mini_list_init(&self->sdeps);
    self->eid = 1;

    /*  Default values for MINI_SOL_SOCKET options. */
    self->sndbuf = 128 * 1024;
    self->rcvbuf = 128 * 1024;
    self->rcvmaxsize = 1024 * 1024;
    self->sndtimeo = -1;
    self->rcvtimeo = -1;
    self->reconnect_ivl = 100;
    self->reconnect_ivl_max = 0;
    self->maxttl = 8;
    self->ep_template.sndprio = 8;
    self->ep_template.rcvprio = 8;
    self->ep_template.ipv4only = 1;

    /* Clear statistic entries */
    memset(&self->statistics, 0, sizeof(self->statistics));

    /*  Should be pretty much enough space for just the number  */
    sprintf(self->socket_name, "%d", fd);

    /* Security attribute */
    self->sec_attr = NULL;
    self->sec_attr_size = 0;
    self->inbuffersz = 4096;
    self->outbuffersz = 4096;

    /*  The transport-specific options are not initialised immediately,
        rather, they are allocated later on when needed. */
    for (i = 0; i != MINI_MAX_TRANSPORT; ++i)
        self->optsets[i] = NULL;

    /*  Create the specific socket type itself. */
    rc = socktype->create((void *)self, &self->sockbase);
    errnum_assert(rc == 0, -rc);
    self->socktype = socktype;

    /*  Launch the state machine. */
    mini_ctx_enter(&self->ctx);
    mini_fsm_start(&self->fsm);
    mini_ctx_leave(&self->ctx);

    return 0;
}

void mini_sock_stopped(struct mini_sock *self)
{
    /*  TODO: Do the following in a more sane way. */
    self->fsm.stopped.fsm = &self->fsm;
    self->fsm.stopped.src = MINI_FSM_ACTION;
    self->fsm.stopped.srcptr = NULL;
    self->fsm.stopped.type = MINI_SOCK_ACTION_STOPPED;
    mini_ctx_raise(self->fsm.ctx, &self->fsm.stopped);
}

/*  Stop the socket.  This will prevent new calls from aquiring a
    hold on the socket, cause endpoints to shut down, and wake any
    threads waiting to recv or send data. */
void mini_sock_stop(struct mini_sock *self)
{
    mini_ctx_enter(&self->ctx);
    mini_fsm_stop(&self->fsm);
    mini_ctx_leave(&self->ctx);
}

int mini_sock_term(struct mini_sock *self)
{
    int rc;
    int i;

    /*  NOTE: mini_sock_stop must have already been called. */

    /*  Some endpoints may still be alive.  Here we are going to wait
        till they are all closed.  This loop is not interruptible, because
        making it so would leave a partially cleaned up socket, and we don't
        have a way to defer resource deallocation. */
    for (;;)
    {
        rc = mini_sem_wait(&self->termsem);
        if (mini_slow(rc == -EINTR))
            continue;
        errnum_assert(rc == 0, -rc);
        break;
    }

    /*  Also, wait for all holds on the socket to be released.  */
    for (;;)
    {
        rc = mini_sem_wait(&self->relesem);
        if (mini_slow(rc == -EINTR))
            continue;
        errnum_assert(rc == 0, -rc);
        break;
    }

    /*  Threads that posted the semaphore(s) can still have the ctx locked
        for a short while. By simply entering the context and exiting it
        immediately we can be sure that any such threads have already
        exited the context. */
    mini_ctx_enter(&self->ctx);
    mini_ctx_leave(&self->ctx);

    /*  At this point, we can be reasonably certain that no other thread
        has any references to the socket. */

    /*  Close the event FDs entirely. */
    if (!(self->socktype->flags & MINI_SOCKTYPE_FLAG_NORECV))
    {
        mini_efd_term(&self->rcvfd);
    }
    if (!(self->socktype->flags & MINI_SOCKTYPE_FLAG_NOSEND))
    {
        mini_efd_term(&self->sndfd);
    }

    mini_fsm_stopped_noevent(&self->fsm);
    mini_fsm_term(&self->fsm);
    mini_sem_term(&self->termsem);
    mini_sem_term(&self->relesem);
    mini_list_term(&self->sdeps);
    mini_list_term(&self->eps);
    mini_ctx_term(&self->ctx);

    /*  Destroy any optsets associated with the socket. */
    for (i = 0; i != MINI_MAX_TRANSPORT; ++i)
        if (self->optsets[i])
            self->optsets[i]->vfptr->destroy(self->optsets[i]);

    return 0;
}

struct mini_ctx *mini_sock_getctx(struct mini_sock *self)
{
    return &self->ctx;
}

int mini_sock_ispeer(struct mini_sock *self, int socktype)
{
    /*  If the peer implements a different SP protocol it is not a valid peer.
        Checking it here ensures that even if faulty protocol implementation
        allows for cross-protocol communication, it will never happen
        in practice. */
    if ((self->socktype->protocol & 0xfff0) != (socktype & 0xfff0))
        return 0;

    /*  As long as the peer speaks the same protocol, socket type itself
        decides which socket types are to be accepted. */
    return self->socktype->ispeer(socktype);
}

int mini_sock_setopt(struct mini_sock *self, int level, int option,
                     const void *optval, size_t optvallen)
{
    int rc;

    mini_ctx_enter(&self->ctx);
    rc = mini_sock_setopt_inner(self, level, option, optval, optvallen);
    mini_ctx_leave(&self->ctx);

    return rc;
}

static int mini_sock_setopt_inner(struct mini_sock *self, int level,
                                  int option, const void *optval, size_t optvallen)
{
    struct mini_optset *optset;
    int val;

    /*  Protocol-specific socket options. */
    if (level > MINI_SOL_SOCKET)
    {
        if (self->sockbase->vfptr->setopt == NULL)
        {
            return -ENOPROTOOPT;
        }
        return self->sockbase->vfptr->setopt(self->sockbase, level, option,
                                             optval, optvallen);
    }

    /*  Transport-specific options. */
    if (level < MINI_SOL_SOCKET)
    {
        optset = mini_sock_optset(self, level);
        if (!optset)
            return -ENOPROTOOPT;
        return optset->vfptr->setopt(optset, option, optval, optvallen);
    }

    mini_assert(level == MINI_SOL_SOCKET);

    /*  Special-casing socket name for now as it's the only string option  */
    if (option == MINI_SOCKET_NAME)
    {
        if (optvallen > 63)
            return -EINVAL;
        memcpy(self->socket_name, optval, optvallen);
        self->socket_name[optvallen] = 0;
        return 0;
    }

    /*  At this point we assume that all options are of type int. */
    if (optvallen != sizeof(int))
        return -EINVAL;
    val = *(int *)optval;

    /*  Generic socket-level options. */
    switch (option)
    {
    case MINI_SNDBUF:
        if (val <= 0)
            return -EINVAL;
        self->sndbuf = val;
        return 0;
    case MINI_RCVBUF:
        if (val <= 0)
            return -EINVAL;
        self->rcvbuf = val;
        return 0;
    case MINI_RCVMAXSIZE:
        if (val < -1)
            return -EINVAL;
        self->rcvmaxsize = val;
        return 0;
    case MINI_SNDTIMEO:
        self->sndtimeo = val;
        return 0;
    case MINI_RCVTIMEO:
        self->rcvtimeo = val;
        return 0;
    case MINI_RECONNECT_IVL:
        if (val < 0)
            return -EINVAL;
        self->reconnect_ivl = val;
        return 0;
    case MINI_RECONNECT_IVL_MAX:
        if (val < 0)
            return -EINVAL;
        self->reconnect_ivl_max = val;
        return 0;
    case MINI_SNDPRIO:
        if (val < 1 || val > 16)
            return -EINVAL;
        self->ep_template.sndprio = val;
        return 0;
    case MINI_RCVPRIO:
        if (val < 1 || val > 16)
            return -EINVAL;
        self->ep_template.rcvprio = val;
        return 0;
    case MINI_IPV4ONLY:
        if (val != 0 && val != 1)
            return -EINVAL;
        self->ep_template.ipv4only = val;
        return 0;
    case MINI_MAXTTL:
        if (val < 1 || val > 255)
            return -EINVAL;
        self->maxttl = val;
        return 0;
    case MINI_LINGER:
        /*  Ignored, retained for compatibility. */
        return 0;
    }

    return -ENOPROTOOPT;
}

int mini_sock_getopt(struct mini_sock *self, int level, int option,
                     void *optval, size_t *optvallen)
{
    int rc;

    mini_ctx_enter(&self->ctx);
    rc = mini_sock_getopt_inner(self, level, option, optval, optvallen);
    mini_ctx_leave(&self->ctx);

    return rc;
}

int mini_sock_getopt_inner(struct mini_sock *self, int level,
                           int option, void *optval, size_t *optvallen)
{
    struct mini_optset *optset;
    int intval;
    mini_fd fd;

    /*  Protocol-specific socket options. */
    if (level > MINI_SOL_SOCKET)
    {
        if (self->sockbase->vfptr->getopt == NULL)
        {
            return -ENOPROTOOPT;
        }
        return self->sockbase->vfptr->getopt(self->sockbase,
                                             level, option, optval, optvallen);
    }

    /*  Transport-specific options. */
    if (level < MINI_SOL_SOCKET)
    {
        optset = mini_sock_optset(self, level);
        if (!optset)
            return -ENOPROTOOPT;
        return optset->vfptr->getopt(optset, option, optval, optvallen);
    }

    mini_assert(level == MINI_SOL_SOCKET);

    /*  Generic socket-level options. */
    switch (option)
    {
    case MINI_DOMAIN:
        intval = self->socktype->domain;
        break;
    case MINI_PROTOCOL:
        intval = self->socktype->protocol;
        break;
    case MINI_LINGER:
        intval = 0;
        break;
    case MINI_SNDBUF:
        intval = self->sndbuf;
        break;
    case MINI_RCVBUF:
        intval = self->rcvbuf;
        break;
    case MINI_RCVMAXSIZE:
        intval = self->rcvmaxsize;
        break;
    case MINI_SNDTIMEO:
        intval = self->sndtimeo;
        break;
    case MINI_RCVTIMEO:
        intval = self->rcvtimeo;
        break;
    case MINI_RECONNECT_IVL:
        intval = self->reconnect_ivl;
        break;
    case MINI_RECONNECT_IVL_MAX:
        intval = self->reconnect_ivl_max;
        break;
    case MINI_SNDPRIO:
        intval = self->ep_template.sndprio;
        break;
    case MINI_RCVPRIO:
        intval = self->ep_template.rcvprio;
        break;
    case MINI_IPV4ONLY:
        intval = self->ep_template.ipv4only;
        break;
    case MINI_MAXTTL:
        intval = self->maxttl;
        break;
    case MINI_SNDFD:
        if (self->socktype->flags & MINI_SOCKTYPE_FLAG_NOSEND)
            return -ENOPROTOOPT;
        fd = mini_efd_getfd(&self->sndfd);
        memcpy(optval, &fd,
               *optvallen < sizeof(mini_fd) ? *optvallen : sizeof(mini_fd));
        *optvallen = sizeof(mini_fd);
        return 0;
    case MINI_RCVFD:
        if (self->socktype->flags & MINI_SOCKTYPE_FLAG_NORECV)
            return -ENOPROTOOPT;
        fd = mini_efd_getfd(&self->rcvfd);
        memcpy(optval, &fd,
               *optvallen < sizeof(mini_fd) ? *optvallen : sizeof(mini_fd));
        *optvallen = sizeof(mini_fd);
        return 0;
    case MINI_SOCKET_NAME:
        strncpy(optval, self->socket_name, *optvallen);
        *optvallen = strlen(self->socket_name);
        return 0;
    default:
        return -ENOPROTOOPT;
    }

    memcpy(optval, &intval,
           *optvallen < sizeof(int) ? *optvallen : sizeof(int));
    *optvallen = sizeof(int);

    return 0;
}

int mini_sock_add_ep(struct mini_sock *self, const struct mini_transport *transport,
                     int bind, const char *addr)
{
    int rc;
    struct mini_ep *ep;
    int eid;

    mini_ctx_enter(&self->ctx);

    /*  Instantiate the endpoint. */
    ep = mini_alloc(sizeof(struct mini_ep), "endpoint");
    if (!ep)
    {
        return -ENOMEM;
    }
    rc = mini_ep_init(ep, MINI_SOCK_SRC_EP, self, self->eid, transport,
                      bind, addr);
    if (mini_slow(rc < 0))
    {
        mini_free(ep);
        mini_ctx_leave(&self->ctx);
        return rc;
    }
    mini_ep_start(ep);

    /*  Increase the endpoint ID for the next endpoint. */
    eid = self->eid;
    ++self->eid;

    /*  Add it to the list of active endpoints. */
    mini_list_insert(&self->eps, &ep->item, mini_list_end(&self->eps));

    mini_ctx_leave(&self->ctx);

    return eid;
}

int mini_sock_rm_ep(struct mini_sock *self, int eid)
{
    struct mini_list_item *it;
    struct mini_ep *ep;

    mini_ctx_enter(&self->ctx);

    /*  Find the specified enpoint. */
    ep = NULL;
    for (it = mini_list_begin(&self->eps);
         it != mini_list_end(&self->eps);
         it = mini_list_next(&self->eps, it))
    {
        ep = mini_cont(it, struct mini_ep, item);
        if (ep->eid == eid)
            break;
        ep = NULL;
    }

    /*  The endpoint doesn't exist. */
    if (!ep)
    {
        mini_ctx_leave(&self->ctx);
        return -EINVAL;
    }

    /*  Move the endpoint from the list of active endpoints to the list
        of shutting down endpoints. */
    mini_list_erase(&self->eps, &ep->item);
    mini_list_insert(&self->sdeps, &ep->item, mini_list_end(&self->sdeps));

    /*  Ask the endpoint to stop. Actual terminatation may be delayed
        by the transport. */
    mini_ep_stop(ep);

    mini_ctx_leave(&self->ctx);

    return 0;
}

int mini_sock_send(struct mini_sock *self, struct mini_msg *msg, int flags)
{
    int rc;
    uint64_t deadline;
    uint64_t now;
    int timeout;

    /*  Some sockets types cannot be used for sending messages. */
    if (mini_slow(self->socktype->flags & MINI_SOCKTYPE_FLAG_NOSEND))
        return -ENOTSUP;

    mini_ctx_enter(&self->ctx);

    /*  Compute the deadline for SNDTIMEO timer. */
    if (self->sndtimeo < 0)
    {
        deadline = -1;
        timeout = -1;
    }
    else
    {
        deadline = mini_clock_ms() + self->sndtimeo;
        timeout = self->sndtimeo;
    }

    while (1)
    {

        switch (self->state)
        {
        case MINI_SOCK_STATE_ACTIVE:
        case MINI_SOCK_STATE_INIT:
            break;

        case MINI_SOCK_STATE_STOPPING_EPS:
        case MINI_SOCK_STATE_STOPPING:
        case MINI_SOCK_STATE_FINI:
            /*  Socket closed or closing.  Should we return something
                else here; recvmsg(2) for example returns no data in
                this case, like read(2).  The use of indexed file
                descriptors is further problematic, as an FD can be reused
                leading to situations where technically the outstanding
                operation should refer to some other socket entirely.  */
            mini_ctx_leave(&self->ctx);
            return -EBADF;
        }

        /*  Try to send the message in a non-blocking way. */
        rc = self->sockbase->vfptr->send(self->sockbase, msg);
        if (mini_fast(rc == 0))
        {
            mini_ctx_leave(&self->ctx);
            return 0;
        }
        mini_assert(rc < 0);

        /*  Any unexpected error is forwarded to the caller. */
        if (mini_slow(rc != -EAGAIN))
        {
            mini_ctx_leave(&self->ctx);
            return rc;
        }

        /*  If the message cannot be sent at the moment and the send call
            is non-blocking, return immediately. */
        if (mini_fast(flags & MINI_DONTWAIT))
        {
            mini_ctx_leave(&self->ctx);
            return -EAGAIN;
        }

        /*  With blocking send, wait while there are new pipes available
            for sending. */
        mini_ctx_leave(&self->ctx);
        rc = mini_efd_wait(&self->sndfd, timeout);
        if (mini_slow(rc == -ETIMEDOUT))
            return -ETIMEDOUT;
        if (mini_slow(rc == -EINTR))
            return -EINTR;
        if (mini_slow(rc == -EBADF))
            return -EBADF;
        errnum_assert(rc == 0, rc);
        mini_ctx_enter(&self->ctx);
        /*
         *  Double check if pipes are still available for sending
         */
        if (!mini_efd_wait(&self->sndfd, 0))
        {
            self->flags |= MINI_SOCK_FLAG_OUT;
        }

        /*  If needed, re-compute the timeout to reflect the time that have
            already elapsed. */
        if (self->sndtimeo >= 0)
        {
            now = mini_clock_ms();
            timeout = (int)(now > deadline ? 0 : deadline - now);
        }
    }
}

int mini_sock_recv(struct mini_sock *self, struct mini_msg *msg, int flags)
{
    int rc;
    uint64_t deadline;
    uint64_t now;
    int timeout;

    /*  Some sockets types cannot be used for receiving messages. */
    if (mini_slow(self->socktype->flags & MINI_SOCKTYPE_FLAG_NORECV))
        return -ENOTSUP;

    mini_ctx_enter(&self->ctx);

    /*  Compute the deadline for RCVTIMEO timer. */
    if (self->rcvtimeo < 0)
    {
        deadline = -1;
        timeout = -1;
    }
    else
    {
        deadline = mini_clock_ms() + self->rcvtimeo;
        timeout = self->rcvtimeo;
    }

    while (1)
    {

        switch (self->state)
        {
        case MINI_SOCK_STATE_ACTIVE:
        case MINI_SOCK_STATE_INIT:
            break;

        case MINI_SOCK_STATE_STOPPING_EPS:
        case MINI_SOCK_STATE_STOPPING:
        case MINI_SOCK_STATE_FINI:
            /*  Socket closed or closing.  Should we return something
                else here; recvmsg(2) for example returns no data in
                this case, like read(2).  The use of indexed file
                descriptors is further problematic, as an FD can be reused
                leading to situations where technically the outstanding
                operation should refer to some other socket entirely.  */
            mini_ctx_leave(&self->ctx);
            return -EBADF;
        }

        /*  Try to receive the message in a non-blocking way. */
        rc = self->sockbase->vfptr->recv(self->sockbase, msg);
        if (mini_fast(rc == 0))
        {
            mini_ctx_leave(&self->ctx);
            return 0;
        }
        mini_assert(rc < 0);

        /*  Any unexpected error is forwarded to the caller. */
        if (mini_slow(rc != -EAGAIN))
        {
            mini_ctx_leave(&self->ctx);
            return rc;
        }

        /*  If the message cannot be received at the moment and the recv call
            is non-blocking, return immediately. */
        if (mini_fast(flags & MINI_DONTWAIT))
        {
            mini_ctx_leave(&self->ctx);
            return -EAGAIN;
        }

        /*  With blocking recv, wait while there are new pipes available
            for receiving. */
        mini_ctx_leave(&self->ctx);
        rc = mini_efd_wait(&self->rcvfd, timeout);
        if (mini_slow(rc == -ETIMEDOUT))
            return -ETIMEDOUT;
        if (mini_slow(rc == -EINTR))
            return -EINTR;
        if (mini_slow(rc == -EBADF))
            return -EBADF;
        errnum_assert(rc == 0, rc);
        mini_ctx_enter(&self->ctx);
        /*
         *  Double check if pipes are still available for receiving
         */
        if (!mini_efd_wait(&self->rcvfd, 0))
        {
            self->flags |= MINI_SOCK_FLAG_IN;
        }

        /*  If needed, re-compute the timeout to reflect the time that have
            already elapsed. */
        if (self->rcvtimeo >= 0)
        {
            now = mini_clock_ms();
            timeout = (int)(now > deadline ? 0 : deadline - now);
        }
    }
}

int mini_sock_add(struct mini_sock *self, struct mini_pipe *pipe)
{
    int rc;

    rc = self->sockbase->vfptr->add(self->sockbase, pipe);
    if (mini_slow(rc >= 0))
    {
        mini_sock_stat_increment(self, MINI_STAT_CURRENT_CONNECTIONS, 1);
    }
    return rc;
}

void mini_sock_rm(struct mini_sock *self, struct mini_pipe *pipe)
{
    self->sockbase->vfptr->rm(self->sockbase, pipe);
    mini_sock_stat_increment(self, MINI_STAT_CURRENT_CONNECTIONS, -1);
}

static void mini_sock_onleave(struct mini_ctx *self)
{
    struct mini_sock *sock;
    int events;

    sock = mini_cont(self, struct mini_sock, ctx);

    /*  If mini_close() was already called there's no point in adjusting the
        snd/rcv file descriptors. */
    if (mini_slow(sock->state != MINI_SOCK_STATE_ACTIVE))
        return;

    /*  Check whether socket is readable and/or writable at the moment. */
    events = sock->sockbase->vfptr->events(sock->sockbase);
    errnum_assert(events >= 0, -events);

    /*  Signal/unsignal IN as needed. */
    if (!(sock->socktype->flags & MINI_SOCKTYPE_FLAG_NORECV))
    {
        if (events & MINI_SOCKBASE_EVENT_IN)
        {
            if (!(sock->flags & MINI_SOCK_FLAG_IN))
            {
                sock->flags |= MINI_SOCK_FLAG_IN;
                mini_efd_signal(&sock->rcvfd);
            }
        }
        else
        {
            if (sock->flags & MINI_SOCK_FLAG_IN)
            {
                sock->flags &= ~MINI_SOCK_FLAG_IN;
                mini_efd_unsignal(&sock->rcvfd);
            }
        }
    }

    /*  Signal/unsignal OUT as needed. */
    if (!(sock->socktype->flags & MINI_SOCKTYPE_FLAG_NOSEND))
    {
        if (events & MINI_SOCKBASE_EVENT_OUT)
        {
            if (!(sock->flags & MINI_SOCK_FLAG_OUT))
            {
                sock->flags |= MINI_SOCK_FLAG_OUT;
                mini_efd_signal(&sock->sndfd);
            }
        }
        else
        {
            if (sock->flags & MINI_SOCK_FLAG_OUT)
            {
                sock->flags &= ~MINI_SOCK_FLAG_OUT;
                mini_efd_unsignal(&sock->sndfd);
            }
        }
    }
}

static struct mini_optset *mini_sock_optset(struct mini_sock *self, int id)
{
    int index;
    const struct mini_transport *tp;

    /*  Transport IDs are negative and start from -1. */
    index = (-id) - 1;

    /*  Check for invalid indices. */
    if (mini_slow(index < 0 || index >= MINI_MAX_TRANSPORT))
        return NULL;

    /*  If the option set already exists return it. */
    if (mini_fast(self->optsets[index] != NULL))
        return self->optsets[index];

    /*  If the option set doesn't exist yet, create it. */
    tp = mini_global_transport(id);
    if (mini_slow(!tp))
        return NULL;
    if (mini_slow(!tp->optset))
        return NULL;
    self->optsets[index] = tp->optset();

    return self->optsets[index];
}

static void mini_sock_shutdown(struct mini_fsm *self, int src, int type,
                               void *srcptr)
{
    struct mini_sock *sock;
    struct mini_list_item *it;
    struct mini_ep *ep;

    sock = mini_cont(self, struct mini_sock, fsm);

    if (mini_slow(src == MINI_FSM_ACTION && type == MINI_FSM_STOP))
    {
        mini_assert(sock->state == MINI_SOCK_STATE_ACTIVE);

        /*  Close sndfd and rcvfd. This should make any current
            select/poll using SNDFD and/or RCVFD exit. */
        if (!(sock->socktype->flags & MINI_SOCKTYPE_FLAG_NORECV))
        {
            mini_efd_stop(&sock->rcvfd);
        }
        if (!(sock->socktype->flags & MINI_SOCKTYPE_FLAG_NOSEND))
        {
            mini_efd_stop(&sock->sndfd);
        }

        /*  Ask all the associated endpoints to stop. */
        it = mini_list_begin(&sock->eps);
        while (it != mini_list_end(&sock->eps))
        {
            ep = mini_cont(it, struct mini_ep, item);
            it = mini_list_next(&sock->eps, it);
            mini_list_erase(&sock->eps, &ep->item);
            mini_list_insert(&sock->sdeps, &ep->item,
                             mini_list_end(&sock->sdeps));
            mini_ep_stop(ep);
        }
        sock->state = MINI_SOCK_STATE_STOPPING_EPS;
        goto finish2;
    }
    if (mini_slow(sock->state == MINI_SOCK_STATE_STOPPING_EPS))
    {

        if (!(src == MINI_SOCK_SRC_EP && type == MINI_EP_STOPPED))
        {
            /*  If we got here waiting for EPs to teardown, but src is
                not an EP, then it isn't safe for us to do anything,
                because we just need to wait for the EPs to finish
                up their thing.  Just bail. */
            return;
        }
        /*  Endpoint is stopped. Now we can safely deallocate it. */
        ep = (struct mini_ep *)srcptr;
        mini_list_erase(&sock->sdeps, &ep->item);
        mini_ep_term(ep);
        mini_free(ep);

    finish2:
        /*  If all the endpoints are deallocated, we can start stopping
            protocol-specific part of the socket. If there' no stop function
            we can consider it stopped straight away. */
        if (!mini_list_empty(&sock->sdeps))
            return;
        mini_assert(mini_list_empty(&sock->eps));
        sock->state = MINI_SOCK_STATE_STOPPING;
        if (!sock->sockbase->vfptr->stop)
            goto finish1;
        sock->sockbase->vfptr->stop(sock->sockbase);
        return;
    }
    if (mini_slow(sock->state == MINI_SOCK_STATE_STOPPING))
    {

        /*  We get here when the deallocation of the socket was delayed by the
            specific socket type. */
        mini_assert(src == MINI_FSM_ACTION && type == MINI_SOCK_ACTION_STOPPED);

    finish1:
        /*  Protocol-specific part of the socket is stopped.
            We can safely deallocate it. */
        sock->sockbase->vfptr->destroy(sock->sockbase);
        sock->state = MINI_SOCK_STATE_FINI;

        /*  Now we can unblock the application thread blocked in
            the mini_close() call. */
        mini_sem_post(&sock->termsem);

        return;
    }

    mini_fsm_bad_state(sock->state, src, type);
}

static void mini_sock_handler(struct mini_fsm *self, int src, int type,
                              void *srcptr)
{
    struct mini_sock *sock;
    struct mini_ep *ep;

    sock = mini_cont(self, struct mini_sock, fsm);

    switch (sock->state)
    {

        /******************************************************************************/
        /*  INIT state.                                                               */
        /******************************************************************************/
    case MINI_SOCK_STATE_INIT:
        switch (src)
        {

        case MINI_FSM_ACTION:
            switch (type)
            {
            case MINI_FSM_START:
                sock->state = MINI_SOCK_STATE_ACTIVE;
                return;
            default:
                mini_fsm_bad_action(sock->state, src, type);
            }

        default:
            mini_fsm_bad_source(sock->state, src, type);
        }

        /******************************************************************************/
        /*  ACTIVE state.                                                             */
        /******************************************************************************/
    case MINI_SOCK_STATE_ACTIVE:
        switch (src)
        {

        case MINI_FSM_ACTION:
            switch (type)
            {
            default:
                mini_fsm_bad_action(sock->state, src, type);
            }

        case MINI_SOCK_SRC_EP:
            switch (type)
            {
            case MINI_EP_STOPPED:

                /*  This happens when an endpoint is closed using
                    mini_shutdown() function. */
                ep = (struct mini_ep *)srcptr;
                mini_list_erase(&sock->sdeps, &ep->item);
                mini_ep_term(ep);
                mini_free(ep);
                return;

            default:
                mini_fsm_bad_action(sock->state, src, type);
            }

        default:

            /*  The assumption is that all the other events come from pipes. */
            switch (type)
            {
            case MINI_PIPE_IN:
                sock->sockbase->vfptr->in(sock->sockbase,
                                          (struct mini_pipe *)srcptr);
                return;
            case MINI_PIPE_OUT:
                sock->sockbase->vfptr->out(sock->sockbase,
                                           (struct mini_pipe *)srcptr);
                return;
            default:
                mini_fsm_bad_action(sock->state, src, type);
            }
        }

        /******************************************************************************/
        /*  Invalid state.                                                            */
        /******************************************************************************/
    default:
        mini_fsm_bad_state(sock->state, src, type);
    }
}

/******************************************************************************/
/*  State machine actions.                                                    */
/******************************************************************************/

void mini_sock_report_error(struct mini_sock *self, struct mini_ep *ep, int errnum)
{
    if (!mini_global_print_errors())
        return;

    if (errnum == 0)
        return;

    if (ep)
    {
        fprintf(stderr, "minimsg: socket.%s[%s]: Error: %s\n",
                self->socket_name, mini_ep_getaddr(ep), mini_strerror(errnum));
    }
    else
    {
        fprintf(stderr, "minimsg: socket.%s: Error: %s\n",
                self->socket_name, mini_strerror(errnum));
    }
}

void mini_sock_stat_increment(struct mini_sock *self, int name, int64_t increment)
{
    switch (name)
    {
    case MINI_STAT_ESTABLISHED_CONNECTIONS:
        mini_assert(increment > 0);
        self->statistics.established_connections += increment;
        break;
    case MINI_STAT_ACCEPTED_CONNECTIONS:
        mini_assert(increment > 0);
        self->statistics.accepted_connections += increment;
        break;
    case MINI_STAT_DROPPED_CONNECTIONS:
        mini_assert(increment > 0);
        self->statistics.dropped_connections += increment;
        break;
    case MINI_STAT_BROKEN_CONNECTIONS:
        mini_assert(increment > 0);
        self->statistics.broken_connections += increment;
        break;
    case MINI_STAT_CONNECT_ERRORS:
        mini_assert(increment > 0);
        self->statistics.connect_errors += increment;
        break;
    case MINI_STAT_BIND_ERRORS:
        mini_assert(increment > 0);
        self->statistics.bind_errors += increment;
        break;
    case MINI_STAT_ACCEPT_ERRORS:
        mini_assert(increment > 0);
        self->statistics.accept_errors += increment;
        break;
    case MINI_STAT_MESSAGES_SENT:
        mini_assert(increment > 0);
        self->statistics.messages_sent += increment;
        break;
    case MINI_STAT_MESSAGES_RECEIVED:
        mini_assert(increment > 0);
        self->statistics.messages_received += increment;
        break;
    case MINI_STAT_BYTES_SENT:
        mini_assert(increment >= 0);
        self->statistics.bytes_sent += increment;
        break;
    case MINI_STAT_BYTES_RECEIVED:
        mini_assert(increment >= 0);
        self->statistics.bytes_received += increment;
        break;

    case MINI_STAT_CURRENT_CONNECTIONS:
        mini_assert(increment > 0 ||
                    self->statistics.current_connections >= -increment);
        mini_assert(increment < INT_MAX && increment > -INT_MAX);
        self->statistics.current_connections += (int)increment;
        break;
    case MINI_STAT_INPROGRESS_CONNECTIONS:
        mini_assert(increment > 0 ||
                    self->statistics.inprogress_connections >= -increment);
        mini_assert(increment < INT_MAX && increment > -INT_MAX);
        self->statistics.inprogress_connections += (int)increment;
        break;
    case MINI_STAT_CURRENT_SND_PRIORITY:
        /*  This is an exception, we don't want to increment priority  */
        mini_assert((increment > 0 && increment <= 16) || increment == -1);
        self->statistics.current_snd_priority = (int)increment;
        break;
    case MINI_STAT_CURRENT_EP_ERRORS:
        mini_assert(increment > 0 ||
                    self->statistics.current_ep_errors >= -increment);
        mini_assert(increment < INT_MAX && increment > -INT_MAX);
        self->statistics.current_ep_errors += (int)increment;
        break;
    }
}

int mini_sock_hold(struct mini_sock *self)
{
    switch (self->state)
    {
    case MINI_SOCK_STATE_ACTIVE:
    case MINI_SOCK_STATE_INIT:
        self->holds++;
        return 0;
    case MINI_SOCK_STATE_STOPPING:
    case MINI_SOCK_STATE_STOPPING_EPS:
    case MINI_SOCK_STATE_FINI:
    default:
        return -EBADF;
    }
}

void mini_sock_rele(struct mini_sock *self)
{
    self->holds--;
    if (self->holds == 0)
    {
        mini_sem_post(&self->relesem);
    }
}
