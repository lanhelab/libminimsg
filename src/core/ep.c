/*
    Copyright (c) 2012 Martin Sustrik  All rights reserved.
    Copyright (c) 2013 GoPivotal, Inc.  All rights reserved.
    Copyright 2016 Garrett D'Amore <garrett@damore.org>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "../transport.h"

#include "ep.h"
#include "sock.h"

#include "../utils/err.h"
#include "../utils/cont.h"
#include "../utils/fast.h"
#include "../utils/attr.h"

#include <string.h>

#define MINI_EP_STATE_IDLE 1
#define MINI_EP_STATE_ACTIVE 2
#define MINI_EP_STATE_STOPPING 3

#define MINI_EP_ACTION_STOPPED 1

/*  Private functions. */
static void mini_ep_handler(struct mini_fsm *self, int src, int type,
                            void *srcptr);
static void mini_ep_shutdown(struct mini_fsm *self, int src, int type,
                             void *srcptr);

int mini_ep_init(struct mini_ep *self, int src, struct mini_sock *sock, int eid,
                 const struct mini_transport *transport, int bind, const char *addr)
{
    int rc;

    mini_fsm_init(&self->fsm, mini_ep_handler, mini_ep_shutdown,
                  src, self, &sock->fsm);
    self->state = MINI_EP_STATE_IDLE;

    self->sock = sock;
    self->eid = eid;
    self->last_errno = 0;
    mini_list_item_init(&self->item);
    memcpy(&self->options, &sock->ep_template, sizeof(struct mini_ep_options));

    /*  Store the textual form of the address. */
    mini_assert(strlen(addr) <= MINI_SOCKADDR_MAX);
    strcpy(self->addr, addr);

    /*  Create transport-specific part of the endpoint. */
    if (bind)
        rc = transport->bind(self);
    else
        rc = transport->connect(self);

    /*  Endpoint creation failed. */
    if (rc < 0)
    {
        mini_list_item_term(&self->item);
        mini_fsm_term(&self->fsm);
        return rc;
    }

    return 0;
}

void mini_ep_term(struct mini_ep *self)
{
    mini_assert_state(self, MINI_EP_STATE_IDLE);

    self->ops.destroy(self->tran);
    mini_list_item_term(&self->item);
    mini_fsm_term(&self->fsm);
}

void mini_ep_start(struct mini_ep *self)
{
    mini_fsm_start(&self->fsm);
}

void mini_ep_stop(struct mini_ep *self)
{
    mini_fsm_stop(&self->fsm);
}

void mini_ep_stopped(struct mini_ep *self)
{
    /*  TODO: Do the following in a more sane way. */
    self->fsm.stopped.fsm = &self->fsm;
    self->fsm.stopped.src = MINI_FSM_ACTION;
    self->fsm.stopped.srcptr = NULL;
    self->fsm.stopped.type = MINI_EP_ACTION_STOPPED;
    mini_ctx_raise(self->fsm.ctx, &self->fsm.stopped);
}

struct mini_ctx *mini_ep_getctx(struct mini_ep *self)
{
    return mini_sock_getctx(self->sock);
}

const char *mini_ep_getaddr(struct mini_ep *self)
{
    return self->addr;
}

void mini_ep_getopt(struct mini_ep *self, int level, int option,
                    void *optval, size_t *optvallen)
{
    int rc;

    rc = mini_sock_getopt_inner(self->sock, level, option, optval, optvallen);
    errnum_assert(rc == 0, -rc);
}

int mini_ep_ispeer(struct mini_ep *self, int socktype)
{
    return mini_sock_ispeer(self->sock, socktype);
}

static void mini_ep_shutdown(struct mini_fsm *self, int src, int type,
                             MINI_UNUSED void *srcptr)
{
    struct mini_ep *ep;

    ep = mini_cont(self, struct mini_ep, fsm);

    if (mini_slow(src == MINI_FSM_ACTION && type == MINI_FSM_STOP))
    {
        ep->ops.stop(ep->tran);
        ep->state = MINI_EP_STATE_STOPPING;
        return;
    }
    if (mini_slow(ep->state == MINI_EP_STATE_STOPPING))
    {
        if (src != MINI_FSM_ACTION || type != MINI_EP_ACTION_STOPPED)
            return;
        ep->state = MINI_EP_STATE_IDLE;
        mini_fsm_stopped(&ep->fsm, MINI_EP_STOPPED);
        return;
    }

    mini_fsm_bad_state(ep->state, src, type);
}

static void mini_ep_handler(struct mini_fsm *self, int src, int type,
                            MINI_UNUSED void *srcptr)
{
    struct mini_ep *ep;

    ep = mini_cont(self, struct mini_ep, fsm);

    switch (ep->state)
    {

        /******************************************************************************/
        /*  IDLE state.                                                               */
        /******************************************************************************/
    case MINI_EP_STATE_IDLE:
        switch (src)
        {

        case MINI_FSM_ACTION:
            switch (type)
            {
            case MINI_FSM_START:
                ep->state = MINI_EP_STATE_ACTIVE;
                return;
            default:
                mini_fsm_bad_action(ep->state, src, type);
            }

        default:
            mini_fsm_bad_source(ep->state, src, type);
        }

        /******************************************************************************/
        /*  ACTIVE state.                                                             */
        /*  We don't expect any events in this state. The only thing that can be done */
        /*  is closing the endpoint.                                                  */
        /******************************************************************************/
    case MINI_EP_STATE_ACTIVE:
        mini_fsm_bad_source(ep->state, src, type);

        /******************************************************************************/
        /*  Invalid state.                                                            */
        /******************************************************************************/
    default:
        mini_fsm_bad_state(ep->state, src, type);
    }
}

void mini_ep_set_error(struct mini_ep *self, int errnum)
{
    if (self->last_errno == errnum)
        /*  Error is still there, no need to report it again  */
        return;
    if (self->last_errno == 0)
        mini_sock_stat_increment(self->sock, MINI_STAT_CURRENT_EP_ERRORS, 1);
    self->last_errno = errnum;
    mini_sock_report_error(self->sock, self, errnum);
}

void mini_ep_clear_error(struct mini_ep *self)
{
    if (self->last_errno == 0)
        /*  Error is already clear, no need to report it  */
        return;
    mini_sock_stat_increment(self->sock, MINI_STAT_CURRENT_EP_ERRORS, -1);
    self->last_errno = 0;
    mini_sock_report_error(self->sock, self, 0);
}

void mini_ep_stat_increment(struct mini_ep *self, int name, int increment)
{
    mini_sock_stat_increment(self->sock, name, increment);
}

int mini_ep_ispeer_ep(struct mini_ep *self, struct mini_ep *other)
{
    return mini_ep_ispeer(self, other->sock->socktype->protocol);
}

/*  Set up an ep for use by a transport.  Note that the core will already have
    done most of the initialization steps.  The tran is passed as the argument
    to the ops. */
void mini_ep_tran_setup(struct mini_ep *ep, const struct mini_ep_ops *ops,
                        void *tran)
{
    ep->ops = *ops;
    ep->tran = tran;
}
