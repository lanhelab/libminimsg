#ifndef MINI_GLOBAL_INCLUDED
#define MINI_GLOBAL_INCLUDED

/*  Provides access to the list of available transports. */
const struct mini_transport *mini_global_transport(int id);

/*  Returns the global worker thread pool. */
struct mini_pool *mini_global_getpool();
int mini_global_print_errors();

#endif
