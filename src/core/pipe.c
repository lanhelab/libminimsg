/*
    Copyright (c) 2012-2013 Martin Sustrik  All rights reserved.
    Copyright 2016 Garrett D'Amore <garrett@damore.org>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "../transport.h"
#include "../protocol.h"

#include "sock.h"
#include "ep.h"

#include "../utils/err.h"
#include "../utils/fast.h"

/*  Internal pipe states. */
#define MINI_PIPEBASE_STATE_IDLE 1
#define MINI_PIPEBASE_STATE_ACTIVE 2
#define MINI_PIPEBASE_STATE_FAILED 3

#define MINI_PIPEBASE_INSTATE_DEACTIVATED 0
#define MINI_PIPEBASE_INSTATE_IDLE 1
#define MINI_PIPEBASE_INSTATE_RECEIVING 2
#define MINI_PIPEBASE_INSTATE_RECEIVED 3
#define MINI_PIPEBASE_INSTATE_ASYNC 4

#define MINI_PIPEBASE_OUTSTATE_DEACTIVATED 0
#define MINI_PIPEBASE_OUTSTATE_IDLE 1
#define MINI_PIPEBASE_OUTSTATE_SENDING 2
#define MINI_PIPEBASE_OUTSTATE_SENT 3
#define MINI_PIPEBASE_OUTSTATE_ASYNC 4

void mini_pipebase_init(struct mini_pipebase *self,
                        const struct mini_pipebase_vfptr *vfptr, struct mini_ep *ep)
{
    mini_assert(ep->sock);

    mini_fsm_init(&self->fsm, NULL, NULL, 0, self, &ep->sock->fsm);
    self->vfptr = vfptr;
    self->state = MINI_PIPEBASE_STATE_IDLE;
    self->instate = MINI_PIPEBASE_INSTATE_DEACTIVATED;
    self->outstate = MINI_PIPEBASE_OUTSTATE_DEACTIVATED;
    self->sock = ep->sock;
    memcpy(&self->options, &ep->options, sizeof(struct mini_ep_options));
    mini_fsm_event_init(&self->in);
    mini_fsm_event_init(&self->out);
}

void mini_pipebase_term(struct mini_pipebase *self)
{
    mini_assert_state(self, MINI_PIPEBASE_STATE_IDLE);

    mini_fsm_event_term(&self->out);
    mini_fsm_event_term(&self->in);
    mini_fsm_term(&self->fsm);
}

int mini_pipebase_start(struct mini_pipebase *self)
{
    int rc;

    mini_assert_state(self, MINI_PIPEBASE_STATE_IDLE);

    self->state = MINI_PIPEBASE_STATE_ACTIVE;
    self->instate = MINI_PIPEBASE_INSTATE_ASYNC;
    self->outstate = MINI_PIPEBASE_OUTSTATE_IDLE;
    rc = mini_sock_add(self->sock, (struct mini_pipe *)self);
    if (mini_slow(rc < 0))
    {
        self->state = MINI_PIPEBASE_STATE_FAILED;
        return rc;
    }
    mini_fsm_raise(&self->fsm, &self->out, MINI_PIPE_OUT);

    return 0;
}

void mini_pipebase_stop(struct mini_pipebase *self)
{
    if (self->state == MINI_PIPEBASE_STATE_ACTIVE)
        mini_sock_rm(self->sock, (struct mini_pipe *)self);
    self->state = MINI_PIPEBASE_STATE_IDLE;
}

void mini_pipebase_received(struct mini_pipebase *self)
{
    if (mini_fast(self->instate == MINI_PIPEBASE_INSTATE_RECEIVING))
    {
        self->instate = MINI_PIPEBASE_INSTATE_RECEIVED;
        return;
    }
    mini_assert(self->instate == MINI_PIPEBASE_INSTATE_ASYNC);
    self->instate = MINI_PIPEBASE_INSTATE_IDLE;
    mini_fsm_raise(&self->fsm, &self->in, MINI_PIPE_IN);
}

void mini_pipebase_sent(struct mini_pipebase *self)
{
    if (mini_fast(self->outstate == MINI_PIPEBASE_OUTSTATE_SENDING))
    {
        self->outstate = MINI_PIPEBASE_OUTSTATE_SENT;
        return;
    }
    mini_assert(self->outstate == MINI_PIPEBASE_OUTSTATE_ASYNC);
    self->outstate = MINI_PIPEBASE_OUTSTATE_IDLE;
    mini_fsm_raise(&self->fsm, &self->out, MINI_PIPE_OUT);
}

void mini_pipebase_getopt(struct mini_pipebase *self, int level, int option,
                          void *optval, size_t *optvallen)
{
    int rc;
    int intval;

    if (level == MINI_SOL_SOCKET)
    {
        switch (option)
        {

        /*  Endpoint options  */
        case MINI_SNDPRIO:
            intval = self->options.sndprio;
            break;
        case MINI_RCVPRIO:
            intval = self->options.rcvprio;
            break;
        case MINI_IPV4ONLY:
            intval = self->options.ipv4only;
            break;

        /*  Fallback to socket options  */
        default:
            rc = mini_sock_getopt_inner(self->sock, level,
                                        option, optval, optvallen);
            errnum_assert(rc == 0, -rc);
            return;
        }

        memcpy(optval, &intval,
               *optvallen < sizeof(int) ? *optvallen : sizeof(int));
        *optvallen = sizeof(int);

        return;
    }

    rc = mini_sock_getopt_inner(self->sock, level, option, optval, optvallen);
    errnum_assert(rc == 0, -rc);
}

int mini_pipebase_ispeer(struct mini_pipebase *self, int socktype)
{
    return mini_sock_ispeer(self->sock, socktype);
}

void mini_pipe_setdata(struct mini_pipe *self, void *data)
{
    ((struct mini_pipebase *)self)->data = data;
}

void *mini_pipe_getdata(struct mini_pipe *self)
{
    return ((struct mini_pipebase *)self)->data;
}

int mini_pipe_send(struct mini_pipe *self, struct mini_msg *msg)
{
    int rc;
    struct mini_pipebase *pipebase;

    pipebase = (struct mini_pipebase *)self;
    mini_assert(pipebase->outstate == MINI_PIPEBASE_OUTSTATE_IDLE);
    pipebase->outstate = MINI_PIPEBASE_OUTSTATE_SENDING;
    rc = pipebase->vfptr->send(pipebase, msg);
    errnum_assert(rc >= 0, -rc);
    if (mini_fast(pipebase->outstate == MINI_PIPEBASE_OUTSTATE_SENT))
    {
        pipebase->outstate = MINI_PIPEBASE_OUTSTATE_IDLE;
        return rc;
    }
    mini_assert(pipebase->outstate == MINI_PIPEBASE_OUTSTATE_SENDING);
    pipebase->outstate = MINI_PIPEBASE_OUTSTATE_ASYNC;
    return rc | MINI_PIPEBASE_RELEASE;
}

int mini_pipe_recv(struct mini_pipe *self, struct mini_msg *msg)
{
    int rc;
    struct mini_pipebase *pipebase;

    pipebase = (struct mini_pipebase *)self;
    mini_assert(pipebase->instate == MINI_PIPEBASE_INSTATE_IDLE);
    pipebase->instate = MINI_PIPEBASE_INSTATE_RECEIVING;
    rc = pipebase->vfptr->recv(pipebase, msg);
    errnum_assert(rc >= 0, -rc);

    if (mini_fast(pipebase->instate == MINI_PIPEBASE_INSTATE_RECEIVED))
    {
        pipebase->instate = MINI_PIPEBASE_INSTATE_IDLE;
        return rc;
    }
    mini_assert(pipebase->instate == MINI_PIPEBASE_INSTATE_RECEIVING);
    pipebase->instate = MINI_PIPEBASE_INSTATE_ASYNC;
    return rc | MINI_PIPEBASE_RELEASE;
}

void mini_pipe_getopt(struct mini_pipe *self, int level, int option,
                      void *optval, size_t *optvallen)
{

    struct mini_pipebase *pipebase;

    pipebase = (struct mini_pipebase *)self;
    mini_pipebase_getopt(pipebase, level, option, optval, optvallen);
}
