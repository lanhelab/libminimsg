#include "../minimsg.h"
#include "../transport.h"
#include "../protocol.h"

#include "global.h"
#include "sock.h"
#include "ep.h"

#include "../io/pool.h"
#include "../io/timer.h"

#include "../utils/err.h"
#include "../utils/alloc.h"
#include "../utils/mutex.h"
#include "../utils/condvar.h"
#include "../utils/once.h"
#include "../utils/list.h"
#include "../utils/cont.h"
#include "../utils/random.h"
#include "../utils/chunk.h"
#include "../utils/msg.h"
#include "../utils/attr.h"

#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#if defined MINI_HAVE_WINDOWS
#include "../utils/win.h"
#else
#include <unistd.h>
#endif

/*  Max number of concurrent SP sockets. Configureable at build time */
#ifndef MINI_MAX_SOCKETS
#define MINI_MAX_SOCKETS 512
#endif

/*  To save some space, list of unused socket slots uses uint16_t integers to
    refer to individual sockets. If there's a need to more that 0x10000 sockets,
    the type should be changed to uint32_t or int. */
CT_ASSERT(MINI_MAX_SOCKETS <= 0x10000);

#define MINI_CTX_FLAG_TERMED 1
#define MINI_CTX_FLAG_TERMING 2
#define MINI_CTX_FLAG_TERM (MINI_CTX_FLAG_TERMED | MINI_CTX_FLAG_TERMING)

#define MINI_GLOBAL_SRC_STAT_TIMER 1

#define MINI_GLOBAL_STATE_IDLE 1
#define MINI_GLOBAL_STATE_ACTIVE 2
#define MINI_GLOBAL_STATE_STOPPING_TIMER 3

/*  We could put these in an external header file, but there really is
    need to.  We are the only thing that needs them. */
extern struct mini_socktype mini_pair_socktype;
extern struct mini_socktype mini_xpair_socktype;
// extern struct mini_socktype mini_pub_socktype;
// extern struct mini_socktype mini_sub_socktype;
// extern struct mini_socktype mini_xpub_socktype;
// extern struct mini_socktype mini_xsub_socktype;
extern struct mini_socktype mini_rep_socktype;
extern struct mini_socktype mini_req_socktype;
extern struct mini_socktype mini_xrep_socktype;
extern struct mini_socktype mini_xreq_socktype;
// extern struct mini_socktype mini_push_socktype;
// extern struct mini_socktype mini_xpush_socktype;
// extern struct mini_socktype mini_pull_socktype;
// extern struct mini_socktype mini_xpull_socktype;
// extern struct mini_socktype mini_respondent_socktype;
// extern struct mini_socktype mini_surveyor_socktype;
// extern struct mini_socktype mini_xrespondent_socktype;
// extern struct mini_socktype mini_xsurveyor_socktype;
// extern struct mini_socktype mini_bus_socktype;
// extern struct mini_socktype mini_xbus_socktype;

/*  Array of known socket types. */
const struct mini_socktype *mini_socktypes[] = {
    &mini_pair_socktype,
    &mini_xpair_socktype,
    // &mini_pub_socktype,
    // &mini_sub_socktype,
    // &mini_xpub_socktype,
    // &mini_xsub_socktype,
    &mini_rep_socktype,
    &mini_req_socktype,
    // &mini_xrep_socktype,
    // &mini_xreq_socktype,
    // &mini_push_socktype,
    // &mini_xpush_socktype,
    // &mini_pull_socktype,
    // &mini_xpull_socktype,
    // &mini_respondent_socktype,
    // &mini_surveyor_socktype,
    // &mini_xrespondent_socktype,
    // &mini_xsurveyor_socktype,
    // &mini_bus_socktype,
    // &mini_xbus_socktype,
    NULL,
};

/* As with protocols, we could have these in a header file, but we are the
   only consumer, so just declare them inline. */

// extern struct mini_transport mini_inproc;
// extern struct mini_transport mini_ipc;
extern struct mini_transport mini_tcp;
extern struct mini_transport mini_ws;

const struct mini_transport *mini_transports[] = {
    // &mini_inproc,
    // &mini_ipc,
    &mini_tcp,
    &mini_ws,
    NULL,
};

struct mini_global
{

    /*  The global table of existing sockets. The descriptor representing
        the socket is the index to this table. This pointer is also used to
        find out whether context is initialised. If it is NULL, context is
        uninitialised. */
    struct mini_sock **socks;

    /*  Stack of unused file descriptors. */
    uint16_t *unused;

    /*  Number of actual open sockets in the socket table. */
    size_t nsocks;

    /*  Combination of the flags listed above. */
    int flags;

    /*  Pool of worker threads. */
    struct mini_pool pool;

    /*  Timer and other machinery for submitting statistics  */
    int state;

    int print_errors;

    int inited;
    mini_mutex_t lock;
    mini_condvar_t cond;
};

/*  Singleton object containing the global state of the library. */
static struct mini_global self;
static mini_once_t once = MINI_ONCE_INITIALIZER;

/*  Context creation- and termination-related private functions. */
static void mini_global_init(void);
static void mini_global_term(void);

/*  Private function that unifies mini_bind and mini_connect functionality.
    It returns the ID of the newly created endpoint. */
static int mini_global_create_ep(struct mini_sock *, const char *addr, int bind);

/*  Private socket creator which doesn't initialize global state and
    does no locking by itself */
static int mini_global_create_socket(int domain, int protocol);

/*  Socket holds. */
static int mini_global_hold_socket(struct mini_sock **sockp, int s);
static int mini_global_hold_socket_locked(struct mini_sock **sockp, int s);
static void mini_global_rele_socket(struct mini_sock *);

int mini_errno(void)
{
    return mini_err_errno();
}

const char *mini_strerror(int errnum)
{
    return mini_err_strerror(errnum);
}

static void mini_global_init(void)
{
    int i;
    char *envvar;

#if defined MINI_HAVE_WINDOWS
    int rc;
    WSADATA data;
#endif
    const struct mini_transport *tp;

    /*  Check whether the library was already initialised. If so, do nothing. */
    if (self.socks)
        return;

        /*  On Windows, initialise the socket library. */
#if defined MINI_HAVE_WINDOWS
    rc = WSAStartup(MAKEWORD(2, 2), &data);
    mini_assert(rc == 0);
    mini_assert(LOBYTE(data.wVersion) == 2 &&
                HIBYTE(data.wVersion) == 2);
#endif

    /*  Initialise the memory allocation subsystem. */
    mini_alloc_init();

    /*  Seed the pseudo-random number generator. */
    mini_random_seed();

    /*  Allocate the global table of SP sockets. */
    self.socks = mini_alloc((sizeof(struct mini_sock *) * MINI_MAX_SOCKETS) +
                                (sizeof(uint16_t) * MINI_MAX_SOCKETS),
                            "socket table");
    alloc_assert(self.socks);
    for (i = 0; i != MINI_MAX_SOCKETS; ++i)
        self.socks[i] = NULL;
    self.nsocks = 0;
    self.flags = 0;

    /*  Print connection and accepting errors to the stderr  */
    envvar = getenv("MINI_PRINT_ERRORS");
    /*  any non-empty string is true */
    self.print_errors = envvar && *envvar;

    /*  Allocate the stack of unused file descriptors. */
    self.unused = (uint16_t *)(self.socks + MINI_MAX_SOCKETS);
    alloc_assert(self.unused);
    for (i = 0; i != MINI_MAX_SOCKETS; ++i)
        self.unused[i] = MINI_MAX_SOCKETS - i - 1;

    /*  Initialize transports if needed. */
    for (i = 0; (tp = mini_transports[i]) != NULL; i++)
    {
        if (tp->init != NULL)
        {
            tp->init();
        }
    }

    /*  Start the worker threads. */
    mini_pool_init(&self.pool);
}

static void mini_global_term(void)
{
#if defined MINI_HAVE_WINDOWS
    int rc;
#endif
    const struct mini_transport *tp;
    int i;

    /*  If there are no sockets remaining, uninitialise the global context. */
    mini_assert(self.socks);
    if (self.nsocks > 0)
        return;

    /*  Shut down the worker threads. */
    mini_pool_term(&self.pool);

    /*  Ask all the transport to deallocate their global resources. */
    for (i = 0; (tp = mini_transports[i]) != NULL; i++)
    {
        if (tp->term)
            tp->term();
    }

    /*  Final deallocation of the mini_global object itself. */
    mini_free(self.socks);

    /*  This marks the global state as uninitialised. */
    self.socks = NULL;

    /*  Shut down the memory allocation subsystem. */
    mini_alloc_term();

    /*  On Windows, uninitialise the socket library. */
#if defined MINI_HAVE_WINDOWS
    rc = WSACleanup();
    mini_assert(rc == 0);
#endif
}

void mini_term(void)
{
    int i;

    if (!self.inited)
    {
        return;
    }

    mini_mutex_lock(&self.lock);
    self.flags |= MINI_CTX_FLAG_TERMING;
    mini_mutex_unlock(&self.lock);

    /* Make sure we really close resources, this will cause global
       resources to be freed too when the last socket is closed. */
    for (i = 0; i < MINI_MAX_SOCKETS; i++)
    {
        (void)mini_close(i);
    }

    mini_mutex_lock(&self.lock);
    self.flags |= MINI_CTX_FLAG_TERMED;
    self.flags &= ~MINI_CTX_FLAG_TERMING;
    mini_condvar_broadcast(&self.cond);
    mini_mutex_unlock(&self.lock);
}

static void mini_lib_init(void)
{
    /*  This function is executed once to initialize global locks. */
    mini_mutex_init(&self.lock);
    mini_condvar_init(&self.cond);
    self.inited = 1;
}

void mini_init(void)
{
    mini_do_once(&once, mini_lib_init);

    mini_mutex_lock(&self.lock);
    /*  Wait for any in progress term to complete. */
    while (self.flags & MINI_CTX_FLAG_TERMING)
    {
        mini_condvar_wait(&self.cond, &self.lock, -1);
    }
    self.flags &= ~MINI_CTX_FLAG_TERMED;
    mini_mutex_unlock(&self.lock);
}

void *mini_allocmsg(size_t size, int type)
{
    int rc;
    void *result;

    rc = mini_chunk_alloc(size, type, &result);
    if (rc == 0)
        return result;
    errno = -rc;
    return NULL;
}

void *mini_reallocmsg(void *msg, size_t size)
{
    int rc;

    rc = mini_chunk_realloc(size, &msg);
    if (rc == 0)
        return msg;
    errno = -rc;
    return NULL;
}

int mini_freemsg(void *msg)
{
    mini_chunk_free(msg);
    return 0;
}

struct mini_cmsghdr *mini_cmsg_nxthdr_(const struct mini_msghdr *mhdr,
                                       const struct mini_cmsghdr *cmsg)
{
    char *data;
    size_t sz;
    struct mini_cmsghdr *next;
    size_t headsz;

    /*  Early return if no message is provided. */
    if (mini_slow(mhdr == NULL))
        return NULL;

    /*  Get the actual data. */
    if (mhdr->msg_controllen == MINI_MSG)
    {
        data = *((void **)mhdr->msg_control);
        sz = mini_chunk_size(data);
    }
    else
    {
        data = (char *)mhdr->msg_control;
        sz = mhdr->msg_controllen;
    }

    /*  Ancillary data allocation was not even large enough for one element. */
    if (mini_slow(sz < MINI_CMSG_SPACE(0)))
        return NULL;

    /*  If cmsg is set to NULL we are going to return first property.
        Otherwise move to the next property. */
    if (!cmsg)
        next = (struct mini_cmsghdr *)data;
    else
        next = (struct mini_cmsghdr *)(((char *)cmsg) + MINI_CMSG_ALIGN_(cmsg->cmsg_len));

    /*  If there's no space for next property, treat it as the end
        of the property list. */
    headsz = ((char *)next) - data;
    if (headsz + MINI_CMSG_SPACE(0) > sz ||
        headsz + MINI_CMSG_ALIGN_(next->cmsg_len) > sz)
        return NULL;

    /*  Success. */
    return next;
}

int mini_global_create_socket(int domain, int protocol)
{
    int rc;
    int s;
    int i;
    const struct mini_socktype *socktype;
    struct mini_sock *sock;

    /* The function is called with lock held */

    /*  Only AF_SP and AF_SP_RAW domains are supported. */
    if (domain != AF_SP && domain != AF_SP_RAW)
    {
        return -EAFNOSUPPORT;
    }

    /*  If socket limit was reached, report error. */
    if (self.nsocks >= MINI_MAX_SOCKETS)
    {
        return -EMFILE;
    }

    /*  Find an empty socket slot. */
    s = self.unused[MINI_MAX_SOCKETS - self.nsocks - 1];

    /*  Find the appropriate socket type. */
    for (i = 0; (socktype = mini_socktypes[i]) != NULL; i++)
    {
        if (socktype->domain == domain && socktype->protocol == protocol)
        {

            /*  Instantiate the socket. */
            if ((sock = mini_alloc(sizeof(struct mini_sock), "sock")) == NULL)
                return -ENOMEM;
            rc = mini_sock_init(sock, socktype, s);
            if (rc < 0)
            {
                mini_free(sock);
                return rc;
            }

            /*  Adjust the global socket table. */
            self.socks[s] = sock;
            ++self.nsocks;
            return s;
        }
    }
    /*  Specified socket type wasn't found. */
    return -EINVAL;
}

int mini_socket(int domain, int protocol)
{
    int rc;

    mini_do_once(&once, mini_lib_init);

    mini_mutex_lock(&self.lock);

    /*  If mini_term() was already called, return ETERM. */
    if (mini_slow(self.flags & MINI_CTX_FLAG_TERM))
    {
        mini_mutex_unlock(&self.lock);
        errno = ETERM;
        return -1;
    }

    /*  Make sure that global state is initialised. */
    mini_global_init();

    rc = mini_global_create_socket(domain, protocol);

    if (rc < 0)
    {
        mini_global_term();
        mini_mutex_unlock(&self.lock);
        errno = -rc;
        return -1;
    }

    mini_mutex_unlock(&self.lock);

    return rc;
}

int mini_close(int s)
{
    int rc;
    struct mini_sock *sock;

    mini_mutex_lock(&self.lock);
    rc = mini_global_hold_socket_locked(&sock, s);
    if (mini_slow(rc < 0))
    {
        mini_mutex_unlock(&self.lock);
        errno = -rc;
        return -1;
    }

    /*  Start the shutdown process on the socket.  This will cause
        all other socket users, as well as endpoints, to begin cleaning up.
        This is done with the lock held to ensure that two instances
        of mini_close can't access the same socket. */
    mini_sock_stop(sock);

    /*  We have to drop both the hold we just acquired, as well as
        the original hold, in order for mini_sock_term to complete. */
    mini_sock_rele(sock);
    mini_sock_rele(sock);
    mini_mutex_unlock(&self.lock);

    /*  Now clean up.  The termination routine below will block until
        all other consumers of the socket have dropped their holds, and
        all endpoints have cleanly exited. */
    rc = mini_sock_term(sock);
    if (mini_slow(rc == -EINTR))
    {
        mini_global_rele_socket(sock);
        errno = EINTR;
        return -1;
    }

    /*  Remove the socket from the socket table, add it to unused socket
        table. */
    mini_mutex_lock(&self.lock);
    self.socks[s] = NULL;
    self.unused[MINI_MAX_SOCKETS - self.nsocks] = s;
    --self.nsocks;
    mini_free(sock);

    /*  Destroy the global context if there's no socket remaining. */
    mini_global_term();

    mini_mutex_unlock(&self.lock);

    return 0;
}

int mini_setsockopt(int s, int level, int option, const void *optval,
                    size_t optvallen)
{
    int rc;
    struct mini_sock *sock;

    rc = mini_global_hold_socket(&sock, s);
    if (mini_slow(rc < 0))
    {
        errno = -rc;
        return -1;
    }

    if (mini_slow(!optval && optvallen))
    {
        rc = -EFAULT;
        goto fail;
    }

    rc = mini_sock_setopt(sock, level, option, optval, optvallen);
    if (mini_slow(rc < 0))
        goto fail;
    errnum_assert(rc == 0, -rc);
    mini_global_rele_socket(sock);
    return 0;

fail:
    mini_global_rele_socket(sock);
    errno = -rc;
    return -1;
}

int mini_getsockopt(int s, int level, int option, void *optval,
                    size_t *optvallen)
{
    int rc;
    struct mini_sock *sock;

    rc = mini_global_hold_socket(&sock, s);
    if (mini_slow(rc < 0))
    {
        errno = -rc;
        return -1;
    }

    if (mini_slow(!optval && optvallen))
    {
        rc = -EFAULT;
        goto fail;
    }

    rc = mini_sock_getopt(sock, level, option, optval, optvallen);
    if (mini_slow(rc < 0))
        goto fail;
    errnum_assert(rc == 0, -rc);
    mini_global_rele_socket(sock);
    return 0;

fail:
    mini_global_rele_socket(sock);
    errno = -rc;
    return -1;
}

int mini_bind(int s, const char *addr)
{
    int rc;
    struct mini_sock *sock;

    rc = mini_global_hold_socket(&sock, s);
    if (rc < 0)
    {
        errno = -rc;
        return -1;
    }

    rc = mini_global_create_ep(sock, addr, 1);
    if (mini_slow(rc < 0))
    {
        mini_global_rele_socket(sock);
        errno = -rc;
        return -1;
    }

    mini_global_rele_socket(sock);
    return rc;
}

int mini_connect(int s, const char *addr)
{
    int rc;
    struct mini_sock *sock;

    rc = mini_global_hold_socket(&sock, s);
    if (mini_slow(rc < 0))
    {
        errno = -rc;
        return -1;
    }

    rc = mini_global_create_ep(sock, addr, 0);
    if (rc < 0)
    {
        mini_global_rele_socket(sock);
        errno = -rc;
        return -1;
    }

    mini_global_rele_socket(sock);
    return rc;
}

int mini_shutdown(int s, int how)
{
    int rc;
    struct mini_sock *sock;

    rc = mini_global_hold_socket(&sock, s);
    if (mini_slow(rc < 0))
    {
        errno = -rc;
        return -1;
    }

    rc = mini_sock_rm_ep(sock, how);
    if (mini_slow(rc < 0))
    {
        mini_global_rele_socket(sock);
        errno = -rc;
        return -1;
    }
    mini_assert(rc == 0);

    mini_global_rele_socket(sock);
    return 0;
}

int mini_send(int s, const void *buf, size_t len, int flags)
{
    struct mini_iovec iov;
    struct mini_msghdr hdr;

    iov.iov_base = (void *)buf;
    iov.iov_len = len;

    hdr.msg_iov = &iov;
    hdr.msg_iovlen = 1;
    hdr.msg_control = NULL;
    hdr.msg_controllen = 0;

    return mini_sendmsg(s, &hdr, flags);
}

int mini_recv(int s, void *buf, size_t len, int flags)
{
    struct mini_iovec iov;
    struct mini_msghdr hdr;

    iov.iov_base = buf;
    iov.iov_len = len;

    hdr.msg_iov = &iov;
    hdr.msg_iovlen = 1;
    hdr.msg_control = NULL;
    hdr.msg_controllen = 0;

    return mini_recvmsg(s, &hdr, flags);
}

int mini_sendmsg(int s, const struct mini_msghdr *msghdr, int flags)
{
    int rc;
    size_t sz;
    size_t spsz;
    int i;
    struct mini_iovec *iov;
    struct mini_msg msg;
    void *chunk;
    int minimsg;
    struct mini_cmsghdr *cmsg;
    struct mini_sock *sock;

    rc = mini_global_hold_socket(&sock, s);
    if (mini_slow(rc < 0))
    {
        errno = -rc;
        return -1;
    }

    if (mini_slow(!msghdr))
    {
        rc = -EINVAL;
        goto fail;
    }

    if (mini_slow(msghdr->msg_iovlen < 0))
    {
        rc = -EMSGSIZE;
        goto fail;
    }

    if (msghdr->msg_iovlen == 1 && msghdr->msg_iov[0].iov_len == MINI_MSG)
    {
        chunk = *(void **)msghdr->msg_iov[0].iov_base;
        if (mini_slow(chunk == NULL))
        {
            rc = -EFAULT;
            goto fail;
        }
        sz = mini_chunk_size(chunk);
        mini_msg_init_chunk(&msg, chunk);
        minimsg = 1;
    }
    else
    {

        /*  Compute the total size of the message. */
        sz = 0;
        for (i = 0; i != msghdr->msg_iovlen; ++i)
        {
            iov = &msghdr->msg_iov[i];
            if (mini_slow(iov->iov_len == MINI_MSG))
            {
                rc = -EINVAL;
                goto fail;
            }
            if (mini_slow(!iov->iov_base && iov->iov_len))
            {
                rc = -EFAULT;
                goto fail;
            }
            if (mini_slow(sz + iov->iov_len < sz))
            {
                rc = -EINVAL;
                goto fail;
            }
            sz += iov->iov_len;
        }

        /*  Create a message object from the supplied scatter array. */
        mini_msg_init(&msg, sz);
        sz = 0;
        for (i = 0; i != msghdr->msg_iovlen; ++i)
        {
            iov = &msghdr->msg_iov[i];
            memcpy(((uint8_t *)mini_chunkref_data(&msg.body)) + sz,
                   iov->iov_base, iov->iov_len);
            sz += iov->iov_len;
        }

        minimsg = 0;
    }

    /*  Add ancillary data to the message. */
    if (msghdr->msg_control)
    {

        /*  Copy all headers. */
        /*  TODO: SP_HDR should not be copied here! */
        if (msghdr->msg_controllen == MINI_MSG)
        {
            chunk = *((void **)msghdr->msg_control);
            mini_chunkref_term(&msg.hdrs);
            mini_chunkref_init_chunk(&msg.hdrs, chunk);
        }
        else
        {
            mini_chunkref_term(&msg.hdrs);
            mini_chunkref_init(&msg.hdrs, msghdr->msg_controllen);
            memcpy(mini_chunkref_data(&msg.hdrs),
                   msghdr->msg_control, msghdr->msg_controllen);
        }

        /* Search for SP_HDR property. */
        cmsg = MINI_CMSG_FIRSTHDR(msghdr);
        while (cmsg)
        {
            if (cmsg->cmsg_level == PROTO_SP && cmsg->cmsg_type == SP_HDR)
            {
                unsigned char *ptr = MINI_CMSG_DATA(cmsg);
                size_t clen = cmsg->cmsg_len - MINI_CMSG_SPACE(0);
                if (clen > sizeof(size_t))
                {
                    spsz = *(size_t *)(void *)ptr;
                    if (spsz <= (clen - sizeof(size_t)))
                    {
                        /*  Copy body of SP_HDR property into 'sphdr'. */
                        mini_chunkref_term(&msg.sphdr);
                        mini_chunkref_init(&msg.sphdr, spsz);
                        memcpy(mini_chunkref_data(&msg.sphdr),
                               ptr + sizeof(size_t), spsz);
                    }
                }
                break;
            }
            cmsg = MINI_CMSG_NXTHDR(msghdr, cmsg);
        }
    }

    /*  Send it further down the stack. */
    rc = mini_sock_send(sock, &msg, flags);
    if (mini_slow(rc < 0))
    {

        /*  If we are dealing with user-supplied buffer, detach it from
            the message object. */
        if (minimsg)
            mini_chunkref_init(&msg.body, 0);

        mini_msg_term(&msg);
        goto fail;
    }

    /*  Adjust the statistics. */
    mini_sock_stat_increment(sock, MINI_STAT_MESSAGES_SENT, 1);
    mini_sock_stat_increment(sock, MINI_STAT_BYTES_SENT, sz);

    mini_global_rele_socket(sock);

    return (int)sz;

fail:
    mini_global_rele_socket(sock);

    errno = -rc;
    return -1;
}

int mini_recvmsg(int s, struct mini_msghdr *msghdr, int flags)
{
    int rc;
    struct mini_msg msg;
    uint8_t *data;
    size_t sz;
    int i;
    struct mini_iovec *iov;
    void *chunk;
    size_t hdrssz;
    void *ctrl;
    size_t ctrlsz;
    size_t spsz;
    size_t sptotalsz;
    struct mini_cmsghdr *chdr;
    struct mini_sock *sock;

    rc = mini_global_hold_socket(&sock, s);
    if (mini_slow(rc < 0))
    {
        errno = -rc;
        return -1;
    }

    if (mini_slow(!msghdr))
    {
        rc = -EINVAL;
        goto fail;
    }

    if (mini_slow(msghdr->msg_iovlen < 0))
    {
        rc = -EMSGSIZE;
        goto fail;
    }

    /*  Get a message. */
    rc = mini_sock_recv(sock, &msg, flags);
    if (mini_slow(rc < 0))
    {
        goto fail;
    }

    if (msghdr->msg_iovlen == 1 && msghdr->msg_iov[0].iov_len == MINI_MSG)
    {
        chunk = mini_chunkref_getchunk(&msg.body);
        *(void **)(msghdr->msg_iov[0].iov_base) = chunk;
        sz = mini_chunk_size(chunk);
    }
    else
    {

        /*  Copy the message content into the supplied gather array. */
        data = mini_chunkref_data(&msg.body);
        sz = mini_chunkref_size(&msg.body);
        for (i = 0; i != msghdr->msg_iovlen; ++i)
        {
            iov = &msghdr->msg_iov[i];
            if (mini_slow(iov->iov_len == MINI_MSG))
            {
                mini_msg_term(&msg);
                rc = -EINVAL;
                goto fail;
            }
            if (iov->iov_len > sz)
            {
                memcpy(iov->iov_base, data, sz);
                break;
            }
            memcpy(iov->iov_base, data, iov->iov_len);
            data += iov->iov_len;
            sz -= iov->iov_len;
        }
        sz = mini_chunkref_size(&msg.body);
    }

    /*  Retrieve the ancillary data from the message. */
    if (msghdr->msg_control)
    {

        spsz = mini_chunkref_size(&msg.sphdr);
        sptotalsz = MINI_CMSG_SPACE(spsz + sizeof(size_t));
        ctrlsz = sptotalsz + mini_chunkref_size(&msg.hdrs);

        if (msghdr->msg_controllen == MINI_MSG)
        {

            /* Allocate the buffer. */
            rc = mini_chunk_alloc(ctrlsz, 0, &ctrl);
            errnum_assert(rc == 0, -rc);

            /* Set output parameters. */
            *((void **)msghdr->msg_control) = ctrl;
        }
        else
        {

            /* Just use the buffer supplied by the user. */
            ctrl = msghdr->msg_control;
            ctrlsz = msghdr->msg_controllen;
        }

        /* If SP header alone won't fit into the buffer, return no ancillary
           properties. */
        if (ctrlsz >= sptotalsz)
        {
            char *ptr;

            /*  Fill in SP_HDR ancillary property. */
            chdr = (struct mini_cmsghdr *)ctrl;
            chdr->cmsg_len = sptotalsz;
            chdr->cmsg_level = PROTO_SP;
            chdr->cmsg_type = SP_HDR;
            ptr = (void *)chdr;
            ptr += sizeof(*chdr);
            *(size_t *)(void *)ptr = spsz;
            ptr += sizeof(size_t);
            memcpy(ptr, mini_chunkref_data(&msg.sphdr), spsz);

            /*  Fill in as many remaining properties as possible.
                Truncate the trailing properties if necessary. */
            hdrssz = mini_chunkref_size(&msg.hdrs);
            if (hdrssz > ctrlsz - sptotalsz)
                hdrssz = ctrlsz - sptotalsz;
            memcpy(((char *)ctrl) + sptotalsz,
                   mini_chunkref_data(&msg.hdrs), hdrssz);
        }
    }

    mini_msg_term(&msg);

    /*  Adjust the statistics. */
    mini_sock_stat_increment(sock, MINI_STAT_MESSAGES_RECEIVED, 1);
    mini_sock_stat_increment(sock, MINI_STAT_BYTES_RECEIVED, sz);

    mini_global_rele_socket(sock);

    return (int)sz;

fail:
    mini_global_rele_socket(sock);

    errno = -rc;
    return -1;
}

uint64_t mini_get_statistic(int s, int statistic)
{
    int rc;
    struct mini_sock *sock;
    uint64_t val;

    rc = mini_global_hold_socket(&sock, s);
    if (mini_slow(rc < 0))
    {
        errno = -rc;
        return (uint64_t)-1;
    }

    switch (statistic)
    {
    case MINI_STAT_ESTABLISHED_CONNECTIONS:
        val = sock->statistics.established_connections;
        break;
    case MINI_STAT_ACCEPTED_CONNECTIONS:
        val = sock->statistics.accepted_connections;
        break;
    case MINI_STAT_DROPPED_CONNECTIONS:
        val = sock->statistics.dropped_connections;
        break;
    case MINI_STAT_BROKEN_CONNECTIONS:
        val = sock->statistics.broken_connections;
        break;
    case MINI_STAT_CONNECT_ERRORS:
        val = sock->statistics.connect_errors;
        break;
    case MINI_STAT_BIND_ERRORS:
        val = sock->statistics.bind_errors;
        break;
    case MINI_STAT_ACCEPT_ERRORS:
        val = sock->statistics.bind_errors;
        break;
    case MINI_STAT_MESSAGES_SENT:
        val = sock->statistics.messages_sent;
        break;
    case MINI_STAT_MESSAGES_RECEIVED:
        val = sock->statistics.messages_received;
        break;
    case MINI_STAT_BYTES_SENT:
        val = sock->statistics.bytes_sent;
        break;
    case MINI_STAT_BYTES_RECEIVED:
        val = sock->statistics.bytes_received;
        break;
    case MINI_STAT_CURRENT_CONNECTIONS:
        val = sock->statistics.current_connections;
        break;
    case MINI_STAT_INPROGRESS_CONNECTIONS:
        val = sock->statistics.inprogress_connections;
        break;
    case MINI_STAT_CURRENT_SND_PRIORITY:
        val = sock->statistics.current_snd_priority;
        break;
    case MINI_STAT_CURRENT_EP_ERRORS:
        val = sock->statistics.current_ep_errors;
        break;
    default:
        val = (uint64_t)-1;
        errno = EINVAL;
        break;
    }

    mini_global_rele_socket(sock);
    return val;
}

static int mini_global_create_ep(struct mini_sock *sock, const char *addr,
                                 int bind)
{
    int rc;
    const char *proto;
    const char *delim;
    size_t protosz;
    const struct mini_transport *tp;
    int i;

    /*  Check whether address is valid. */
    if (!addr)
        return -EINVAL;
    if (strlen(addr) >= MINI_SOCKADDR_MAX)
        return -ENAMETOOLONG;

    /*  Separate the protocol and the actual address. */
    proto = addr;
    delim = strchr(addr, ':');
    if (!delim)
        return -EINVAL;
    if (delim[1] != '/' || delim[2] != '/')
        return -EINVAL;
    protosz = delim - addr;
    addr += protosz + 3;

    /*  Find the specified protocol. */
    tp = NULL;
    for (i = 0; ((tp = mini_transports[i]) != NULL); i++)
    {
        if (strlen(tp->name) == protosz &&
            memcmp(tp->name, proto, protosz) == 0)
            break;
    }

    /*  The protocol specified doesn't match any known protocol. */
    if (tp == NULL)
    {
        return -EPROTONOSUPPORT;
    }

    /*  Ask the socket to create the endpoint. */
    rc = mini_sock_add_ep(sock, tp, bind, addr);
    return rc;
}

const struct mini_transport *mini_global_transport(int id)
{
    const struct mini_transport *tp;
    int i;

    for (i = 0; (tp = mini_transports[i]) != NULL; i++)
    {
        if (tp->id == id)
            return tp;
    }
    return NULL;
}

struct mini_pool *mini_global_getpool()
{
    return &self.pool;
}

int mini_global_print_errors()
{
    return self.print_errors;
}

/*  Get the socket structure for a socket id.  This must be called under
    the global lock (self.lock.)  The socket itself will not be freed
    while the hold is active. */
int mini_global_hold_socket_locked(struct mini_sock **sockp, int s)
{
    struct mini_sock *sock;

    if (mini_slow(s < 0 || s >= MINI_MAX_SOCKETS || self.socks == NULL))
        return -EBADF;

    sock = self.socks[s];
    if (mini_slow(sock == NULL))
    {
        return -EBADF;
    }

    if (mini_slow(mini_sock_hold(sock) != 0))
    {
        return -EBADF;
    }
    *sockp = sock;
    return 0;
}

int mini_global_hold_socket(struct mini_sock **sockp, int s)
{
    int rc;
    mini_mutex_lock(&self.lock);
    rc = mini_global_hold_socket_locked(sockp, s);
    mini_mutex_unlock(&self.lock);
    return rc;
}

void mini_global_rele_socket(struct mini_sock *sock)
{
    mini_mutex_lock(&self.lock);
    mini_sock_rele(sock);
    mini_mutex_unlock(&self.lock);
}
