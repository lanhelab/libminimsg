/*
    Copyright (c) 2013 Martin Sustrik  All rights reserved.

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "../protocol.h"

#include "sock.h"

#include "../utils/err.h"
#include "../utils/attr.h"

void mini_sockbase_init(struct mini_sockbase *self,
                        const struct mini_sockbase_vfptr *vfptr, void *hint)
{
    self->vfptr = vfptr;
    self->sock = (struct mini_sock *)hint;
}

void mini_sockbase_term(MINI_UNUSED struct mini_sockbase *self)
{
}

void mini_sockbase_stopped(struct mini_sockbase *self)
{
    mini_sock_stopped(self->sock);
}

struct mini_ctx *mini_sockbase_getctx(struct mini_sockbase *self)
{
    return mini_sock_getctx(self->sock);
}

int mini_sockbase_getopt(struct mini_sockbase *self, int option,
                         void *optval, size_t *optvallen)
{
    return mini_sock_getopt_inner(self->sock, MINI_SOL_SOCKET, option,
                                  optval, optvallen);
}

void mini_sockbase_stat_increment(struct mini_sockbase *self, int name,
                                  int increment)
{
    mini_sock_stat_increment(self->sock, name, increment);
}
