/*
    Copyright 2016 Garrett D'Amore <garrett@damore.org>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#ifndef MINI_ONCE_INCLUDED
#define MINI_ONCE_INCLUDED

#ifdef MINI_HAVE_WINDOWS
#include "win.h"
struct mini_once
{
    INIT_ONCE once;
};
#define MINI_ONCE_INITIALIZER \
    {                         \
        INIT_ONCE_STATIC_INIT \
    }

#else /* !MINI_HAVE_WINDOWS */

#include <pthread.h>

struct mini_once
{
    pthread_once_t once;
};
#define MINI_ONCE_INITIALIZER \
    {                         \
        PTHREAD_ONCE_INIT     \
    }

#endif /* MINI_HAVE_WINDOWS */

typedef struct mini_once mini_once_t;
void mini_do_once(mini_once_t *once, void (*func)(void));

#endif /* MINI_ONCE_INCLUDED */
