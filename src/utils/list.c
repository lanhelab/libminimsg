/*
    Copyright (c) 2012 Martin Sustrik  All rights reserved.

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include <stddef.h>

#include "list.h"
#include "err.h"
#include "attr.h"

void mini_list_init(struct mini_list *self)
{
    self->first = NULL;
    self->last = NULL;
}

void mini_list_term(struct mini_list *self)
{
    mini_assert(self->first == NULL);
    mini_assert(self->last == NULL);
}

int mini_list_empty(struct mini_list *self)
{
    return self->first ? 0 : 1;
}

struct mini_list_item *mini_list_begin(struct mini_list *self)
{
    return self->first;
}

struct mini_list_item *mini_list_end(MINI_UNUSED struct mini_list *self)
{
    return NULL;
}

struct mini_list_item *mini_list_prev(struct mini_list *self,
                                      struct mini_list_item *it)
{
    if (!it)
        return self->last;
    mini_assert(it->prev != MINI_LIST_NOTINLIST);
    return it->prev;
}

struct mini_list_item *mini_list_next(MINI_UNUSED struct mini_list *self,
                                      struct mini_list_item *it)
{
    mini_assert(it->next != MINI_LIST_NOTINLIST);
    return it->next;
}

void mini_list_insert(struct mini_list *self, struct mini_list_item *item,
                      struct mini_list_item *it)
{
    mini_assert(!mini_list_item_isinlist(item));

    item->prev = it ? it->prev : self->last;
    item->next = it;
    if (item->prev)
        item->prev->next = item;
    if (item->next)
        item->next->prev = item;
    if (!self->first || self->first == it)
        self->first = item;
    if (!it)
        self->last = item;
}

struct mini_list_item *mini_list_erase(struct mini_list *self,
                                       struct mini_list_item *item)
{
    struct mini_list_item *next;

    mini_assert(mini_list_item_isinlist(item));

    if (item->prev)
        item->prev->next = item->next;
    else
        self->first = item->next;
    if (item->next)
        item->next->prev = item->prev;
    else
        self->last = item->prev;

    next = item->next;

    item->prev = MINI_LIST_NOTINLIST;
    item->next = MINI_LIST_NOTINLIST;

    return next;
}

void mini_list_item_init(struct mini_list_item *self)
{
    self->prev = MINI_LIST_NOTINLIST;
    self->next = MINI_LIST_NOTINLIST;
}

void mini_list_item_term(struct mini_list_item *self)
{
    mini_assert(!mini_list_item_isinlist(self));
}

int mini_list_item_isinlist(struct mini_list_item *self)
{
    return self->prev == MINI_LIST_NOTINLIST ? 0 : 1;
}
