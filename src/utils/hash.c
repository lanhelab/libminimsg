/*
    Copyright (c) 2012-2013 Martin Sustrik  All rights reserved.

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "hash.h"
#include "fast.h"
#include "alloc.h"
#include "cont.h"
#include "err.h"

#define MINI_HASH_INITIAL_SLOTS 32

static uint32_t mini_hash_key(uint32_t key);

void mini_hash_init(struct mini_hash *self)
{
    uint32_t i;

    self->slots = MINI_HASH_INITIAL_SLOTS;
    self->items = 0;
    self->array = mini_alloc(sizeof(struct mini_list) * MINI_HASH_INITIAL_SLOTS,
                             "hash map");
    alloc_assert(self->array);
    for (i = 0; i != MINI_HASH_INITIAL_SLOTS; ++i)
        mini_list_init(&self->array[i]);
}

void mini_hash_term(struct mini_hash *self)
{
    uint32_t i;

    for (i = 0; i != self->slots; ++i)
        mini_list_term(&self->array[i]);
    mini_free(self->array);
}

static void mini_hash_rehash(struct mini_hash *self)
{
    uint32_t i;
    uint32_t oldslots;
    struct mini_list *oldarray;
    struct mini_hash_item *hitm;
    uint32_t newslot;

    /*  Allocate new double-sized array of slots. */
    oldslots = self->slots;
    oldarray = self->array;
    self->slots *= 2;
    self->array = mini_alloc(sizeof(struct mini_list) * self->slots, "hash map");
    alloc_assert(self->array);
    for (i = 0; i != self->slots; ++i)
        mini_list_init(&self->array[i]);

    /*  Move the items from old slot array to new slot array. */
    for (i = 0; i != oldslots; ++i)
    {
        while (!mini_list_empty(&oldarray[i]))
        {
            hitm = mini_cont(mini_list_begin(&oldarray[i]),
                             struct mini_hash_item, list);
            mini_list_erase(&oldarray[i], &hitm->list);
            newslot = mini_hash_key(hitm->key) % self->slots;
            mini_list_insert(&self->array[newslot], &hitm->list,
                             mini_list_end(&self->array[newslot]));
        }

        mini_list_term(&oldarray[i]);
    }

    /*  Deallocate the old array of slots. */
    mini_free(oldarray);
}

void mini_hash_insert(struct mini_hash *self, uint32_t key,
                      struct mini_hash_item *item)
{
    struct mini_list_item *it;
    uint32_t i;

    i = mini_hash_key(key) % self->slots;

    for (it = mini_list_begin(&self->array[i]);
         it != mini_list_end(&self->array[i]);
         it = mini_list_next(&self->array[i], it))
        mini_assert(mini_cont(it, struct mini_hash_item, list)->key != key);

    item->key = key;
    mini_list_insert(&self->array[i], &item->list,
                     mini_list_end(&self->array[i]));
    ++self->items;

    /*  If the hash is getting full, double the amount of slots and
        re-hash all the items. */
    if (mini_slow(self->items * 2 > self->slots && self->slots < 0x80000000))
        mini_hash_rehash(self);
}

void mini_hash_erase(struct mini_hash *self, struct mini_hash_item *item)
{
    uint32_t slot;

    slot = mini_hash_key(item->key) % self->slots;
    mini_list_erase(&self->array[slot], &item->list);
    --self->items;
}

struct mini_hash_item *mini_hash_get(struct mini_hash *self, uint32_t key)
{
    uint32_t slot;
    struct mini_list_item *it;
    struct mini_hash_item *item;

    slot = mini_hash_key(key) % self->slots;

    for (it = mini_list_begin(&self->array[slot]);
         it != mini_list_end(&self->array[slot]);
         it = mini_list_next(&self->array[slot], it))
    {
        item = mini_cont(it, struct mini_hash_item, list);
        if (item->key == key)
            return item;
    }

    return NULL;
}

uint32_t mini_hash_key(uint32_t key)
{
    /*  TODO: This is a randomly chosen hashing function. Give some thought
        to picking a more fitting one. */
    key = (key ^ 61) ^ (key >> 16);
    key += key << 3;
    key = key ^ (key >> 4);
    key = key * 0x27d4eb2d;
    key = key ^ (key >> 15);

    return key;
}

void mini_hash_item_init(struct mini_hash_item *self)
{
    mini_list_item_init(&self->list);
}

void mini_hash_item_term(struct mini_hash_item *self)
{
    mini_list_item_term(&self->list);
}
