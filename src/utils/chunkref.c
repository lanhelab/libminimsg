/*
    Copyright (c) 2013 Martin Sustrik  All rights reserved.

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "chunkref.h"
#include "err.h"

#include <string.h>

/*  mini_chunkref should be reinterpreted as this structure in case the first
    byte ('tag') is 0xff. */
struct mini_chunkref_chunk
{
    uint8_t tag;
    void *chunk;
};

/*  Check whether VSM are small enough for size to fit into the first byte
    of the structure. */
CT_ASSERT(MINI_CHUNKREF_MAX < 255);

/*  Check whether mini_chunkref_chunk fits into mini_chunkref. */
CT_ASSERT(sizeof(struct mini_chunkref) >= sizeof(struct mini_chunkref_chunk));

void mini_chunkref_init(struct mini_chunkref *self, size_t size)
{
    int rc;
    struct mini_chunkref_chunk *ch;

    if (size < MINI_CHUNKREF_MAX)
    {
        self->u.ref[0] = (uint8_t)size;
        return;
    }

    ch = (struct mini_chunkref_chunk *)self;
    ch->tag = 0xff;
    rc = mini_chunk_alloc(size, 0, &ch->chunk);
    errno_assert(rc == 0);
}

void mini_chunkref_init_chunk(struct mini_chunkref *self, void *chunk)
{
    struct mini_chunkref_chunk *ch;

    ch = (struct mini_chunkref_chunk *)self;
    ch->tag = 0xff;
    ch->chunk = chunk;
}

void mini_chunkref_term(struct mini_chunkref *self)
{
    struct mini_chunkref_chunk *ch;

    if (self->u.ref[0] == 0xff)
    {
        ch = (struct mini_chunkref_chunk *)self;
        mini_chunk_free(ch->chunk);
    }
}

void *mini_chunkref_getchunk(struct mini_chunkref *self)
{
    int rc;
    struct mini_chunkref_chunk *ch;
    void *chunk;

    if (self->u.ref[0] == 0xff)
    {
        ch = (struct mini_chunkref_chunk *)self;
        self->u.ref[0] = 0;
        return ch->chunk;
    }

    rc = mini_chunk_alloc(self->u.ref[0], 0, &chunk);
    errno_assert(rc == 0);
    memcpy(chunk, &self->u.ref[1], self->u.ref[0]);
    self->u.ref[0] = 0;
    return chunk;
}

void mini_chunkref_mv(struct mini_chunkref *dst, struct mini_chunkref *src)
{
    memcpy(dst, src, src->u.ref[0] == 0xff ? (int)sizeof(struct mini_chunkref_chunk) : src->u.ref[0] + 1);
}

void mini_chunkref_cp(struct mini_chunkref *dst, struct mini_chunkref *src)
{
    struct mini_chunkref_chunk *ch;

    if (src->u.ref[0] == 0xff)
    {
        ch = (struct mini_chunkref_chunk *)src;
        mini_chunk_addref(ch->chunk, 1);
    }
    memcpy(dst, src, sizeof(struct mini_chunkref));
}

void *mini_chunkref_data(struct mini_chunkref *self)
{
    return self->u.ref[0] == 0xff ? ((struct mini_chunkref_chunk *)self)->chunk : &self->u.ref[1];
}

size_t mini_chunkref_size(struct mini_chunkref *self)
{
    return self->u.ref[0] == 0xff ? mini_chunk_size(((struct mini_chunkref_chunk *)self)->chunk) : self->u.ref[0];
}

void mini_chunkref_trim(struct mini_chunkref *self, size_t n)
{
    struct mini_chunkref_chunk *ch;

    if (self->u.ref[0] == 0xff)
    {
        ch = (struct mini_chunkref_chunk *)self;
        ch->chunk = mini_chunk_trim(ch->chunk, n);
        return;
    }

    mini_assert(self->u.ref[0] >= n);
    memmove(&self->u.ref[1], &self->u.ref[1 + n], self->u.ref[0] - n);
    self->u.ref[0] -= (uint8_t)n;
}

void mini_chunkref_bulkcopy_start(struct mini_chunkref *self, uint32_t copies)
{
    struct mini_chunkref_chunk *ch;

    if (self->u.ref[0] == 0xff)
    {
        ch = (struct mini_chunkref_chunk *)self;
        mini_chunk_addref(ch->chunk, copies);
    }
}

void mini_chunkref_bulkcopy_cp(struct mini_chunkref *dst, struct mini_chunkref *src)
{
    memcpy(dst, src, sizeof(struct mini_chunkref));
}
