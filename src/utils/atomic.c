/*
    Copyright (c) 2013 Martin Sustrik  All rights reserved.

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "atomic.h"
#include "err.h"
#include "attr.h"

void mini_atomic_init(struct mini_atomic *self, uint32_t n)
{
    self->n = n;
#if defined MINI_ATOMIC_MUTEX
    mini_mutex_init(&self->sync);
#endif
}

#if defined MINI_ATOMIC_MUTEX
void mini_atomic_term(struct mini_atomic *self)
{
    mini_mutex_term(&self->sync);
}
#else
void mini_atomic_term(MINI_UNUSED struct mini_atomic *self)
{
}
#endif

uint32_t mini_atomic_inc(struct mini_atomic *self, uint32_t n)
{
#if defined MINI_ATOMIC_WINAPI
    return (uint32_t)InterlockedExchangeAdd((LONG *)&self->n, n);
#elif defined MINI_ATOMIC_SOLARIS
    return atomic_add_32_nv(&self->n, n) - n;
#elif defined MINI_ATOMIC_GCC_BUILTINS
    return (uint32_t)__sync_fetch_and_add(&self->n, n);
#elif defined MINI_ATOMIC_MUTEX
    uint32_t res;
    mini_mutex_lock(&self->sync);
    res = self->n;
    self->n += n;
    mini_mutex_unlock(&self->sync);
    return res;
#else
#error
#endif
}

uint32_t mini_atomic_dec(struct mini_atomic *self, uint32_t n)
{
#if defined MINI_ATOMIC_WINAPI
    return (uint32_t)InterlockedExchangeAdd((LONG *)&self->n, -((LONG)n));
#elif defined MINI_ATOMIC_SOLARIS
    return atomic_add_32_nv(&self->n, -((int32_t)n)) + n;
#elif defined MINI_ATOMIC_GCC_BUILTINS
    return (uint32_t)__sync_fetch_and_sub(&self->n, n);
#elif defined MINI_ATOMIC_MUTEX
    uint32_t res;
    mini_mutex_lock(&self->sync);
    res = self->n;
    self->n -= n;
    mini_mutex_unlock(&self->sync);
    return res;
#else
#error
#endif
}
