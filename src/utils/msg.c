/*
    Copyright (c) 2012-2013 Martin Sustrik  All rights reserved.

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "msg.h"

#include <string.h>

void mini_msg_init(struct mini_msg *self, size_t size)
{
    mini_chunkref_init(&self->sphdr, 0);
    mini_chunkref_init(&self->hdrs, 0);
    mini_chunkref_init(&self->body, size);
}

void mini_msg_init_chunk(struct mini_msg *self, void *chunk)
{
    mini_chunkref_init(&self->sphdr, 0);
    mini_chunkref_init(&self->hdrs, 0);
    mini_chunkref_init_chunk(&self->body, chunk);
}

void mini_msg_term(struct mini_msg *self)
{
    mini_chunkref_term(&self->sphdr);
    mini_chunkref_term(&self->hdrs);
    mini_chunkref_term(&self->body);
}

void mini_msg_mv(struct mini_msg *dst, struct mini_msg *src)
{
    mini_chunkref_mv(&dst->sphdr, &src->sphdr);
    mini_chunkref_mv(&dst->hdrs, &src->hdrs);
    mini_chunkref_mv(&dst->body, &src->body);
}

void mini_msg_cp(struct mini_msg *dst, struct mini_msg *src)
{
    mini_chunkref_cp(&dst->sphdr, &src->sphdr);
    mini_chunkref_cp(&dst->hdrs, &src->hdrs);
    mini_chunkref_cp(&dst->body, &src->body);
}

void mini_msg_bulkcopy_start(struct mini_msg *self, uint32_t copies)
{
    mini_chunkref_bulkcopy_start(&self->sphdr, copies);
    mini_chunkref_bulkcopy_start(&self->hdrs, copies);
    mini_chunkref_bulkcopy_start(&self->body, copies);
}

void mini_msg_bulkcopy_cp(struct mini_msg *dst, struct mini_msg *src)
{
    mini_chunkref_bulkcopy_cp(&dst->sphdr, &src->sphdr);
    mini_chunkref_bulkcopy_cp(&dst->hdrs, &src->hdrs);
    mini_chunkref_bulkcopy_cp(&dst->body, &src->body);
}

void mini_msg_replace_body(struct mini_msg *self, struct mini_chunkref new_body)
{
    mini_chunkref_term(&self->body);
    self->body = new_body;
}
