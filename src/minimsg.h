﻿#ifndef MINIMSG_H_
#define MINIMSG_H_

#ifdef __cplusplus
extern "C"
{
#endif

#ifdef __GNUC__
#define UNUSED(x) UNUSED_##x __attribute__((__unused__))
#else
#define UNUSED(x) UNUSED_##x
#endif

#include <errno.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

/*  Handle DSO symbol visibility. */
#if !defined(MINI_EXPORT)
#if defined(_WIN32) && !defined(MINI_STATIC_LIB)
#if defined MINI_SHARED_LIB
#define MINI_EXPORT __declspec(dllexport)
#else
#define MINI_EXPORT __declspec(dllimport)
#endif
#else
#define MINI_EXPORT extern
#endif
#endif

/******************************************************************************/
/*  ABI versioning support.                                                   */
/******************************************************************************/

/*  Don't change this unless you know exactly what you're doing and have      */
/*  read and understand the following documents:                              */
/*  www.gnu.org/software/libtool/manual/html_node/Libtool-versioning.html     */
/*  www.gnu.org/software/libtool/manual/html_node/Updating-version-info.html  */

/*  The current interface version. */
#define MINI_VERSION_CURRENT 1

/*  The latest revision of the current interface. */
#define MINI_VERSION_REVISION 1

/******************************************************************************/
/*  Errors.                                                                   */
/******************************************************************************/

/*  A number random enough not to collide with different errno ranges on      */
/*  different OSes. The assumption is that error_t is at least 32-bit type.   */
#define MINI_HAUSNUMERO 156384712

/*  On some platforms some standard POSIX errnos are not defined.    */
#ifndef ENOTSUP
#define ENOTSUP (MINI_HAUSNUMERO + 1)
#endif
#ifndef EPROTONOSUPPORT
#define EPROTONOSUPPORT (MINI_HAUSNUMERO + 2)
#endif
#ifndef ENOBUFS
#define ENOBUFS (MINI_HAUSNUMERO + 3)
#endif
#ifndef ENETDOWN
#define ENETDOWN (MINI_HAUSNUMERO + 4)
#endif
#ifndef EADDRINUSE
#define EADDRINUSE (MINI_HAUSNUMERO + 5)
#endif
#ifndef EADDRNOTAVAIL
#define EADDRNOTAVAIL (MINI_HAUSNUMERO + 6)
#endif
#ifndef ECONNREFUSED
#define ECONNREFUSED (MINI_HAUSNUMERO + 7)
#endif
#ifndef EINPROGRESS
#define EINPROGRESS (MINI_HAUSNUMERO + 8)
#endif
#ifndef ENOTSOCK
#define ENOTSOCK (MINI_HAUSNUMERO + 9)
#endif
#ifndef EAFNOSUPPORT
#define EAFNOSUPPORT (MINI_HAUSNUMERO + 10)
#endif
#ifndef EPROTO
#define EPROTO (MINI_HAUSNUMERO + 11)
#endif
#ifndef EAGAIN
#define EAGAIN (MINI_HAUSNUMERO + 12)
#endif
#ifndef EBADF
#define EBADF (MINI_HAUSNUMERO + 13)
#endif
#ifndef EINVAL
#define EINVAL (MINI_HAUSNUMERO + 14)
#endif
#ifndef EMFILE
#define EMFILE (MINI_HAUSNUMERO + 15)
#endif
#ifndef EFAULT
#define EFAULT (MINI_HAUSNUMERO + 16)
#endif
#ifndef EACCES
#define EACCES (MINI_HAUSNUMERO + 17)
#endif
#ifndef EACCESS
#define EACCESS (EACCES)
#endif
#ifndef ENETRESET
#define ENETRESET (MINI_HAUSNUMERO + 18)
#endif
#ifndef ENETUNREACH
#define ENETUNREACH (MINI_HAUSNUMERO + 19)
#endif
#ifndef EHOSTUNREACH
#define EHOSTUNREACH (MINI_HAUSNUMERO + 20)
#endif
#ifndef ENOTCONN
#define ENOTCONN (MINI_HAUSNUMERO + 21)
#endif
#ifndef EMSGSIZE
#define EMSGSIZE (MINI_HAUSNUMERO + 22)
#endif
#ifndef ETIMEDOUT
#define ETIMEDOUT (MINI_HAUSNUMERO + 23)
#endif
#ifndef ECONNABORTED
#define ECONNABORTED (MINI_HAUSNUMERO + 24)
#endif
#ifndef ECONNRESET
#define ECONNRESET (MINI_HAUSNUMERO + 25)
#endif
#ifndef ENOPROTOOPT
#define ENOPROTOOPT (MINI_HAUSNUMERO + 26)
#endif
#ifndef EISCONN
#define EISCONN (MINI_HAUSNUMERO + 27)
#define MINI_EISCOMINI_DEFINED
#endif
#ifndef ESOCKTNOSUPPORT
#define ESOCKTNOSUPPORT (MINI_HAUSNUMERO + 28)
#endif

/*  Native minimsg error codes.                                               */
#ifndef ETERM
#define ETERM (MINI_HAUSNUMERO + 53)
#endif
#ifndef EFSM
#define EFSM (MINI_HAUSNUMERO + 54)
#endif

    /*  This function retrieves the errno as it is known to the library.          */
    /*  The goal of this function is to make the code 100% portable, including    */
    /*  where the library is compiled with certain CRT library (on Windows) and   */
    /*  linked to an application that uses different CRT library.                 */
    MINI_EXPORT int mini_errno(void);

    /*  Resolves system errors and native errors to human-readable string.        */
    MINI_EXPORT const char *mini_strerror(int errnum);

    /*  Returns the symbol name (e.g. "MINI_REQ") and value at a specified index.   */
    /*  If the index is out-of-range, returns NULL and sets errno to EINVAL       */
    /*  General usage is to start at i=0 and iterate until NULL is returned.      */
    MINI_EXPORT const char *mini_symbol(int i, int *value);

/*  Constants that are returned in `ns` member of mini_symbol_properties        */
#define MINI_NS_NAMESPACE 0
#define MINI_NS_VERSION 1
#define MINI_NS_DOMAIN 2
#define MINI_NS_TRANSPORT 3
#define MINI_NS_PROTOCOL 4
#define MINI_NS_OPTION_LEVEL 5
#define MINI_NS_SOCKET_OPTION 6
#define MINI_NS_TRANSPORT_OPTION 7
#define MINI_NS_OPTION_TYPE 8
#define MINI_NS_OPTION_UNIT 9
#define MINI_NS_FLAG 10
#define MINI_NS_ERROR 11
#define MINI_NS_LIMIT 12
#define MINI_NS_EVENT 13
#define MINI_NS_STATISTIC 14

/*  Constants that are returned in `type` member of mini_symbol_properties      */
#define MINI_TYPE_NONE 0
#define MINI_TYPE_INT 1
#define MINI_TYPE_STR 2

/*  Constants that are returned in the `unit` member of mini_symbol_properties  */
#define MINI_UNIT_NONE 0
#define MINI_UNIT_BYTES 1
#define MINI_UNIT_MILLISECONDS 2
#define MINI_UNIT_PRIORITY 3
#define MINI_UNIT_BOOLEAN 4
#define MINI_UNIT_MESSAGES 5
#define MINI_UNIT_COUNTER 6

    /*  Structure that is returned from mini_symbol  */
    struct mini_symbol_properties
    {

        /*  The constant value  */
        int value;

        /*  The constant name  */
        const char *name;

        /*  The constant namespace, or zero for namespaces themselves */
        int ns;

        /*  The option type for socket option constants  */
        int type;

        /*  The unit for the option value for socket option constants  */
        int unit;
    };

    /*  Fills in mini_symbol_properties structure and returns it's length           */
    /*  If the index is out-of-range, returns 0                                   */
    /*  General usage is to start at i=0 and iterate until zero is returned.      */
    MINI_EXPORT int mini_symbol_info(int i,
                                     struct mini_symbol_properties *buf, int buflen);

    /******************************************************************************/
    /*  Helper function for shutting down multi-threaded applications.            */
    /******************************************************************************/

    MINI_EXPORT void mini_term(void);

    /******************************************************************************/
    /*  Zero-copy support.                                                        */
    /******************************************************************************/

#define MINI_MSG ((size_t)-1)

    MINI_EXPORT void *mini_allocmsg(size_t size, int type);
    MINI_EXPORT void *mini_reallocmsg(void *msg, size_t size);
    MINI_EXPORT int mini_freemsg(void *msg);

    /******************************************************************************/
    /*  Socket definition.                                                        */
    /******************************************************************************/

    struct mini_iovec
    {
        void *iov_base;
        size_t iov_len;
    };

    struct mini_msghdr
    {
        struct mini_iovec *msg_iov;
        int msg_iovlen;
        void *msg_control;
        size_t msg_controllen;
    };

    struct mini_cmsghdr
    {
        size_t cmsg_len;
        int cmsg_level;
        int cmsg_type;
    };

    /*  Internal stuff. Not to be used directly.                                  */
    MINI_EXPORT struct mini_cmsghdr *mini_cmsg_nxthdr_(
        const struct mini_msghdr *mhdr,
        const struct mini_cmsghdr *cmsg);
#define MINI_CMSG_ALIGN_(len) \
    (((len) + sizeof(size_t) - 1) & (size_t) ~(sizeof(size_t) - 1))

    /* POSIX-defined msghdr manipulation. */

#define MINI_CMSG_FIRSTHDR(mhdr) \
    mini_cmsg_nxthdr_((struct mini_msghdr *)(mhdr), NULL)

#define MINI_CMSG_NXTHDR(mhdr, cmsg) \
    mini_cmsg_nxthdr_((struct mini_msghdr *)(mhdr), (struct mini_cmsghdr *)(cmsg))

#define MINI_CMSG_DATA(cmsg) \
    ((unsigned char *)(((struct mini_cmsghdr *)(cmsg)) + 1))

    /* Extensions to POSIX defined by RFC 3542.                                   */

#define MINI_CMSG_SPACE(len) \
    (MINI_CMSG_ALIGN_(len) + MINI_CMSG_ALIGN_(sizeof(struct mini_cmsghdr)))

#define MINI_CMSG_LEN(len) \
    (MINI_CMSG_ALIGN_(sizeof(struct mini_cmsghdr)) + (len))

/*  SP address families.                                                      */
#define AF_SP 1
#define AF_SP_RAW 2

/*  Max size of an SP address.                                                */
#define MINI_SOCKADDR_MAX 128

/*  Socket option levels: Negative numbers are reserved for transports,
    positive for socket types. */
#define MINI_SOL_SOCKET 0

/*  Generic socket options (MINI_SOL_SOCKET level).                             */
#define MINI_LINGER 1
#define MINI_SNDBUF 2
#define MINI_RCVBUF 3
#define MINI_SNDTIMEO 4
#define MINI_RCVTIMEO 5
#define MINI_RECONNECT_IVL 6
#define MINI_RECONNECT_IVL_MAX 7
#define MINI_SNDPRIO 8
#define MINI_RCVPRIO 9
#define MINI_SNDFD 10
#define MINI_RCVFD 11
#define MINI_DOMAIN 12
#define MINI_PROTOCOL 13
#define MINI_IPV4ONLY 14
#define MINI_SOCKET_NAME 15
#define MINI_RCVMAXSIZE 16
#define MINI_MAXTTL 17

/*  Send/recv options.                                                        */
#define MINI_DONTWAIT 1

/*  Ancillary data.                                                           */
#define PROTO_SP 1
#define SP_HDR 1

    MINI_EXPORT int mini_socket(int domain, int protocol);
    MINI_EXPORT int mini_close(int s);
    MINI_EXPORT int mini_setsockopt(int s, int level, int option, const void *optval,
                                    size_t optvallen);
    MINI_EXPORT int mini_getsockopt(int s, int level, int option, void *optval,
                                    size_t *optvallen);
    MINI_EXPORT int mini_bind(int s, const char *addr);
    MINI_EXPORT int mini_connect(int s, const char *addr);
    MINI_EXPORT int mini_shutdown(int s, int how);
    MINI_EXPORT int mini_send(int s, const void *buf, size_t len, int flags);
    MINI_EXPORT int mini_recv(int s, void *buf, size_t len, int flags);
    MINI_EXPORT int mini_sendmsg(int s, const struct mini_msghdr *msghdr, int flags);
    MINI_EXPORT int mini_recvmsg(int s, struct mini_msghdr *msghdr, int flags);

    /******************************************************************************/
    /*  Socket mutliplexing support.                                              */
    /******************************************************************************/

#define MINI_POLLIN 1
#define MINI_POLLOUT 2

    struct mini_pollfd
    {
        int fd;
        short events;
        short revents;
    };

    MINI_EXPORT int mini_poll(struct mini_pollfd *fds, int nfds, int timeout);

    /******************************************************************************/
    /*  Built-in support for devices.                                             */
    /******************************************************************************/

    MINI_EXPORT int mini_device(int s1, int s2);

/******************************************************************************/
/*  Statistics.                                                               */
/******************************************************************************/

/*  Transport statistics  */
#define MINI_STAT_ESTABLISHED_CONNECTIONS 101
#define MINI_STAT_ACCEPTED_CONNECTIONS 102
#define MINI_STAT_DROPPED_CONNECTIONS 103
#define MINI_STAT_BROKEN_CONNECTIONS 104
#define MINI_STAT_CONNECT_ERRORS 105
#define MINI_STAT_BIND_ERRORS 106
#define MINI_STAT_ACCEPT_ERRORS 107

#define MINI_STAT_CURRENT_CONNECTIONS 201
#define MINI_STAT_INPROGRESS_CONNECTIONS 202
#define MINI_STAT_CURRENT_EP_ERRORS 203

/*  The socket-internal statistics  */
#define MINI_STAT_MESSAGES_SENT 301
#define MINI_STAT_MESSAGES_RECEIVED 302
#define MINI_STAT_BYTES_SENT 303
#define MINI_STAT_BYTES_RECEIVED 304
/*  Protocol statistics  */
#define MINI_STAT_CURRENT_SND_PRIORITY 401

    MINI_EXPORT uint64_t mini_get_statistic(int s, int stat);

#ifdef __cplusplus
}
#endif

#endif // MINIIMSG_H_