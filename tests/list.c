/*
    Copyright (c) 2013 Nir Soffer <nirsof@gmail.com>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include "../src/utils/cont.h"

#include "../src/utils/err.c"
#include "../src/utils/list.c"

static struct mini_list_item sentinel;

/*  Typical object that can be added to a list. */
struct item
{
    int value;
    struct mini_list_item item;
};

/*  Initializing list items statically so they can be inserted into a list. */
static struct item that = {1, MINI_LIST_ITEM_INITIALIZER};
static struct item other = {2, MINI_LIST_ITEM_INITIALIZER};

int main()
{
    int rc;
    struct mini_list list;
    struct mini_list_item *list_item;
    struct item *item;

    /*  List item life cycle. */

    /*  Initialize the item. Make sure it's not part of any list. */
    mini_list_item_init(&that.item);
    mini_assert(!mini_list_item_isinlist(&that.item));

    /*  That may be part of some list, or uninitialized memory. */
    that.item.prev = &sentinel;
    that.item.next = &sentinel;
    mini_assert(mini_list_item_isinlist(&that.item));
    that.item.prev = NULL;
    that.item.next = NULL;
    mini_assert(mini_list_item_isinlist(&that.item));

    /*  Before termination, item must be removed from the list. */
    mini_list_item_init(&that.item);
    mini_list_item_term(&that.item);

    /*  Initializing a list. */

    /*  Uninitialized list has random content. */
    list.first = &sentinel;
    list.last = &sentinel;

    mini_list_init(&list);

    mini_assert(list.first == NULL);
    mini_assert(list.last == NULL);

    mini_list_term(&list);

    /*  Empty list. */

    mini_list_init(&list);

    rc = mini_list_empty(&list);
    mini_assert(rc == 1);

    list_item = mini_list_begin(&list);
    mini_assert(list_item == NULL);

    list_item = mini_list_end(&list);
    mini_assert(list_item == NULL);

    mini_list_term(&list);

    /*  Inserting and erasing items. */

    mini_list_init(&list);
    mini_list_item_init(&that.item);

    /*  Item doesn'tt belong to list yet. */
    mini_assert(!mini_list_item_isinlist(&that.item));

    mini_list_insert(&list, &that.item, mini_list_end(&list));

    /*  Item is now part of a list. */
    mini_assert(mini_list_item_isinlist(&that.item));

    /*  Single item does not have prev or next item. */
    mini_assert(that.item.prev == NULL);
    mini_assert(that.item.next == NULL);

    /*  Item is both first and list item. */
    mini_assert(list.first == &that.item);
    mini_assert(list.last == &that.item);

    /*  Removing an item. */
    mini_list_erase(&list, &that.item);
    mini_assert(!mini_list_item_isinlist(&that.item));

    mini_assert(list.first == NULL);
    mini_assert(list.last == NULL);

    mini_list_item_term(&that.item);
    mini_list_term(&list);

    /*  Iterating items. */

    mini_list_init(&list);
    mini_list_item_init(&that.item);

    mini_list_insert(&list, &that.item, mini_list_end(&list));

    list_item = mini_list_begin(&list);
    mini_assert(list_item == &that.item);

    item = mini_cont(list_item, struct item, item);
    mini_assert(item == &that);

    list_item = mini_list_end(&list);
    mini_assert(list_item == NULL);

    list_item = mini_list_prev(&list, &that.item);
    mini_assert(list_item == NULL);

    list_item = mini_list_next(&list, &that.item);
    mini_assert(list_item == NULL);

    rc = mini_list_empty(&list);
    mini_assert(rc == 0);

    mini_list_erase(&list, &that.item);
    mini_list_item_term(&that.item);
    mini_list_term(&list);

    /*  Appending items. */

    mini_list_init(&list);
    mini_list_item_init(&that.item);
    mini_list_item_init(&other.item);

    mini_list_insert(&list, &that.item, mini_list_end(&list));
    mini_list_insert(&list, &other.item, mini_list_end(&list));

    list_item = mini_list_begin(&list);
    mini_assert(list_item == &that.item);

    list_item = mini_list_next(&list, list_item);
    mini_assert(list_item == &other.item);

    mini_list_erase(&list, &that.item);
    mini_list_erase(&list, &other.item);
    mini_list_item_term(&that.item);
    mini_list_item_term(&other.item);
    mini_list_term(&list);

    /*  Prepending items. */

    mini_list_init(&list);
    mini_list_item_init(&that.item);
    mini_list_item_init(&other.item);

    mini_list_insert(&list, &that.item, mini_list_begin(&list));
    mini_list_insert(&list, &other.item, mini_list_begin(&list));

    list_item = mini_list_begin(&list);
    mini_assert(list_item == &other.item);

    list_item = mini_list_next(&list, list_item);
    mini_assert(list_item == &that.item);

    mini_list_erase(&list, &that.item);
    mini_list_erase(&list, &other.item);
    mini_list_item_term(&that.item);
    mini_list_item_term(&other.item);
    mini_list_term(&list);

    return 0;
}
