# libminimsg - C/C++ Asynchronous event network library

libminimsg is a standalone implementation of C bindings for POSIX platforms (including GNU/Linux,Apple macOS and iOS) and Microsoft

## Supported Platforms

    Microsoft Windows
    Apple macOS
    Linux
    FreeBSD
    OpenBSD
    Solaris
    AIX

## Environmental dependence

    cmake
    Compiler tool:
        gcc/clang/msvc/mingw

## Support architecture

    arm aarch x86 amd64

## Third party Library

### linux install

    apt-get install libuv1-dev

### mac install

    brew install libuv
